# French translation of optimystic.
# Copyright (C) 2004 Free Software Foundation, Inc.
# This file is distributed under the same license as the optimystic package.
#
# Laurent RICHARD <laurent.richard@lilit.be>, 2004.
# Christophe Merlet (RedFox) <redfox@redfoxcenter.org>, 2004.
#
msgid ""
msgstr ""
"Project-Id-Version: optimystic 0.1.0\n"
"POT-Creation-Date: 2004-01-24 00:15-0500\n"
"PO-Revision-Date: 2004-01-22 16:52+0100\n"
"Last-Translator: Laurent RICHARD <laurent.richard@lilit.be>\n"
"Language-Team: GNOME French Team <gnomefr@traduc.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Report-Msgid-Bugs-To: \n"

#: optimystic.desktop.in.h:1
msgid "CD Writer"
msgstr "Graveur de CD"

#: optimystic.desktop.in.h:2
msgid "Create audio and data CDs"
msgstr "Crée des CD audios et de données"

#: src/cd-audio.c:392 src/cd-filelist.c:731
msgid "Track"
msgstr "Piste"

#: src/cd-audio.c:397 src/cd-data.c:229
msgid "Name"
msgstr "Nom"

#: src/cd-audio.c:402 src/cd-filelist.c:757
msgid "Length"
msgstr "Durée"

#: src/cd-audio.c:428
msgid "CD _Length:"
msgstr "_Durée du CD :"

#: src/cd-audio.c:434
msgid "74 minutes"
msgstr "74 minutes"

#: src/cd-audio.c:439
msgid "80 minutes"
msgstr "80 minutes"

#: src/cd-audio.c:469 src/cd-lib.c:345
msgid "_Play"
msgstr "_Lecture"

#: src/cd-audio.c:476
msgid "Sto_p"
msgstr "A_rrêt"

#: src/cd-data.c:236 src/cd-filelist.c:714
msgid "Size"
msgstr "Taille"

#: src/cd-data.c:262
msgid "CD _Size:"
msgstr "_Taille du CD :"

#: src/cd-data.c:268
msgid "650 megabytes"
msgstr "650 megaoctets"

#: src/cd-data.c:273
msgid "700 megabytes"
msgstr "700 megaoctets"

#: src/cd-data.c:313
msgid "_Create Folder"
msgstr "_Créer un dossier"

#: src/cd-filelist.c:222
msgid "%H:%M:%S"
msgstr "%H:%M:%S"

#: src/cd-filelist.c:223
msgid "--:--:--"
msgstr "--:--:--"

#: src/cd-filelist.c:227
msgid "%M:%S"
msgstr "%M:%S"

#: src/cd-filelist.c:228
msgid "--:--"
msgstr "--:--"

#: src/cd-filelist.c:695
msgid "File name"
msgstr "Nom du fichier"

#: src/cd-filelist.c:738
msgid "Title"
msgstr "Titre"

#: src/cd-filelist.c:880 src/cd-lib.c:746 src/cd-lib.c:811
msgid "Untitled Disc"
msgstr "Disque sans titre"

#: src/cd-filelist.c:1306 src/dialogs.c:487
#, c-format
msgid "The file '%s' could not be found."
msgstr "Le fichier « %s » ne peut être trouvé."

#: src/cd-filelist.c:1312 src/dialogs.c:493
#, c-format
msgid "You do not have permission to access '%s'."
msgstr "Vous n'avez pas l'autorisation d'accéder à « %s »."

#: src/cd-filelist.c:1318 src/dialogs.c:499
#, c-format
msgid "Unable to open the file '%s'."
msgstr "Impossible d'ouvrir le fichier « %s »."

#: src/cd-filelist.c:1349
#, c-format
msgid "Could not add '%s' because another file on the disc has the same name."
msgstr ""
"Impossible d'ajouter « %s » car un autre fichier sur le disque porte le même "
"nom."

#: src/cd-filelist.c:1357
#, c-format
msgid "The file '%s' is not supported by this type of disc."
msgstr "Le fichier « %s » n'est pas accepté par ce type de disque."

#: src/cd-filelist.c:1679
msgid "New Folder"
msgstr "Nouveau dossier"

#: src/cd-filelist.c:1687
#, c-format
msgid "New Folder %d"
msgstr "Nouveau dossier %d"

#: src/cd-filelist.c:1830
#, c-format
msgid "Could not save file '%s'."
msgstr "Impossible d'enregistrer le fichier « %s »."

#: src/cd-filelist.c:1860 src/cd-filelist.c:1865 src/dialogs.c:518
#, c-format
msgid "Could not open '%s'.  The file does not appear to be a saved disc."
msgstr ""
"Impossible d'ouvrir « %s ». Le fichier ne semble pas être un disque "
"enregistré."

#: src/cd-lib.c:272
msgid "Add Files"
msgstr "Ajouter des fichiers"

#: src/cd-lib.c:349
msgid "Move _Up"
msgstr "_Monter"

#: src/cd-lib.c:355
msgid "Move Do_wn"
msgstr "_Descendre"

#. Instead of the untranslated string in path[0], use a translated
#. * string to represent the disc's root.
#.
#: src/cd-lib.c:385
msgid "Disc"
msgstr "Disque"

#: src/cd-lib.c:486
msgid "_Location:"
msgstr "_Emplacement :"

#: src/cd-lib.c:701
msgid "Changing the disc type will remove the files currently on the disc."
msgstr ""
"Changer le type de disque supprimera les fichiers actuellement sur le disque."

#: src/cd-lib.c:705
msgid "Change"
msgstr "Changer"

#: src/cd-lib.c:878
msgid "Audio Disc"
msgstr "Disque audio"

#: src/cd-lib.c:884
msgid "Data Disc"
msgstr "Disque de données"

#: src/dialogs.c:130
#, c-format
msgid ""
"A file named \"%s\" already exists.  Do you want to replace it with the one "
"you are saving?"
msgstr ""
"Un fichier nommé « %s » existe déjà. Vouleez-vous le remplacer par celui que "
"vous enregistrez ?"

#: src/dialogs.c:134
msgid "_Replace"
msgstr "_Remplacer"

#: src/dialogs.c:159
msgid "translator_credits"
msgstr ""
"Laurent RICHARD\n"
"Christophe Merlet (RedFox) <redfox@redfoxcenter.org>"

#. Disc Properties
#: src/dialogs.c:223 src/dialogs.c:850
msgid "Disc Properties"
msgstr "Propriétés du disque"

#: src/dialogs.c:240 src/dialogs.c:859
#, fuzzy
msgid "Disc _title:"
msgstr "/Disque/_Quitter"

#: src/dialogs.c:256
msgid "Disc _publisher:"
msgstr ""

#: src/dialogs.c:287
#, c-format
msgid "%s Preferences"
msgstr "Préférence de %s"

#: src/dialogs.c:395
msgid "Save Disc"
msgstr "Enregistrer le disque"

#: src/dialogs.c:574
msgid "Open Disc"
msgstr "Ouvrir un disque"

#: src/dialogs.c:623
msgid "Do you want to save the changes you made to this disc?"
msgstr ""
"Désirez-vous enregistrer les modifications que vous avez faîtes sur le "
"disque ?"

#: src/dialogs.c:625
#, fuzzy
msgid "_Don't save"
msgstr "Ne pas enregistrer"

#: src/dialogs.c:735
msgid "Save Image File as"
msgstr "Enregistrer le fichier image sous"

#: src/dialogs.c:774
msgid "Image file"
msgstr "Fichier image"

#: src/dialogs.c:789
msgid "Fastest possible"
msgstr "Le plus plus rapidement possible"

#: src/dialogs.c:833 src/win-main.c:334
msgid "Write to CD"
msgstr "Graver sur le CD"

#: src/dialogs.c:837
msgid "_Write"
msgstr "_Graver"

#. Writing Options
#: src/dialogs.c:872
msgid "Writing Options"
msgstr "Options de gravure"

#: src/dialogs.c:881
#, fuzzy
msgid "W_rite to:"
msgstr "Graver sur le CD"

#: src/dialogs.c:898
msgid "_Speed:"
msgstr ""

#. Simulate
#: src/dialogs.c:910
#, fuzzy
msgid "Test _before writing"
msgstr "_Essai avant la gravure"

#: src/eggsidebarbutton.c:229
msgid "/_Rename"
msgstr "/_Renommer"

#: src/eggsidebarbutton.c:230
msgid "/_Delete"
msgstr "/_Supprimer"

#: src/optimystic.c:218 src/optimystic.c:274
msgid ""
"Could not open your preferred GStreamer audio sink.  Please ensure that it "
"is set correctly."
msgstr ""
"Impossible d'utiliser votre sortie audio GStreamer favorite. Veuillez vous "
"assurer qu'elle est configurée correctement."

#: src/optimystic.c:227 src/optimystic.c:283
msgid ""
"Could not open the GStreamer disk source.  Please ensure that your GStreamer "
"installation was successful."
msgstr ""
"Impossible d'ouvrir la source disque GStreamer. Veuillez vous assurer que "
"votre installation de GStreamer a réussi."

#: src/win-main.c:51
msgid "/_Disc"
msgstr "/_Disque"

#: src/win-main.c:52
msgid "/Disc/_New"
msgstr "/Disque/_Nouveau"

#: src/win-main.c:53
msgid "/Disc/_Open..."
msgstr "/Disque/_Ouvrir..."

#: src/win-main.c:55
msgid "/Disc/_Save"
msgstr "/Disque/_Enregistrer"

#: src/win-main.c:56
msgid "/Disc/Save _as..."
msgstr "/Disque/Enregistrer _sous..."

#: src/win-main.c:58
msgid "/Disc/_Properties"
msgstr "/Disque/_Propriétés"

#: src/win-main.c:60
msgid "/Disc/_Write to CD..."
msgstr "/Disque/_Graver sur le CD..."

#: src/win-main.c:62
msgid "/Disc/_Close"
msgstr "/Disque/_Fermer"

#: src/win-main.c:63
msgid "/Disc/_Quit"
msgstr "/Disque/_Quitter"

#: src/win-main.c:64
msgid "/_Edit"
msgstr "/_Édition"

#: src/win-main.c:65
msgid "/Edit/_Add Files..."
msgstr "/Édition/A_jouter des fichiers..."

#: src/win-main.c:66
msgid "/Edit/_Create Folder..."
msgstr "/Édition/_Créer un dossier..."

#: src/win-main.c:67
msgid "/Edit/_Remove"
msgstr "/Édition/_Supprimer"

#: src/win-main.c:69
msgid "/Edit/Re_name..."
msgstr "/Édition/Re_nommer..."

#: src/win-main.c:70
msgid "/Edit/Move _up"
msgstr "/Édition/_Monter"

#: src/win-main.c:71
msgid "/Edit/Move _down"
msgstr "/Édition/_Descendre"

#: src/win-main.c:73
msgid "/Edit/_Preferences"
msgstr "/Édition/_Préférences"

#: src/win-main.c:74
msgid "/_Help"
msgstr "/_Aide"

#: src/win-main.c:75
msgid "/Help/_About"
msgstr "/Aide/À _propos"

#: src/win-main.c:304
msgid "/Disc/Save"
msgstr "/Disque/Enregistrer"

#: src/win-main.c:305
msgid "/Disc/Save as..."
msgstr "/Disque/Enregistrer sous..."

#: src/win-main.c:306
msgid "/Disc/Properties"
msgstr "/Disque/Propriétés"

#: src/win-main.c:307
msgid "/Disc/Write to CD..."
msgstr "/Disque/Graver sur le CD..."

#: src/win-main.c:308
msgid "/Disc/Close"
msgstr "/Disque/Fermer"

#: src/win-main.c:309
msgid "/Edit/Add Files..."
msgstr "/Édition/Ajouter fichiers..."

#: src/win-main.c:310
msgid "/Edit/Create Folder..."
msgstr "/Édition/Créer un dossier..."

#: src/win-main.c:311
msgid "/Edit/Remove"
msgstr "/Édition/Supprimer"

#: src/win-main.c:312
msgid "/Edit/Rename..."
msgstr "/Édition/Renommer..."

#: src/win-main.c:313
msgid "/Edit/Move up"
msgstr "/Édition/Monter"

#: src/win-main.c:314
msgid "/Edit/Move down"
msgstr "/Édition/Descendre"

#: src/win-main.c:327
msgid "Create a new disc"
msgstr "Créer un nouveau disque"

#: src/win-main.c:329
msgid "Open an existing disc"
msgstr "Ouvrir un disque existant"

#: src/win-main.c:331
msgid "Save this disc"
msgstr "Enregistrer ce disque"

#: src/win-main.c:334
msgid "Write contents to CD"
msgstr "Graver le contenu sur le CD"
