
* Optimize refresh_view() to only refresh after all files have been added or
  removed.

* Add "Go" menu with an "Up" menu item, a separator, and then a list of folders
  in the current path.  For example, if the current directory is 
  "Disc/src/code.c", the menu should contain items to take the user to the
  "Disc" and "src" folders.

* Re-write "Burn Image" dialog to use libburn instead of mkfsiso. (in progress)

* Add verbose user-visible error messages to CDFileList via the GError** passed
  to most CDFileList methods.

* Hook-up the user interface to the GError's mentioned above.

* Add support for recent-files.

* Busy-cursor when adding files to disc.

* Edit->Add folder menuitem.

=======================
Milestone Release Plan
=======================

* 0.1
- Support creating and editing data discs.
- Support saving and loading disc layouts.
- Support creating data ISO images.

* 0.2
- Support creating and editing MP3 discs.
- Support automatic conversion of any gstreamer-supported audio file
  to MP3 format.
- Support creating MP3 ISO images.

* 0.3
- Support creating and editing RedBook audio discs.
- Support automatic conversion of any gstreamer-supported audio file
  to RedBook audio.
- Include some form of audio file writing support, i.e. enable creation
  of audio-cd.cdr files which can be burned using cdrecord.
  
* 0.4
- Implement CD burning support via libburn.

==============
Design Notes
==============

* Write CD Dialog
- All values should be saved after clicking "Write to CD"
- Previous values should be restored upon dialog creation

* Disc Properties Dialog
- Publisher should be saved and used as the default for the next disc
- If copyright is empty, when editing Publisher, copyright should be updated
  to read "Copyright XXXX by PUBLISHER" where XXXX is the current year.
