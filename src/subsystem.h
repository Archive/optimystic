/*
 *  Copyright 2003 Todd Kulesza <todd@dropline.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef _SUBSYSTEM_H_
#define _SUBSYSTEM_H_

#include "cd-filelist.h"

#include <libburn/libburn.h>
#include <glib.h>

gint
drives_detect (struct burn_drive_info **drives, gint *n_drives);
		
void
drives_free (struct burn_drive_info *drives);

void
write_image_to_file (CDFileList *list, const gchar *image, GAsyncQueue *queue);

void
write_image_to_cd (struct burn_drive_info *drive, const gchar *image,
		gboolean simulate, GAsyncQueue *queue);

void
erase_cd (struct burn_drive_info *drive, GAsyncQueue *queue);

#endif /* _SUBSYSTEM_H_ */
