/*
 *  Copyright 2003-2004 Todd Kulesza <todd@dropline.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h"

#include "icon-text-type.h"
#include "optimystic.h"
#include "cd-lib.h"

#include <string.h>
#include <gnome.h>
#include <libgnomevfs/gnome-vfs-utils.h>
#include <libgnomevfs/gnome-vfs-directory.h>
#include <libgnomevfs/gnome-vfs-ops.h>
#include <libxml/tree.h>
#include <libxml/parser.h>

enum
{
	SIDEBAR_DISPLAY,
	SIDEBAR_TYPE,
	N_SIDEBAR
};

/* global filename, used when changing disc types */
gchar *global_filename = NULL;

/* convert the url to a text uri */
/* code from from file-roller 2.2 */

static gchar *
get_path_from_url (gchar *url)
{
	GnomeVFSURI *uri;
	gchar *escaped;
	gchar *path;

	if (url == NULL)
		return NULL;

	uri = gnome_vfs_uri_new (url);
	escaped = gnome_vfs_uri_to_string (uri, GNOME_VFS_URI_HIDE_TOPLEVEL_METHOD);
	path = gnome_vfs_unescape_string (escaped, NULL);

	gnome_vfs_uri_unref (uri);
	g_free (escaped);

	return path;
}

/* convert the list of urls to a list of text uris */
/* code from from file-roller 2.2 */

static GList *
get_file_list_from_url_list (gchar *url_list)
{
	GList *list = NULL;
	int    i;
	char  *url_start, *url_end;

	i = 0;
	url_start = url_list;
	while (url_list[i] != '\0')
	{
		char *url;

		while ((url_list[i] != '\0')
		       && (url_list[i] != '\r')
		       && (url_list[i] != '\n')) i++;

		url_end = url_list + i;
		if (strncmp (url_start, "file:", 5) == 0)
		{
			url_start += 5;
			if ((url_start[0] == '/') 
			    && (url_start[1] == '/')) url_start += 2;
		}

		url = g_strndup (url_start, url_end - url_start);
		list = g_list_prepend (list, get_path_from_url (url));
		g_free (url);

		while ((url_list[i] != '\0')
		       && ((url_list[i] == '\r')
			   || (url_list[i] == '\n'))) i++;
		url_start = url_list + i;
	}
	
	return g_list_reverse (list);
}

static void
add_data_free (AddData *ad)
{
	g_list_foreach (ad->file_list, (GFunc) g_free, NULL);
	g_list_free (ad->file_list);
	g_free (ad->base_uri);
	g_free (ad);
	
	return;
}

static gboolean
visited_dir (const gchar *rel_path, GnomeVFSFileInfo *info, 
		gboolean recursing_will_loop, gpointer data, gboolean *recurse)
{
	AddData *ad;
	gchar *uri;
	
	ad = (AddData *) data;
	
	uri = g_strdup_printf ("%s%c%s", ad->base_uri, G_DIR_SEPARATOR, rel_path);
	ad->file_list = g_list_prepend (ad->file_list, uri);
	
	*recurse = !recursing_will_loop;
	
	return TRUE;
}

static gpointer
add_files_threaded (gpointer data)
{
	AddData *ad, *recursive_ad;
	GList *current, *files_to_add;
	gchar *uri;
	GnomeVFSFileInfo info;
	GnomeVFSResult result;
	
	ad = (AddData *) data;
	recursive_ad = g_new0 (AddData, 1);
	files_to_add = NULL;
	
	for (current = ad->file_list; current; current = current->next)
	{
		uri = (gchar *) current->data;
		
		result = gnome_vfs_get_file_info (uri, &info, 
				GNOME_VFS_FILE_INFO_DEFAULT |
				GNOME_VFS_FILE_INFO_GET_MIME_TYPE |
				GNOME_VFS_FILE_INFO_FOLLOW_LINKS);
		
		if (result == GNOME_VFS_OK)
		{
			if (!g_ascii_strcasecmp (info.mime_type, "x-directory/normal"))
			{
				/* 'uri' is a folder */
				recursive_ad->base_uri = uri;
				recursive_ad->file_list = NULL;
				
				gnome_vfs_directory_visit (uri,
						GNOME_VFS_FILE_INFO_DEFAULT | 
						GNOME_VFS_FILE_INFO_GET_MIME_TYPE |
						GNOME_VFS_FILE_INFO_FOLLOW_LINKS,
						GNOME_VFS_DIRECTORY_VISIT_LOOPCHECK,
						visited_dir,
						recursive_ad);
				
				/* no need to free the 2nd list since g_list_concat uses it directly */
				files_to_add = g_list_concat (files_to_add, recursive_ad->file_list);
			}
			
			/* now add the uri itself */
			files_to_add = g_list_prepend (files_to_add, g_strdup (uri));
		}
	}

	for (current = files_to_add; current; current = current->next)
	{
		gdk_threads_enter ();
		uri = (gchar *) current->data;
		cdfilelist_file_add (CDFILELIST (od->fl), uri, ad->base_uri, NULL);
		gdk_threads_leave ();
	}
	
	add_data_free (ad);
	
	return NULL;
}

/* create a list of files from icons dragged onto the 'widget' */

static void  
drag_recv_cb (GtkWidget *widget, GdkDragContext *context,
		gint x, gint y, GtkSelectionData *data, guint info, guint time, 
		gpointer user_data)
{
	GList *file_list;
	AddData *ad;
	
	if (! ((data->length >= 0) && (data->format == 8)))
	{
		gtk_drag_finish (context, FALSE, FALSE, time);
		return;
	}
	
	gtk_drag_finish (context, TRUE, FALSE, time);
	
	file_list = get_file_list_from_url_list ((gchar *)data->data);
	
	if (file_list)
	{
		ad = g_new0 (AddData, 1);
		ad->file_list = file_list;
		ad->base_uri = g_path_get_dirname ((gchar *) file_list->data);
		
		g_thread_create (add_files_threaded, ad, FALSE, NULL);
	}
	
	return;
}

/* create a list of files selected in the Add Files dialog */

static void
add_files_dialog_cb (GtkWidget *dialog)
{
	gchar *utf8, *local;
	GSList *files, *current;
	GList *file_list;
	gsize read, write;
	GError *error;
	AddData *ad;
	
	file_list = NULL;
	error = NULL;
	files = gtk_file_chooser_get_filenames (GTK_FILE_CHOOSER (dialog));
	
	for (current = files; current; current = current->next)
	{
		local = current->data;
		utf8 = g_filename_to_utf8 (local, -1, &read, &write, &error);
		if (utf8)
			file_list = g_list_append (file_list, utf8);
		else
			opti_standard_error (dialog, error);
		g_free (local);
	}
	
	g_slist_free (files);
	
	if (file_list)
	{
		ad = g_new0 (AddData, 1);
		ad->file_list = file_list;
		ad->base_uri = g_path_get_dirname ((gchar *) file_list->data);
		
		g_thread_create (add_files_threaded, ad, FALSE, NULL);
	}
	
	return;
}

/* open the Add Files dialog */

void
add_files_dialog (void)
{
	GtkWidget *dialog;
	gint response;
	
	dialog = gtk_file_chooser_dialog_new (_("Add Files"),
			GTK_WINDOW (od->window_main),
			GTK_FILE_CHOOSER_ACTION_OPEN,
			GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
			GTK_STOCK_ADD, GTK_RESPONSE_OK,
			NULL);
	gtk_file_chooser_set_select_multiple (GTK_FILE_CHOOSER (dialog), TRUE);
	gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_OK);
	
	response = gtk_dialog_run (GTK_DIALOG (dialog));
	if (response == GTK_RESPONSE_OK)
		add_files_dialog_cb (dialog);
	
	gtk_widget_destroy (dialog);
	
	return;
}

void
add_folder_dialog (void)
{
	GtkWidget *dialog;
	gint response;
	
	dialog = gtk_file_chooser_dialog_new (_("Add Folder"),
			GTK_WINDOW (od->window_main),
			GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER,
			GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
			GTK_STOCK_ADD, GTK_RESPONSE_OK,
			NULL);
	gtk_file_chooser_set_select_multiple (GTK_FILE_CHOOSER (dialog), TRUE);
	gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_OK);
	
	response = gtk_dialog_run (GTK_DIALOG (dialog));
	if (response == GTK_RESPONSE_OK)
		add_files_dialog_cb (dialog);
	
	gtk_widget_destroy (dialog);
	
	return;
}

static void
add_cb (GtkWidget *w, gpointer data)
{
	add_files_dialog ();
	
	return;
}

/* Remove any selected files from the disc */

void
remove_files (void)
{
	cdfilelist_remove_selected (CDFILELIST (od->fl), NULL);
	
	return;
}

static void
remove_cb (GtkWidget *w, gpointer data)
{
	remove_files ();
	
	return;
}

static void
move_up_cb (GtkWidget *w, gpointer data)
{
	move_files (CDFILELIST_UP);
	
	return;
}

static void
move_down_cb (GtkWidget *w, gpointer data)
{
	move_files (CDFILELIST_DOWN);
	
	return;
}

/* build the audio controls for a CDFileList widget */

static void
cdfilelist_controls_build_audio (CDFileListControls *flc)
{
	GtkWidget *hbox;
	GtkWidget *w;
	
	hbox = gtk_hbutton_box_new ();
	gtk_button_box_set_layout (GTK_BUTTON_BOX (hbox), GTK_BUTTONBOX_START);
	gtk_box_set_spacing (GTK_BOX (hbox), 12);
	gtk_box_pack_start (GTK_BOX (flc->vbox), hbox, FALSE, FALSE, 0);
	
	w = gtk_button_new_with_mnemonic (_("_Play"));
	gtk_box_pack_start (GTK_BOX (hbox), w, FALSE, FALSE, 0);
	flc->play = w;
	
	w = gtk_button_new_with_mnemonic (_("Move _Up"));
	g_signal_connect (G_OBJECT (w), "clicked",
			G_CALLBACK (move_up_cb), NULL);
	gtk_box_pack_start (GTK_BOX (hbox), w, FALSE, FALSE, 0);
	flc->move_up = w;
	
	w = gtk_button_new_with_mnemonic (_("Move Do_wn"));
	g_signal_connect (G_OBJECT (w), "clicked",
			G_CALLBACK (move_down_cb), NULL);
	gtk_box_pack_start (GTK_BOX (hbox), w, FALSE, FALSE, 0);
	flc->move_down = w;
	
	return;
}

/* set 'menu' to a top-down heirarchy of the current path */

static void
set_path_menu (GtkWidget *menu)
{
	gchar **path, **temp;
	gchar *text;
	gint i, j, length;
	GtkWidget *menuitem;
	
	path = cdfilelist_path_get_array (CDFILELIST (od->fl));
	
	for (length = 0; path && path [length]; length++) {}

	for (i = 0; i < length; i++)
	{
		temp = g_new (gchar *, length + 2);
		
		/* Instead of the untranslated string in path[0], use a translated
		 * string to represent the disc's root.
		 */
		temp [0] = g_strdup (_("Disc"));
		
		for (j = 1; (j + i) < length; j++)
			temp [j] = g_strdup (path [j]);
		temp [j++] = g_strdup ("");
		temp [j] = NULL;
		
		text = g_strjoinv (" " G_DIR_SEPARATOR_S " ", temp);
		menuitem = gtk_menu_item_new_with_label (text);
		gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
		g_free (text);
		
		g_strfreev (temp);
	}
	
	gtk_widget_show_all (menu);
	
	g_strfreev (path);
	
	return;
}

/* Set the "path" menu.  Called whenever the CDFileList's path changes. */

void
path_changed_menu_cb (GtkWidget *list, GtkWidget *option_menu)
{
	GtkWidget *menu;
	
	/* Destroy the current path menu... */
	menu = gtk_option_menu_get_menu (GTK_OPTION_MENU (option_menu));
	gtk_widget_destroy (menu);
	
	/* ... and build a new one. */
	menu = gtk_menu_new ();
	set_path_menu (menu);
	gtk_option_menu_set_menu (GTK_OPTION_MENU (option_menu), menu);
	
	return;
}

/* Change the CDFileList's path when a user selects a new path from the
 * "path" option menu.
 */

void
path_changed (GtkWidget *option_menu, gpointer data)
{
	gint index, i;
	
	index = gtk_option_menu_get_history (GTK_OPTION_MENU (option_menu));
	for (i = 0; i < index; i++)
		cdfilelist_path_up (CDFILELIST (od->fl));
	
	return;
}

/* Set the sensitivity of the "up" button.  Called whenever the CDFileList's 
 * path changes.
 */

void
path_changed_up_cb (GtkWidget *list, GtkWidget *up)
{
	gchar **path;
	gint count;
	
	path = cdfilelist_path_get_array (CDFILELIST (list));
	for (count = 0; path [count]; count++) {}
	
	gtk_widget_set_sensitive (up, (count > 1));
	
	return;
}

/* Go up one folder in the CDFileList's path. */

void
path_up (GtkWidget *up, gpointer data)
{
	cdfilelist_path_up (CDFILELIST (od->fl));
	
	return;
}

/* build the data controls for a CDFileList widget */

static void
cdfilelist_controls_build_data (CDFileListControls *flc)
{
	GtkWidget *hbox1, *hbox2;
	GtkWidget *label;
	GtkWidget *menu;
	GtkWidget *w;
	
	hbox1 = gtk_hbox_new (FALSE, 12);
	gtk_box_pack_start (GTK_BOX (flc->vbox), hbox1, FALSE, FALSE, 0);
	
	hbox2 = gtk_hbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (hbox1), hbox2, TRUE, TRUE, 0);
	
	label = gtk_label_new_with_mnemonic (_("_Location:"));
	gtk_box_pack_start (GTK_BOX (hbox2), label, FALSE, FALSE, 0);
	
	menu = gtk_menu_new ();
	set_path_menu (menu);
	
	w = gtk_option_menu_new ();
	gtk_option_menu_set_menu (GTK_OPTION_MENU (w), menu);
	gtk_label_set_mnemonic_widget (GTK_LABEL (label), w);
	g_signal_connect (G_OBJECT (od->fl), "path_changed",
			G_CALLBACK (path_changed_menu_cb), w);
	g_signal_connect (G_OBJECT (w), "changed",
			G_CALLBACK (path_changed), NULL);
	gtk_box_pack_start (GTK_BOX (hbox2), w, TRUE, TRUE, 0);
	
	hbox2 = gtk_hbutton_box_new ();
	gtk_button_box_set_layout (GTK_BUTTON_BOX (hbox2), GTK_BUTTONBOX_START);
	gtk_box_set_spacing (GTK_BOX (hbox2), 12);
	gtk_box_pack_start (GTK_BOX (hbox1), hbox2, FALSE, FALSE, 0);
	
	w = gtk_button_new_from_stock (GTK_STOCK_GO_UP);
	gtk_widget_set_sensitive (w, FALSE);
	g_signal_connect (G_OBJECT (od->fl), "path_changed",
			G_CALLBACK (path_changed_up_cb), w);
	g_signal_connect (G_OBJECT (w), "clicked",
			G_CALLBACK (path_up), NULL);
	gtk_box_pack_start (GTK_BOX (hbox2), w, FALSE, FALSE, 0);
	flc->go_up = w;
	
	/* Desensitive 'flc->go_up' */
	path_changed_up_cb (od->fl, flc->go_up);
	
	return;
}

static void
refresh_cdfilelist_controls_sensitivity (CDFileListControls *flc, CDFileList *fl, const gint n)
{
	CDFileListType type;
	
	if (!flc)
		return;
	
	type = cdfilelist_get_cd_type (fl);

	gtk_widget_set_sensitive (flc->remove, n);

	if (type == CDFILELIST_AUDIO)
	{
		gtk_widget_set_sensitive (flc->play, (n == 1));
		gtk_widget_set_sensitive (flc->move_up, n);
		gtk_widget_set_sensitive (flc->move_down, n);
	}

	return;
}

static void
selection_changed (GtkTreeSelection *selection, gpointer data)
{
	gint n;
	
	n = gtk_tree_selection_count_selected_rows (selection);
	refresh_cdfilelist_controls_sensitivity (od->flc, CDFILELIST (od->fl), n);
	opti_set_menu_sensitivity (n);
	
	return;
}

/* build the general controls for a CDFileList widget */

static CDFileListControls *
cdfilelist_controls_new (CDFileList *fl)
{
	CDFileListControls *flc;
	GtkTreeSelection *selection;
	GtkWidget *hbox;
	GtkWidget *w;
	
	flc = g_new (CDFileListControls, 1);
	
	flc->vbox = gtk_vbox_new (FALSE, 12);
	gtk_container_set_border_width (GTK_CONTAINER (flc->vbox), 12);
	
	hbox = gtk_hbutton_box_new ();
	gtk_button_box_set_layout (GTK_BUTTON_BOX (hbox), GTK_BUTTONBOX_END);
	gtk_box_set_spacing (GTK_BOX (hbox), 12);
	gtk_box_pack_end (GTK_BOX (flc->vbox), hbox, FALSE, FALSE, 0);
	
	w = gtk_button_new_from_stock (GTK_STOCK_ADD);
	g_signal_connect (G_OBJECT (w), "clicked",
			G_CALLBACK (add_cb), NULL);
	gtk_box_pack_start (GTK_BOX (hbox), w, FALSE, FALSE, 0);
	flc->add = w;
	
	w = gtk_button_new_from_stock (GTK_STOCK_REMOVE);
	g_signal_connect (G_OBJECT (w), "clicked",
			G_CALLBACK (remove_cb), NULL);
	gtk_box_pack_start (GTK_BOX (hbox), w, FALSE, FALSE, 0);
	flc->remove = w;
	
	switch (fl->type)
	{
		case CDFILELIST_AUDIO:
		{
			cdfilelist_controls_build_audio (flc);
			break;
		}
		case CDFILELIST_DATA:
		{
			cdfilelist_controls_build_data (flc);
			break;
		}
		default:
		{
			g_warning ("CDFileList->type is unknown\n");
			break;
		}
	}
	
	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (fl));
	g_signal_connect (G_OBJECT (selection), "changed",
			G_CALLBACK (selection_changed), NULL);
	refresh_cdfilelist_controls_sensitivity (flc, fl, 0);
	
	return flc;
}

/* destroy and free the widgets in a CDFileListControls struct */

void
cdfilelist_controls_destroy (CDFileListControls *flc)
{
	gtk_widget_destroy (flc->vbox);
	g_free (flc);
	
	return;
}

static GtkWidget *
build_filelist_view (GtkWidget *fl, CDFileListControls *flc, GtkWidget *vbox)
{
	GtkWidget *scrolled;
	
	scrolled = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled),
			GTK_POLICY_AUTOMATIC, GTK_POLICY_ALWAYS);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scrolled),
			GTK_SHADOW_IN);
	gtk_box_pack_start (GTK_BOX (vbox), scrolled, TRUE, TRUE, 0);
	gtk_container_add (GTK_CONTAINER (scrolled), fl);
	
	gtk_box_pack_start (GTK_BOX (vbox), od->flc->vbox, FALSE, FALSE, 0);
	
	gtk_widget_show_all (vbox);
	
	return scrolled;
}

static void
files_changed_cb (CDFileList *list, gboolean modified, gpointer data)
{
	gboolean open;
	
	if (od->fl)
		open = TRUE;
	else
		open = FALSE;
	
	gtk_action_group_set_sensitive (od->menus->modified_group, open && modified);
	
	return;
}

static void
name_changed_cb (CDFileList *list, const gchar *name, gpointer data)
{
	gchar *title;
	
	title = g_strdup_printf ("%s - %s", name, DISPLAY_NAME);
	gtk_window_set_title (GTK_WINDOW (od->window_main), title);
	g_free (title);
	
	return;
}

static gboolean
confirm_change_disc_type (void)
{
	gboolean cancel;
	gint result;
	GtkWidget *dialog;
	
	if (!od->fl || cdfilelist_get_is_empty (CDFILELIST (od->fl)))
		return FALSE;
	
	dialog = gtk_message_dialog_new (GTK_WINDOW (od->window_main), 
			GTK_DIALOG_MODAL, 
			GTK_MESSAGE_QUESTION, 
			GTK_BUTTONS_NONE, 
			_("Changing the disc type will remove the files currently on the "
			"disc."));
	gtk_dialog_add_buttons (GTK_DIALOG (dialog),
			GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
			_("Change"), GTK_RESPONSE_ACCEPT,
			NULL);
	
	result = gtk_dialog_run (GTK_DIALOG (dialog));
	
	gtk_widget_destroy (dialog);
	
	if (result == GTK_RESPONSE_ACCEPT)
		cancel = FALSE;
	else
		cancel = TRUE;
	
	return cancel;
}

/* set the last_cd_type gconf key */

static void
set_last_cd_type (CDFileListType type)
{
	const gchar *type_string;
	
	switch (type)
	{
		case CDFILELIST_AUDIO: type_string = "audio"; break;
		case CDFILELIST_DATA: type_string = "data"; break;
		default: type_string = "unknown"; break;
	}
	
	gconf_client_set_string (od->gconf, "/apps/optimystic/last_cd_type",
					type_string, NULL);
	
	return;
}

/* change the current cd-filelist type in 'vbox' */

static void
change_view (GtkWidget *vbox, CDFileListType type)
{
	const gchar *filename;
	
	/* if the filelist exists, destroy it */
	
	if (od->fl)
	{
		if (confirm_change_disc_type ())
		{
			/* FIXME: Set the selection back to the previous type */
			return;
		}
		else
		{
			filename = cdfilelist_get_property (CDFILELIST (od->fl), CDFILELIST_FILE);
			g_free (global_filename);
			if (filename)
				global_filename = g_strdup (filename);
			else
				global_filename = NULL;
			
			cdfilelist_controls_destroy (od->flc);
			od->flc = NULL;
			gtk_widget_destroy (od->fl);
			od->fl = NULL;
			gtk_widget_destroy (od->scrolled);
			od->scrolled = NULL;
		}
	}
	
	/* build the new filelist */
	
	od->fl = cdfilelist_new (type);
	cdfilelist_set_dnd_add_func (CDFILELIST (od->fl), drag_recv_cb, NULL);
	if (type == CDFILELIST_AUDIO)
		cdfilelist_set_audio_types (CDFILELIST (od->fl), od->supported_audio_types);
	od->flc = cdfilelist_controls_new (CDFILELIST (od->fl));
	od->scrolled = build_filelist_view (od->fl, od->flc, vbox);
	set_last_cd_type (type);
	g_signal_connect (G_OBJECT (od->fl), "files_changed",
			G_CALLBACK (files_changed_cb), NULL);
	g_signal_connect (G_OBJECT (od->fl), "name_changed",
			G_CALLBACK (name_changed_cb), NULL);
	name_changed_cb (CDFILELIST (od->fl), _("Untitled Disc"), NULL);
	
	if (global_filename)
		cdfilelist_set_property (CDFILELIST (od->fl), CDFILELIST_FILE, global_filename);
	
	opti_set_menu_sensitivity (0);
	
	return;
}

/* listen for the 'changed' signal from the sidebar and react accordingly */

static void
disc_type_changed_cb (GtkWidget *selection, GtkWidget *vbox)
{
	GtkTreeModel *model;
	GtkTreeIter iter;
	CDFileListType type;
	
	gtk_tree_selection_get_selected (GTK_TREE_SELECTION (selection), &model, &iter);
	gtk_tree_model_get (model, &iter, SIDEBAR_TYPE, &type, -1);
	
	change_view (vbox, type);
	
	return;
}

static void
icon_set_func_text (GtkTreeViewColumn *col, GtkCellRenderer *renderer, GtkTreeModel *model, GtkTreeIter *iter, gpointer data)
{
	FileDisplay *fd;
	
	gtk_tree_model_get (model, iter, SIDEBAR_DISPLAY, &fd, -1);
	
	if (fd && fd->icon)
		g_object_set (renderer, "pixbuf", fd->icon, NULL);
	
	file_display_free (fd);
	
	return;
}

static void
name_set_func_text (GtkTreeViewColumn *col, GtkCellRenderer *renderer, GtkTreeModel *model, GtkTreeIter *iter, gpointer data)
{
	FileDisplay *fd;
	
	gtk_tree_model_get (model, iter, SIDEBAR_DISPLAY, &fd, -1);
	
	if (fd && fd->name)
		g_object_set (renderer, "text", fd->name, NULL);
	
	file_display_free (fd);
	
	return;
}

/* build the sidebar with buttons for each type of cd a user can create */

static GtkWidget *
build_disc_view (CDFileListType last_type)
{
	GtkWidget *hbox, *vbox, *sidebar, *scrolled;
	GtkListStore *store;
	GtkTreeIter iter;
	GtkCellRenderer *renderer;
	GtkTreeViewColumn *column;
	GtkTreeSelection *selection;
	CDFileListType sidebar_type;
	FileDisplay *fd;
	gboolean valid;
	
	/* FIXME: handle the case where a disc already exists */
	if (od->fl)
		return NULL;
	
	hbox = gtk_hbox_new (FALSE, 6);
	gtk_container_set_border_width (GTK_CONTAINER (hbox), 6);
	
	store = gtk_list_store_new (N_SIDEBAR, 
			FILE_DISPLAY_TYPE,
			G_TYPE_INT);
	sidebar = gtk_tree_view_new_with_model (GTK_TREE_MODEL (store));
	
	scrolled = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled), 
			GTK_POLICY_NEVER, GTK_POLICY_AUTOMATIC);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scrolled),
			GTK_SHADOW_IN);
	gtk_container_add (GTK_CONTAINER (scrolled), sidebar);
	
	column = gtk_tree_view_column_new ();
	gtk_tree_view_column_set_title (column, _("Disc Type"));

/*	FIXME: The audio view doesn't work yet, so hide this for now */
/*
	gtk_list_store_append (store, &iter);
	fd = file_display_new ();
	fd->name = g_strdup (_("Audio"));
	fd->icon = gtk_icon_theme_load_icon (gtk_icon_theme_get_default (), 
			"gnome-dev-cdrom-audio", 32, 0, NULL);
	gtk_list_store_set (store, &iter, 
			SIDEBAR_DISPLAY, fd,
			SIDEBAR_TYPE, CDFILELIST_DATA,
			-1);
*/
	gtk_list_store_append (store, &iter);
	fd = file_display_new ();
	fd->name = g_strdup (_("Data"));
	fd->icon = gtk_icon_theme_load_icon (gtk_icon_theme_get_default (), 
			"gnome-dev-cdrom", 32, 0, NULL);
	gtk_list_store_set (store, &iter, 
			SIDEBAR_DISPLAY, fd,
			SIDEBAR_TYPE, CDFILELIST_DATA,
			-1);

	renderer = gtk_cell_renderer_pixbuf_new ();
	gtk_tree_view_column_pack_start (column, renderer, FALSE);
	gtk_tree_view_column_set_cell_data_func (column, renderer, 
			icon_set_func_text, NULL, NULL);
	
	renderer = gtk_cell_renderer_text_new ();
	gtk_tree_view_column_pack_start (column, renderer, TRUE);
	gtk_tree_view_column_set_cell_data_func (column, renderer, 
			name_set_func_text, NULL, NULL);
	
	gtk_tree_view_append_column (GTK_TREE_VIEW (sidebar), column);
	
	gtk_box_pack_start (GTK_BOX (hbox), scrolled, FALSE, FALSE, 0);
	od->sidebar = scrolled;
	
	vbox = gtk_vbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (hbox), vbox, TRUE, TRUE, 0);

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (sidebar));
	g_signal_connect (G_OBJECT (selection), "changed",
			G_CALLBACK (disc_type_changed_cb), vbox);
	
	/* set the disc type to the same type the user last worked with */
	
	valid = gtk_tree_model_get_iter_first (GTK_TREE_MODEL (store), &iter);
	while (valid)
	{
		gtk_tree_model_get (GTK_TREE_MODEL (store), &iter, 
				SIDEBAR_TYPE, &sidebar_type, -1);
		if (sidebar_type == last_type)
		{
			gtk_tree_selection_select_iter (selection, &iter);
			valid = FALSE;
		}
		else
			valid = gtk_tree_model_iter_next (GTK_TREE_MODEL (store), &iter);
	}
	
	/* FIXME: eventually use different icons for different disc types */

	gtk_widget_show_all (hbox);
	
	return hbox;
}

void
opti_new_disc (CDFileListType type)
{
	GtkWidget *box;
	
	box = build_disc_view (type);
	
	if (box)
	{
		gnome_app_set_contents (GNOME_APP (od->window_main), box);
		opti_set_menu_sensitivity (0);
	}
	
	return;
}

void
rename_file (void)
{
	cdfilelist_rename_selected (CDFILELIST (od->fl), NULL);
	
	return;
}

void
move_files (CDFileListDirection direction)
{
	cdfilelist_move_selected (CDFILELIST (od->fl), direction, NULL);
	
	return;
}
/*
static GList *
verify_paths_audio (GList *list)
{
	GList *new_list, *current, *removed;
	AudioFileEntry *entry;
	
	new_list = NULL;
	removed = NULL;
	
	for (current = list; current; current = current->next)
	{
		entry = current->data;
		if (g_file_test (entry->hd_path, G_FILE_TEST_EXISTS))
			new_list = g_list_prepend (new_list, entry);
		else
		{
			removed = g_list_prepend (removed, g_strdup (entry->hd_path));
			opti_audio_file_entry_free (entry, NULL);
		}
	}
	
	new_list = g_list_reverse (new_list);
	removed = g_list_reverse (removed);
	
	if (removed)
	{
//		opti_cd_display_invalid_files (removed);
	
		g_list_foreach (removed, (GFunc) g_free, NULL);
		g_list_free (removed);
		
		cdfilelist_set_modified (CDFILELIST (od->fl), TRUE);
	}
	
	g_list_free (list);
	
//	opti_set_menu_sensitivity ();
	
	return new_list;
}

static GList *
verify_paths_data (GList *list)
{
	GList *new_list, *current, *removed;
	DataFileEntry *entry;
	
	new_list = NULL;
	removed = NULL;
	
	for (current = list; current; current = current->next)
	{
		entry = current->data;
		if ((!strcmp (entry->hd_path, "folder")) || 
			(g_file_test (entry->hd_path, G_FILE_TEST_EXISTS)))
			new_list = g_list_prepend (new_list, entry);
		else
		{
			removed = g_list_prepend (removed, g_strdup (entry->hd_path));
			opti_data_file_entry_free (entry, NULL);
		}
	}
	
	new_list = g_list_reverse (new_list);
	removed = g_list_reverse (removed);
	
	if (removed)
	{
//		opti_cd_display_invalid_files (removed);
	
		g_list_foreach (removed, (GFunc) g_free, NULL);
		g_list_free (removed);
		
		cdfilelist_set_modified (CDFILELIST (od->fl), TRUE);
	}
	
	g_list_free (list);
	
//	opti_set_menu_sensitivity ();
	
	return new_list;
}


*/
/* ensure 'size_needed' bytes exist on the volume containing 'drive' */

gboolean
opti_check_drive_free_space (gulong size_needed, const gchar *drive)
{
	gboolean result;
	GnomeVFSURI *uri;
	GnomeVFSFileSize size;
	GnomeVFSResult vfs_result;
	
	result = FALSE;
	
	uri = gnome_vfs_uri_new (drive);
	if (uri)
	{
		vfs_result = gnome_vfs_get_volume_free_space (uri, &size);
		if (vfs_result == GNOME_VFS_OK)
		{
			if (size > size_needed)
				result = TRUE;
		}
	}
	
	return result;
}

/* return a uri to the user's cache directory */

static gchar *
get_cache_dir (void)
{
	gchar *dir, *temp;
	
	dir = gconf_client_get_string (od->gconf, "/apps/optimystic/cache_dir", NULL);
	if (dir && dir [0] == '~')
	{
		temp = g_strdup_printf ("%s/%s", g_get_home_dir (), dir + 1);
		g_free (dir);
		dir = temp;
	}
	
	if (!dir || dir [0] == ' ')
		dir = g_strdup_printf ("%s/.optimystic/cache", g_get_home_dir ());
	
	return dir;
}

/* get the user's cache directory, create it if it doesn't exist, and return
 * NULL if it couldn't be created
 */

gchar *
opti_cd_check_cache_dir (void)
{
	gchar *dir, *error, *path_to_dir;
	GnomeVFSResult result;
	gboolean exists;
	GList *list, *current;;
	
	dir = get_cache_dir ();
	
	/* Since gnome_vfs_make_directory can't make recursive directories,
	 * we'll figure out which dirs don't exist, then go back and make
	 * each one ourselves.
	 */
	list = NULL;
	exists = g_file_test (dir, G_FILE_TEST_EXISTS | G_FILE_TEST_IS_DIR);
	while (!exists)
	{
		path_to_dir = g_path_get_dirname (dir);
		list = g_list_prepend (list, path_to_dir);
		exists = g_file_test (path_to_dir, G_FILE_TEST_EXISTS | G_FILE_TEST_IS_DIR);
	}
	current = list;
	while (current)
	{
		result = gnome_vfs_make_directory (current->data, 0744);
		if (result != GNOME_VFS_OK)
		{
			error = g_strdup (gnome_vfs_result_to_string (result));
			opti_string_error (od->window_main, error);
			g_free (error);
		}
		g_free (current->data);
		current = g_list_next (list);
	}
	g_list_free (list);
	
	if (!g_file_test (dir, G_FILE_TEST_EXISTS | G_FILE_TEST_IS_DIR))
	{
		result = gnome_vfs_make_directory (dir, 0744);
		if (result != GNOME_VFS_OK)
		{
			error = g_strdup (gnome_vfs_result_to_string (result));
			opti_string_error (od->window_main, error);
			g_free (error);
			g_free (dir);
			dir = NULL;
		}
	}
	
	return dir;
}
