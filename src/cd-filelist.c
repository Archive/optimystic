/*
 *  Copyright 2003-2004 Todd Kulesza <todd@dropline.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h"

#include "icon-text-type.h"
#include "cd-filelist.h"

#include <string.h>
#include <time.h>
#include <gtk/gtktreeview.h>
#include <gtk/gtkliststore.h>
#include <gtk/gtkcellrenderertext.h>
#include <gtk/gtkcellrendererpixbuf.h>
#include <gtk/gtktreeselection.h>
#include <libgnomevfs/gnome-vfs-ops.h>
#include <libgnomevfs/gnome-vfs-mime-handlers.h>
#include <libgnomeui/gnome-icon-theme.h>
#include <libgnomeui/gnome-icon-lookup.h>
#include <libxml/tree.h>
#include <libxml/parser.h>
#include <gst/gst.h>
#include <gst/play/play.h>

#define FOLDER_KEYWORD	   "folder"
#define ICON_SIZE	       24.0

typedef struct _FileEntry   FileEntry;

struct _FileEntry
{
	gchar *hd_uri;
	gchar *cd_uri;
	gint position;		/* position of audio tracks */
	gint length;		/* length of audio tracks */
};

enum
{
	POSITION_COLUMN,
	NAME_COLUMN,
	SIZE_COLUMN,
	DIR_COLUMN,
	EDITABLE_COLUMN,
	N_COLUMNS
};

enum
{
	TARGET_STRING,
	TARGET_URL
};

enum
{
	REFRESH_SIGNAL,
	PATH_CHANGED_SIGNAL,
	FILES_CHANGED_SIGNAL,
	NAME_CHANGED_SIGNAL,
	LAST_SIGNAL
};

enum
{
	SORT_DEFAULT
};

static guint cdfilelist_signals [LAST_SIGNAL] = { 0 };

static const GtkTargetEntry target_table [] =
{
	{ "STRING",        0, TARGET_STRING },
	{ "text/plain",    0, TARGET_STRING },
	{ "text/uri-list", 0, TARGET_URL }
};

static gint
file_entry_cmp (const FileEntry *a, const FileEntry *b)
{
	return (strcmp (a->cd_uri, b->cd_uri));
}

static gint
file_entry_cd_vs_uri_cmp (const FileEntry *fe, const gchar *uri)
{
	return (strcmp (fe->cd_uri, uri));
}

static gint
file_entry_hd_vs_uri_cmp (const FileEntry *fe, const gchar *uri)
{
	return (strcmp (fe->hd_uri, uri));
}

static void
file_entry_free (FileEntry *fe)
{
	g_free (fe->hd_uri);
	g_free (fe->cd_uri);
	g_free (fe);
	
	return;
}

static gboolean
is_supported_audio (CDFileList *list, const gchar *mime_type)
{
	gboolean retval;
	
	if (g_list_find_custom (list->audio_types, mime_type, (GCompareFunc) strcmp))
		retval = TRUE;
	else
		retval = FALSE;
	
	return retval;
}

/* Determine if the mime type of 'info' is supported by the disc */

static gboolean
mime_type_is_valid (CDFileList *list, GnomeVFSFileInfo *info, CDFileListType type)
{
	gboolean result;
	
	/* FIXME: do mime-type checking here to ensure the file can be added to the disc */
	
	result = TRUE;
	
	switch (type)
	{
		case CDFILELIST_DATA:
		{
			break;
		}
		case CDFILELIST_AUDIO:
		{
			result = is_supported_audio (list, info->mime_type);
			break;
		}
		default: {}
	}
	
	return result;
}

/* Format 'seconds' into a user-readable string */

static gchar*
get_time_string (time_t seconds)
{
	struct tm *tm = gmtime (&seconds);
	gchar *time;

	time = g_malloc (128);
	if (seconds > 3600) {
		/* include the hours here */
		if (!seconds ||
			strftime (time, 128, _("%H:%M:%S"), tm) <= 0 )
				strcpy (time, _("--:--:--"));
	} else {
		/* just minutes and seconds */
		if (!seconds ||
			strftime (time, 128, _("%M:%S"), tm) <= 0)
				strcpy (time, _("--:--"));
	}
	
	return time;
}

/* Return a formatted string containing the length of the audio file */

static gchar *
get_size_audio (gint seconds)
{
	gchar *length;
	
	length = get_time_string ((time_t) seconds);

	return length;
}

/* Return a formatted string containing the size of the data file */

static gchar *
get_size_data (GnomeVFSFileInfo *info)
{
	gchar *size;
	
	if (g_ascii_strcasecmp (info->mime_type, "x-directory/normal"))
		size = g_strdup_printf ("%lld K", info->size / 1024);
	else
		size = NULL;
	
	return size;
}

/* Return true if 'info' is a directory */

static gboolean
get_is_dir_data (GnomeVFSFileInfo *info)
{
	gboolean is_dir;
	
	if (g_ascii_strcasecmp (info->mime_type, "x-directory/normal"))
		is_dir = FALSE;
	else
		is_dir = TRUE;
	
	return is_dir;
}

/* Get a uri for the icon associated with "filename's" mimetype */
/* Based on code from gnome-search-tool 2.2 */

gchar *
get_file_icon_with_mime_type (const gchar *filename, const gchar *mimetype, GtkIconTheme *icon_theme)
{
	GtkIconInfo *icon_info;
	gchar *icon_name = NULL;	
	gchar *icon_path = NULL;
	const gchar *path;

	if (filename == NULL || mimetype == NULL) {
		icon_name = g_strdup ("gnome-fs-regular");
	}
	else if ((g_file_test (filename, G_FILE_TEST_IS_EXECUTABLE) == TRUE) &&
	         (g_ascii_strcasecmp (mimetype, "application/x-executable-binary") == 0)) {
		icon_name = g_strdup ("gnome-fs-executable");
	}
	else {
		icon_name = gnome_icon_lookup (icon_theme, NULL, filename, NULL, 
					       NULL, mimetype, 0, NULL);
	}

	if (icon_name)
	{
		icon_info = gtk_icon_theme_lookup_icon (icon_theme, icon_name, ICON_SIZE, 0);
		path = gtk_icon_info_get_filename (icon_info);
		if (path)
			icon_path = g_strdup (path);
	
		gtk_icon_info_free (icon_info);
		g_free (icon_name);
	}
	
	return icon_path;	
}

/* Refresh the CDFileList, displaying the files residing in the current 'path' */

/* FIXME: this function gets called *way* more than is healthy */

static void
refresh_view (CDFileList *list)
{
	GList *current;
	FileEntry *entry;
	FileDisplay *fd;
	gchar *base_uri, *size, *icon_path;
	GnomeVFSFileInfo info;
	GnomeVFSResult result;
	GtkTreeIter iter;
	gint base_uri_len;
	gboolean is_dir;
	GdkPixbuf *icon;
	
	gtk_list_store_clear (list->store);
	
	base_uri_len = strlen (list->path_string);
	
	for (current = list->files; current; current = current->next)
	{
		entry = current->data;
		base_uri = g_path_get_dirname (entry->cd_uri);
		
		if (!strcmp (base_uri, list->path_string))
		{
			if (!strcmp (entry->hd_uri, FOLDER_KEYWORD))
			{
				result = GNOME_VFS_OK;
				info.mime_type = g_strdup ("x-directory/normal");
				info.size = 0;
			}
			else
			{
				result = gnome_vfs_get_file_info (entry->hd_uri, &info, 
						GNOME_VFS_FILE_INFO_GET_MIME_TYPE |
						GNOME_VFS_FILE_INFO_FOLLOW_LINKS);
			}
			
			if (result == GNOME_VFS_OK)
			{
				switch (list->type)
				{
					case CDFILELIST_AUDIO:
					{
						size = get_size_audio (entry->length);
						is_dir = FALSE;
						break;
					}
					case CDFILELIST_DATA:
					{
						size = get_size_data (&info);
						is_dir = get_is_dir_data (&info);
						break;
					}
					default:
					{
						size = NULL;
						is_dir = FALSE;
					}
				}
				
				icon_path = get_file_icon_with_mime_type (entry->hd_uri, info.mime_type, list->icon_theme);
			}
			else
			{
				size = g_strdup ("-");
				is_dir = FALSE;
				icon_path = NULL;
			}

			fd = file_display_new ();
			fd->name = g_strdup_printf ("%s", entry->cd_uri + base_uri_len + 1);
			if (icon_path)
			{
				icon = gdk_pixbuf_new_from_file (icon_path, NULL);
				fd->icon = gdk_pixbuf_scale_simple (icon, ICON_SIZE, ICON_SIZE, GDK_INTERP_BILINEAR);
				g_object_unref (G_OBJECT (icon));
			}
			
			gtk_list_store_append (list->store, &iter);
			gtk_list_store_set (list->store, &iter,
					NAME_COLUMN, fd,
					SIZE_COLUMN, size,
					DIR_COLUMN, is_dir,
					POSITION_COLUMN, entry->position,
					EDITABLE_COLUMN, FALSE,
					-1);
			
			file_display_free (fd);
			g_free (size);
		}
		
		g_free (base_uri);
	}
	
	return;
}

/* Handle the user activating (double-clicking) a row of the CDFileList.
 * If the row is a folder, enter it.
 */

static void
row_activated (GtkWidget *view, GtkTreePath *path, GtkTreeViewColumn *column, gpointer data)
{
	GtkTreeModel *model;
	GtkTreeIter iter;
	gboolean is_dir;
	CDFileList *list;
	FileDisplay *fd;
	
	list = CDFILELIST (data);
	
	model = gtk_tree_view_get_model (GTK_TREE_VIEW (view));	
	gtk_tree_model_get_iter (model, &iter, path);
	gtk_tree_model_get (model, &iter, 
			DIR_COLUMN, &is_dir,
			NAME_COLUMN, &fd,
			-1);
	
	if (is_dir)
	{
		cdfilelist_path_add (list, fd->name);
		g_signal_emit_by_name (G_OBJECT (list), "refresh");
	}
	
	file_display_free (fd);
	
	return;
}

static gint
int_cmp_ascending (const gint *a, const gint *b)
{
	return (*a - *b);
}

static gint
int_cmp_descending (const gint *a, const gint *b)
{
	return (*b - *a);
}

/* Select each list entry with a 'position' value in 'rows' */

static void
reselect_rows_by_position (CDFileList *list, GList *rows)
{
	GtkTreeSelection *selection;
	GtkTreeIter iter;
	GtkTreePath *path;
	gboolean valid;
	gint pos;
	
	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (list));
	valid = gtk_tree_model_get_iter_first (GTK_TREE_MODEL (list->store), &iter);
	while (valid)
	{
		gtk_tree_model_get (GTK_TREE_MODEL (list->store), &iter, 
				POSITION_COLUMN, &pos, -1);
		
		if (g_list_find_custom (rows, &pos, (GCompareFunc) int_cmp_ascending))
		{
			path = gtk_tree_model_get_path (GTK_TREE_MODEL (list->store), &iter);
			gtk_tree_selection_select_path (selection, path);
			gtk_tree_path_free (path);
		}
		
		valid = gtk_tree_model_iter_next (GTK_TREE_MODEL (list->store), &iter);
	}
		
	return;
}

/* Select each list entry with a 'name' value in 'rows' */

static void
reselect_rows_by_name (CDFileList *list, GList *rows)
{
	GtkTreeSelection *selection;
	GtkTreeIter iter;
	GtkTreePath *path;
	gboolean valid;
	FileDisplay *fd;
	
	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (list));
	valid = gtk_tree_model_get_iter_first (GTK_TREE_MODEL (list->store), &iter);
	while (valid)
	{
		gtk_tree_model_get (GTK_TREE_MODEL (list->store), &iter, 
				NAME_COLUMN, &fd, -1);
		
		if (g_list_find_custom (rows, fd->name, (GCompareFunc) strcmp))
		{
			path = gtk_tree_model_get_path (GTK_TREE_MODEL (list->store), &iter);
			gtk_tree_selection_select_path (selection, path);
			gtk_tree_path_free (path);
		}
		
		valid = gtk_tree_model_iter_next (GTK_TREE_MODEL (list->store), &iter);
	}
	
	return;
}

/* Rename all instances of 'old_uri' to 'new_uri' */

static void
rename_files (CDFileList *list, const gchar *old_uri, const gchar *new_uri)
{
	GList *current;
	FileEntry *entry;
	gint len_cd_uri, len_uri;
	gchar *old_cd_uri;
	gboolean modified;
	
	len_uri = strlen (old_uri);
	modified = FALSE;
	
	for (current = list->files; current; current = current->next)
	{
		entry = current->data;
		
		if (!strncmp (entry->cd_uri, old_uri, len_uri))
		{
			len_cd_uri = strlen (entry->cd_uri);
			
			/* If the lenths are the same the files are the same.  If the
			 * character following the uri is a G_DIR_SEPARATOR, the file is
			 * in the folder 'old_uri' and must be renamed as well.
			 */
			if ( (len_cd_uri == len_uri) ||
				 (entry->cd_uri [len_uri] == G_DIR_SEPARATOR) )
			{
				old_cd_uri = entry->cd_uri;
				entry->cd_uri = g_strdup_printf ("%s%s", new_uri, old_cd_uri + len_uri);
				modified = TRUE;
				g_free (old_cd_uri);
			}
		}
	}
	
	if (modified)
		cdfilelist_set_modified (list, TRUE);
	
	return;
}

/* Callback to handle the user editing a text cell in CDFileList */

static void
edit_name_cb (GtkCellRendererText *cell, const gchar *path_string, const gchar *new_name, gpointer data)
{
	CDFileList *list;
	FileDisplay *fd;
	GtkTreePath *path;
	GtkTreeIter iter;
	GList *selected;
	gchar *new_uri, *old_uri;
	
	list = CDFILELIST (data);
	selected = NULL;
	path = gtk_tree_path_new_from_string (path_string);
	
	gtk_tree_model_get_iter (GTK_TREE_MODEL (list->store), &iter, path);
	gtk_tree_model_get (GTK_TREE_MODEL (list->store), &iter, NAME_COLUMN, &fd, -1);
	
	old_uri = g_strdup_printf ("%s%c%s", list->path_string, G_DIR_SEPARATOR, fd->name);
	new_uri = g_strdup_printf ("%s%c%s", list->path_string, G_DIR_SEPARATOR, new_name);
	
	selected = g_list_prepend (selected, g_strdup (new_name));
	
	rename_files (list, old_uri, new_uri);
	
	gtk_list_store_set (list->store, &iter, EDITABLE_COLUMN, FALSE, -1);
	
	g_signal_emit_by_name (G_OBJECT (list), "refresh");
	
	reselect_rows_by_name (list, selected);
	
	g_free (old_uri);
	g_free (new_uri);
	file_display_free (fd);
	gtk_tree_path_free (path);
	g_list_foreach (selected, (GFunc) g_free, NULL);
	g_list_free (selected);
	
	return;
}

/* Sort the CDFileList.
 * Data: Alphebetical, folders first.
 * Audio: Numerical, by track number.
 */

gint
sort_default (GtkTreeModel *model, GtkTreeIter *a, GtkTreeIter *b, gpointer data)
{
	CDFileList *list;
	FileDisplay *fd1, *fd2;
	gint pos1, pos2, retval;
	gboolean dir1, dir2;
	
	list = CDFILELIST (data);
	
	gtk_tree_model_get (model, a, 
			POSITION_COLUMN, &pos1,
			NAME_COLUMN, &fd1, 
			DIR_COLUMN, &dir1,
			-1);
	gtk_tree_model_get (model, b, 
			POSITION_COLUMN, &pos2,
			NAME_COLUMN, &fd2, 
			DIR_COLUMN, &dir2,
			-1);
	
	switch (list->type)
	{
		case CDFILELIST_AUDIO:
		{
			retval = pos1 - pos2;
			break;
		}
		case CDFILELIST_DATA:
		{
			if (dir1 && !dir2)
				retval = -1;
			else if (!dir1 && dir2)
				retval = 1;
			else if (fd1 && fd1->name && fd2 && fd2->name)
				retval = strcmp (fd1->name, fd2->name);
			else
				retval = 0;
			break;
		}
		default:
		{
			retval = 0;
		}
	}
	
	file_display_free (fd1);
	file_display_free (fd2);
	
	return retval;
}

static void
icon_set_func_text (GtkTreeViewColumn *col, GtkCellRenderer *renderer, GtkTreeModel *model, GtkTreeIter *iter, gpointer data)
{
	FileDisplay *fd;
	
	gtk_tree_model_get (model, iter, NAME_COLUMN, &fd, -1);
	
	if (fd && fd->icon)
		g_object_set (renderer, "pixbuf", fd->icon, NULL);
	else
		g_object_set (renderer, "pixbuf", NULL, NULL);
	
	file_display_free (fd);
	
	return;
}

static void
name_set_func_text (GtkTreeViewColumn *col, GtkCellRenderer *renderer, GtkTreeModel *model, GtkTreeIter *iter, gpointer data)
{
	FileDisplay *fd;
	gboolean editable;
	
	gtk_tree_model_get (model, iter, 
			NAME_COLUMN, &fd,
			EDITABLE_COLUMN, &editable,
			-1);
	
	if (fd && fd->name)
		g_object_set (renderer, "text", fd->name, "editable", editable, NULL);
	
	file_display_free (fd);
	
	return;
}

static void
build_view_data (CDFileList *list)
{
	GtkCellRenderer *renderer;
	GtkTreeViewColumn *col;
	
	/* File name & icon */
	col = gtk_tree_view_column_new ();
	gtk_tree_view_column_set_title (col, _("File name"));
	
	renderer = gtk_cell_renderer_pixbuf_new ();
	gtk_tree_view_column_pack_start (col, renderer, FALSE);
	gtk_tree_view_column_set_cell_data_func (col, renderer, 
			icon_set_func_text, NULL, NULL);
	
	renderer = gtk_cell_renderer_text_new ();
	gtk_tree_view_column_pack_start (col, renderer, TRUE);
	gtk_tree_view_column_set_cell_data_func (col, renderer, 
			name_set_func_text, NULL, NULL);
	g_signal_connect (G_OBJECT (renderer), "edited",
			G_CALLBACK (edit_name_cb), list);
	
	gtk_tree_view_append_column (GTK_TREE_VIEW (list), col);
	
	/* File size */
	renderer = gtk_cell_renderer_text_new ();
	col = gtk_tree_view_column_new_with_attributes (
			_("Size"), renderer, 
			"text", SIZE_COLUMN, 
			NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (list), col);
	
	return;
}

static void
build_view_audio (CDFileList *list)
{
	GtkCellRenderer *renderer;
	GtkTreeViewColumn *col;
	
	/* Track position */
	renderer = gtk_cell_renderer_text_new ();
	col = gtk_tree_view_column_new_with_attributes (
			_("Track"), renderer, 
			"text", POSITION_COLUMN, 
			NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (list), col);
	
	/* Track name & icon */
	col = gtk_tree_view_column_new ();
	gtk_tree_view_column_set_title (col, _("Title"));
	
	renderer = gtk_cell_renderer_pixbuf_new ();
	gtk_tree_view_column_pack_start (col, renderer, FALSE);
	gtk_tree_view_column_set_cell_data_func (col, renderer, 
			icon_set_func_text, NULL, NULL);
	
	renderer = gtk_cell_renderer_text_new ();
	gtk_tree_view_column_pack_start (col, renderer, TRUE);
	gtk_tree_view_column_set_cell_data_func (col, renderer, 
			name_set_func_text, NULL, NULL);
	g_signal_connect (G_OBJECT (renderer), "edited",
			G_CALLBACK (edit_name_cb), list);
	
	gtk_tree_view_append_column (GTK_TREE_VIEW (list), col);
	
	/* Track length */
	renderer = gtk_cell_renderer_text_new ();
	col = gtk_tree_view_column_new_with_attributes (
			_("Length"), renderer, 
			"text", SIZE_COLUMN, 
			NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (list), col);
	
	return;
}

static void
gst_stream_end (GstPlay *play, gpointer data)
{
	gst_element_set_state (GST_ELEMENT (play), GST_STATE_NULL);
	
	return;
}

static GstPlay*
build_gst_player (void)
{
	GstPlay *player;
	GstElement *src, *sink;
	GError *error;
	
	error = NULL;

	player = gst_play_new (&error);
	if (error)
		g_warning ("Could not create GstPlay object.\n");
	else
	{
		src = gst_element_factory_make ("gnomevfssrc", "source");
		sink = gst_element_factory_make ("fakesink", "sink");
		
		gst_play_set_data_src (player, src);
		gst_play_set_audio_sink (player, sink);
		gst_element_set_state (GST_ELEMENT (player), GST_STATE_NULL);
		
		g_signal_connect (G_OBJECT (player), "stream_end",
				G_CALLBACK (gst_stream_end), NULL);
	}

	return player;
}

static void
cdfilelist_class_init (CDFileListClass *klass)
{
	cdfilelist_signals [REFRESH_SIGNAL] = g_signal_new ("refresh",
			G_TYPE_FROM_CLASS (klass),
			G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
			G_STRUCT_OFFSET (CDFileListClass, refresh),
			NULL,
			NULL,
			g_cclosure_marshal_VOID__VOID,
			G_TYPE_NONE, 0);
	
	cdfilelist_signals [PATH_CHANGED_SIGNAL] = g_signal_new ("path_changed",
			G_TYPE_FROM_CLASS (klass),
			G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
			G_STRUCT_OFFSET (CDFileListClass, path_changed),
			NULL,
			NULL,
			g_cclosure_marshal_VOID__VOID,
			G_TYPE_NONE, 0);
	
	cdfilelist_signals [PATH_CHANGED_SIGNAL] = g_signal_new ("files_changed",
			G_TYPE_FROM_CLASS (klass),
			G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
			G_STRUCT_OFFSET (CDFileListClass, files_changed),
			NULL,
			NULL,
			g_cclosure_marshal_VOID__BOOLEAN,
			G_TYPE_NONE, 1,
			G_TYPE_BOOLEAN);
	
	cdfilelist_signals [PATH_CHANGED_SIGNAL] = g_signal_new ("name_changed",
			G_TYPE_FROM_CLASS (klass),
			G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
			G_STRUCT_OFFSET (CDFileListClass, name_changed),
			NULL,
			NULL,
			g_cclosure_marshal_VOID__STRING,
			G_TYPE_NONE, 1,
			G_TYPE_STRING);
	
	return;
}

static void
cdfilelist_init (CDFileList *list)
{
	GtkTreeSelection *selection;
	
	list->icon_theme = gtk_icon_theme_get_default ();
	
	list->store = gtk_list_store_new (N_COLUMNS,
			G_TYPE_INT,			/* Position */
			FILE_DISPLAY_TYPE,	/* Name & Icon */
			G_TYPE_STRING,		/* Size */
			G_TYPE_BOOLEAN,		/* Folder */
			G_TYPE_BOOLEAN);	/* Editable */
	gtk_tree_view_set_model (GTK_TREE_VIEW (list), GTK_TREE_MODEL (list->store));
	
	/* Allow multiple selections */
	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (list));
	gtk_tree_selection_set_mode (selection, GTK_SELECTION_MULTIPLE);
	
	/* Set the default sorting function */
	gtk_tree_sortable_set_sort_func (GTK_TREE_SORTABLE (list->store),
			SORT_DEFAULT, sort_default, list, NULL);
	gtk_tree_sortable_set_default_sort_func (GTK_TREE_SORTABLE (list->store),
			sort_default, list, NULL);
	gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE (list->store),
			SORT_DEFAULT, GTK_SORT_ASCENDING);
	
	list->type = CDFILELIST_DATA;
	list->files = NULL;
	/* Don't translate this string; it gets used in file path names */
	list->path = g_list_prepend (list->path, g_strdup ("Disc"));
	list->path_string = cdfilelist_path_get_string (list);
	list->filename = NULL;
	list->title = g_strdup (_("Untitled Disc"));
	list->modified = FALSE;
	list->audio_types = NULL;
	
	list->length_mutex = g_mutex_new ();
	list->length_files = NULL;
	list->length_uri = NULL;
	list->length_id_timeout = 0;
	list->length_id_cb = 0;
	
	g_signal_connect (G_OBJECT (list), "refresh",
			G_CALLBACK (refresh_view), NULL);
	g_signal_connect (G_OBJECT (list), "row_activated",
			G_CALLBACK (row_activated), list);
	
	return;
}

GType
cdfilelist_get_type (void)
{
	static GType cdfilelist_type = 0;
	
	if (!cdfilelist_type)
	{
		static const GTypeInfo cdfilelist_info =
		{
			sizeof (CDFileListClass),
			NULL,	/* base_init */
			NULL,	/* base_finalize */
			(GClassInitFunc) cdfilelist_class_init,
			NULL,	/* class_finalize */
			NULL,	/* class_data */
			sizeof (CDFileList),
			0,		/* n_preallocs */
			(GInstanceInitFunc) cdfilelist_init,
		};
			
		
		cdfilelist_type = g_type_register_static (GTK_TYPE_TREE_VIEW, 
				"CDFileList",
				&cdfilelist_info,
				0);
	}
	
	return cdfilelist_type;
}

/* Build a new CDFileList widget */

GtkWidget *
cdfilelist_new (CDFileListType type)
{
	CDFileList *list;
	
	list = g_object_new (TYPE_CDFILELIST, NULL);
	list->type = type;
	
	switch (list->type)
	{
		case CDFILELIST_DATA:
		{
			build_view_data (list);
			list->player = NULL;
			break;
		}
		case CDFILELIST_AUDIO:
		{
			build_view_audio (list);
			list->player = build_gst_player ();
			break;
		}
		default:
		{
			g_warning ("cdfilelist_new: 'type' is not a valid CDFileListType");
			break;
		}
	}

	return (GTK_WIDGET (list));
}

/* Set whether the CDFileList has been modified since the last save */

void
cdfilelist_set_modified (CDFileList *list, gboolean modified)
{
	g_return_if_fail (IS_CDFILELIST (list));
	
	if (modified != list->modified)
	{
		list->modified = modified;
		g_signal_emit_by_name (G_OBJECT (list), "files_changed", modified);
	}
	
	return;
}

/* Return TRUE if the CDFileList has been modified since the last save */

gboolean
cdfilelist_get_modified (CDFileList *list)
{
	g_return_val_if_fail (IS_CDFILELIST (list), FALSE);
	
	return (list->modified);
}

/* Return the type of disc; audio, data, etc. */

CDFileListType
cdfilelist_get_cd_type (CDFileList *list)
{
	g_return_val_if_fail (IS_CDFILELIST (list), CDFILELIST_UNKNOWN);
	
	return (list->type);
}

/* Set the CDFileList's 'filename' */

void
cdfilelist_set_property (CDFileList *list, CDFileListProp property, const gchar *value)
{
	g_return_if_fail (IS_CDFILELIST (list));
	
	if (!value)
		value = g_strdup ("");
	
	switch (property)
	{
		case CDFILELIST_FILE:
		{
			g_free (list->filename);
			list->filename = g_strdup (value);
			g_signal_emit_by_name (G_OBJECT (list), "name_changed", list->filename);
			break;
		}
		case CDFILELIST_TITLE:
		{
			g_free (list->title);
			list->title = g_strdup (value);
			break;
		}
		case CDFILELIST_PUB:
		{
			g_free (list->publisher);
			list->publisher = g_strdup (value);
			break;
		}
		case CDFILELIST_CR:
		{
			g_free (list->copyright);
			list->copyright = g_strdup (value);
			break;
		}
		default:
			g_warning ("Unknown property: %d\n", property);
	}
	
	return;
}

/* Return the CDFileList's 'filename' */

const gchar*
cdfilelist_get_property (CDFileList *list, CDFileListProp property)
{
	const gchar *value;
	
	g_return_val_if_fail (IS_CDFILELIST (list), NULL);
	
	switch (property)
	{
		case CDFILELIST_FILE:
		{
			value = list->filename;
			break;
		}
		case CDFILELIST_TITLE:
		{
			value = list->title;
			break;
		}
		case CDFILELIST_PUB:
		{
			value = list->publisher;
			break;
		}
		case CDFILELIST_CR:
		{
			value = list->copyright;
			break;
		}
		default:
		{
			value = NULL;
			g_warning ("Unknown property: %d\n", property);
		}
	}
	
	return (value);
}

/* Return TRUE if there are no files on the disc */

gboolean
cdfilelist_get_is_empty (CDFileList *list)
{
	g_return_val_if_fail (IS_CDFILELIST (list), TRUE);
	
	return (list->files == NULL);
}

/* Add one folder level to the current disc path */

void
cdfilelist_path_add (CDFileList *list, const gchar *folder)
{
	g_return_if_fail (IS_CDFILELIST (list));
	g_return_if_fail (folder);
	
	list->path = g_list_prepend (list->path, g_strdup (folder));
	list->path_string = cdfilelist_path_get_string (list);
	
	g_signal_emit_by_name (G_OBJECT (list), "refresh");
	g_signal_emit_by_name (G_OBJECT (list), "path_changed");
	
	return;
}

/* Move the current disc path up one folder level */

void
cdfilelist_path_up (CDFileList *list)
{
	GList *item;
	
	g_return_if_fail (IS_CDFILELIST (list));
	g_return_if_fail (g_list_length (list->path) > 1);
	
	/* remove the leading element */
	item = list->path;
	list->path = g_list_remove_link (list->path, item);
	g_free (item->data);
	g_list_free_1 (item);
	
	list->path_string = cdfilelist_path_get_string (list);
	
	g_signal_emit_by_name (G_OBJECT (list), "refresh");
	g_signal_emit_by_name (G_OBJECT (list), "path_changed");
	
	return;
}

/* Return the current path of the disc with each folder occupying a seperate
 * element of an array of gchar arrays. */

gchar **
cdfilelist_path_get_array (CDFileList *list)
{
	gchar **path;
	GList *current;
	gint i;
	
	g_return_val_if_fail (IS_CDFILELIST (list), NULL);
	
	list->path = g_list_reverse (list->path);
	
	path = g_new (gchar *, g_list_length (list->path) + 1);
	for (current = list->path, i = 0; current; current = current->next)
		path [i++] = g_strdup (current->data);
	path [i] = NULL;
	
	list->path = g_list_reverse (list->path);
	
	return path;
}

/* Return the current path of the disc as a gchar array */

gchar *
cdfilelist_path_get_string (CDFileList *list)
{
	gchar *path, **path_array;
	
	g_return_val_if_fail (IS_CDFILELIST (list), NULL);
	
	path_array = cdfilelist_path_get_array (list);
	path = g_strjoinv (G_DIR_SEPARATOR_S, path_array);
	g_strfreev (path_array);
	
	return path;
}

/* Lookup the FileEntry struct for the uri we got the length of and update
 * its 'length' attribute to reflect this.
 */

static void
gst_stream_length (GstPlay *player, gint64 length_nanos, CDFileList *list)
{	
	GList *item;
	FileEntry *entry;
	gint seconds;
	
	g_mutex_lock (list->length_mutex);
	
	seconds = (gint) (length_nanos / GST_SECOND);
	gst_element_set_state (GST_ELEMENT (player), GST_STATE_NULL);
	
	item = g_list_find_custom (list->files, list->length_uri, 
			(GCompareFunc) file_entry_hd_vs_uri_cmp);
	if (item)
	{
		entry = item->data;
		entry->length = seconds;
	}
	
	g_free (list->length_uri);
	list->length_uri = NULL;
	g_signal_handler_disconnect (G_OBJECT (list->player), list->length_id_cb);
	list->length_id_cb = 0;
	
	g_mutex_unlock (list->length_mutex);
	
	return;
}

/* Called from a timeout loop by get_file_length().  If there are files we
 * need to get the length of, setup a GstPlay object and wait for the
 * "stream_length" signal.  If not and last call to gst_stream_length() has
 * finished, refresh the CDFileList and return.
 */

static gboolean
get_file_length_cb (CDFileList *list)
{
	gboolean retval;
	GList *head;
	
	g_mutex_lock (list->length_mutex);
	
	retval = TRUE;
	
	if (list->length_files)
	{
		if (!list->length_uri)
		{
			head = list->length_files;
			list->length_uri = head->data;
			list->length_files = g_list_remove_link (list->length_files, head);
			g_list_free_1 (head);
			
			gst_element_set_state (GST_ELEMENT (list->player), GST_STATE_NULL);
			gst_play_set_location (list->player, list->length_uri);
			list->length_id_cb = g_signal_connect (G_OBJECT (list->player),
					"stream_length", G_CALLBACK (gst_stream_length), list);
			gst_element_set_state (GST_ELEMENT (list->player), GST_STATE_PLAYING);
		}
	}
	else if (!list->length_id_cb)
	{
		/* we're done, so kill this timeout and refresh the CDFileList */
		refresh_view (list);
		list->length_id_timeout = 0;
		retval = FALSE;
	}
	
	g_mutex_unlock (list->length_mutex);
	
	return retval;
}

/* Add 'uri' to the list of files we need to get the length of.  If 
 * get_file_length_cb() isn't already running in a timeout loop, start it.
 */

static void
get_file_length (CDFileList *list, const gchar *uri)
{
	g_mutex_lock (list->length_mutex);
	list->length_files = g_list_prepend (list->length_files, g_strdup (uri));
	if (!list->length_id_timeout)
		list->length_id_timeout = g_timeout_add (200, (GSourceFunc) get_file_length_cb, list);
	g_mutex_unlock (list->length_mutex);
	
	return;
}

static void
build_error (GError **error, const gchar *mesg)
{
	GQuark q;
	
	g_warning (mesg);
		
	if (error && mesg)
	{
		q = g_quark_from_string ("Optimystic");
		*error = g_error_new (q, 0, mesg);
	}
	
	return;
}

/* Add 'uri' to the disc at the current path, first eliminating 'uri_base' 
 * from the uri as displayed on the disc.  For example, adding /usr/bin/bash to
 * a disc with a 'uri_base' of /usr would name the file /bin/bash on the disc. */

gboolean
cdfilelist_file_add (CDFileList *list, const gchar *uri, const gchar *uri_base, GError **error)
{
	GnomeVFSFileInfo info;
	GnomeVFSResult result;
	gchar *err_str, *file;
	gboolean retval;
	FileEntry *fe;
	
	g_return_val_if_fail (IS_CDFILELIST (list), FALSE);
	g_return_val_if_fail (uri, FALSE);
	g_return_val_if_fail (uri_base, FALSE);
	
	retval = FALSE;
	err_str = NULL;
	file = g_path_get_basename (uri);
	
	result = gnome_vfs_get_file_info (uri, &info, 
			GNOME_VFS_FILE_INFO_DEFAULT |
			GNOME_VFS_FILE_INFO_GET_MIME_TYPE |
			GNOME_VFS_FILE_INFO_FOLLOW_LINKS);
	
	if (result != GNOME_VFS_OK)
	{
		switch (result)
		{
			case GNOME_VFS_ERROR_NOT_FOUND:
			{
				err_str = g_strdup_printf (_("The file '%s' could not be "
						"found."), file);
				break;
			}
			case GNOME_VFS_ERROR_ACCESS_DENIED:
			{
				err_str = g_strdup_printf (_("You do not have permission to "
						"access '%s'."), file);
				break;
			}
			default:
			{
				err_str = g_strdup_printf (_("Unable to open the file '%s'."),
						file);
				break;
			}
		}
	}
	else if (mime_type_is_valid (list, &info, list->type))
	{
		fe = g_new0 (FileEntry, 1);
		if (!g_ascii_strcasecmp (info.mime_type, "x-directory/normal"))
			fe->hd_uri = g_strdup (FOLDER_KEYWORD);
		else
			fe->hd_uri = g_strdup (uri);
		fe->cd_uri = g_strdup_printf ("%s%c%s", list->path_string,
				G_DIR_SEPARATOR, uri + strlen (uri_base) + 1);
		if (list->type == CDFILELIST_AUDIO)
			fe->position = g_list_length (list->files) + 1;
		else
			fe->position = 0;
		
		if (list->type == CDFILELIST_AUDIO)
		{
			get_file_length (list, uri);
		}
		
		if (!g_list_find_custom (list->files, fe, (GCompareFunc) file_entry_cmp))
		{
			/* Add FileEntry to the disc */
			list->files = g_list_prepend (list->files, fe);
			cdfilelist_set_modified (list, TRUE);
			retval = TRUE;
		}
		else
		{
			err_str = g_strdup_printf (_("Could not add '%s' because another "
					"file on the disc has the same name."), file);
			file_entry_free (fe);
		}
	}
	else
	{
		g_warning ("MIME type %s is not supported.", info.mime_type);
		err_str = g_strdup_printf (_("The file '%s' is not supported by this "
				"type of disc."), file);
	}
	
	if (err_str)
		build_error (error, err_str);

	g_free (err_str);
	
	g_signal_emit_by_name (G_OBJECT (list), "refresh");
	
	return retval;
}

/* Remove 'uri' from the disc. */

static gboolean
remove_file (CDFileList *list, const gchar *uri, const gint pos)
{
	gboolean result;
	gint len_uri, len_cd_uri;
	GList *current, *to_remove;
	FileEntry *entry;
	
	len_uri = strlen (uri);
	to_remove = NULL;
	result = FALSE;
	
	for (current = list->files; current; current = current->next)
	{
		entry = current->data;
		
		if (entry->position > pos)
			entry->position--;
		if (!strncmp (entry->cd_uri, uri, len_uri))
		{
			len_cd_uri = strlen (entry->cd_uri);
			
			/* If the lenths are the same the files are the same.  If the
			 * character following the uri is a G_DIR_SEPARATOR, the file is
			 * in the folder 'uri' and must be removed as well.
			 */
			if ( (len_cd_uri == len_uri) ||
				 (entry->cd_uri [len_uri] == G_DIR_SEPARATOR) )
			{
				to_remove = g_list_prepend (to_remove, entry);
			}
		}
	}
	
	if (to_remove)
		result = TRUE;
	
	for (current = to_remove; current; current = current->next)
	{
		entry = current->data;
		
		list->files = g_list_remove (list->files, entry);
		file_entry_free (entry);
	}
	
	g_list_free (to_remove);
	
	return result;
}

/* Remove the currently selected files from the disc */

gboolean
cdfilelist_remove_selected (CDFileList *list, GError **error)
{
	GtkTreeSelection *selection;
	GtkTreeModel *model;
	GtkTreePath *path;
	GtkTreeIter iter;
	GList *selected, *current;
	gchar *disc_path, *uri;
	gboolean retval;
	gint pos, i;
	FileDisplay *fd;
	
	g_return_val_if_fail (IS_CDFILELIST (list), FALSE);
	
	retval = FALSE;
	disc_path = cdfilelist_path_get_string (list);
	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (list));
	selected = gtk_tree_selection_get_selected_rows (selection, &model);
	
	for (current = selected, i = 0; current; current = current->next)
	{
		path = current->data;
		gtk_tree_model_get_iter (model, &iter, path);
		gtk_tree_model_get (model, &iter, 
				NAME_COLUMN, &fd, 
				POSITION_COLUMN, &pos,
				-1);
		
		uri = g_strdup_printf ("%s%c%s", disc_path, G_DIR_SEPARATOR, fd->name);
		if (remove_file (list, uri, pos - i))
			retval = TRUE;
		
		g_free (uri);
		file_display_free (fd);
		
		i++;
	}
	
	if (retval)
	{
		g_signal_emit_by_name (G_OBJECT (list), "refresh");
		cdfilelist_set_modified (list, TRUE);
	}

	g_list_foreach (selected, (GFunc) gtk_tree_path_free, NULL);
	g_list_free (selected);
	g_free (disc_path);
	
	return (retval);
}

/* Return the index of the GtkTreeViewColumn displaying the file names */

static gint
get_column_index (CDFileListType type)
{
	gint index;
	
	switch (type)
	{
		case CDFILELIST_AUDIO:
		{
			index = 1;
			break;
		}
		case CDFILELIST_DATA:
		{
			index = 0;
			break;
		}
		default:
		{
			index = 0;
			break;
		}
	}
	
	return index;
}

/* Set the selected row to "editable" so the user can rename the file */

gboolean
cdfilelist_rename_selected (CDFileList *list, GError **error)
{
	gboolean retval;
	GtkTreePath *path;
	GtkTreeIter iter;
	GtkTreeModel *model;
	GtkTreeSelection *selection;
	GtkTreeViewColumn *col;
	GList *selected;
	gint col_index;
	
	retval = FALSE;
	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (list));
	selected = gtk_tree_selection_get_selected_rows (selection, &model);
	if (selected && (g_list_length (selected) == 1))
	{
		path = selected->data;
		
		gtk_tree_model_get_iter (GTK_TREE_MODEL (list->store), &iter, path);
		gtk_list_store_set (list->store, &iter, EDITABLE_COLUMN, TRUE, -1);
		col_index = get_column_index (list->type);
		col = gtk_tree_view_get_column (GTK_TREE_VIEW (list), col_index);
		gtk_tree_view_set_cursor (GTK_TREE_VIEW (list), path, col, TRUE);
		retval = TRUE;
	}
	
	g_list_foreach (selected, (GFunc) gtk_tree_path_free, NULL);
	g_list_free (selected);
	
	return retval;
}

/* Move the files with positions in 'to_move' up one position */

static gboolean
move_files_up (CDFileList *list, GList *to_move)
{
	GList *current, *current_out;
	gint *pos;
	FileEntry *entry;
	gboolean retval;
	
	retval = FALSE;
	to_move = g_list_sort (to_move, (GCompareFunc) int_cmp_ascending);
	
	for (current_out = to_move; current_out; current_out = current_out->next)
	{
		pos = current_out->data;
		
		/* we can't move the 1st element any higher */
		if (*pos <= 1)
			continue;
		
		for (current = list->files; current; current = current->next)
		{
			entry = current->data;
			if (entry->position == *pos)
			{
				entry->position--;
				retval = TRUE;
			}
			else if (entry->position == (*pos - 1))
				entry->position++;
		}
		
		*pos = (*pos - 1);
	}
	
	return retval;
}

/* Move the files with positions in 'to_move' down one position */

static gboolean
move_files_down (CDFileList *list, GList *to_move)
{
	GList *current, *current_out;
	gint *pos;
	FileEntry *entry;
	gboolean retval;
	
	retval = FALSE;
	to_move = g_list_sort (to_move, (GCompareFunc) int_cmp_descending);
	
	for (current_out = to_move; current_out; current_out = current_out->next)
	{
		pos = current_out->data;
		
		/* we can't move the last element any lower */
		if (*pos >= g_list_length (list->files))
			continue;
		
		for (current = list->files; current; current = current->next)
		{
			entry = current->data;
			if (entry->position == *pos)
			{
				entry->position++;
				retval = TRUE;
			}
			else if (entry->position == (*pos + 1))
				entry->position--;
		}
		
		*pos = (*pos + 1);
	}
	
	return retval;
}

/* Move the selected files one position in 'direction' */

gboolean
cdfilelist_move_selected (CDFileList *list, CDFileListDirection direction, GError **error)
{
	gboolean retval;
	GtkTreeModel *model;
	GtkTreePath *path;
	GtkTreeIter iter;
	GtkTreeSelection *selection;
	GList *selected, *to_move, *current;
	gint *pos;
	
	retval = FALSE;
	to_move = NULL;
	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (list));
	selected = gtk_tree_selection_get_selected_rows (selection, &model);
	
	for (current = selected; current; current = current->next)
	{
		path = current->data;
		pos = g_new0 (gint, 1);
		
		gtk_tree_model_get_iter (GTK_TREE_MODEL (list->store), &iter, path);
		gtk_tree_model_get (GTK_TREE_MODEL (list->store), &iter, 
				POSITION_COLUMN, pos, -1);
		to_move = g_list_prepend (to_move, pos);
	}
	
	if (direction == CDFILELIST_UP)
		retval = move_files_up (list, to_move);
	else if (direction == CDFILELIST_DOWN)
		retval = move_files_down (list, to_move);
	
	if (retval)
	{
		g_signal_emit_by_name (G_OBJECT (list), "refresh");
		reselect_rows_by_position (list, to_move);
		cdfilelist_set_modified (list, TRUE);
	}
	
	g_list_foreach (to_move, (GFunc) g_free, NULL);
	g_list_free (to_move);
	g_list_foreach (selected, (GFunc) gtk_tree_path_free, NULL);
	g_list_free (selected);
	
	return retval;
}

/* Return a string containing "New Folder" or "New Folder N" where N is the 
 * next available integer not already used by a file on the disc.
 */

static gchar *
next_available_new_folder (CDFileList *list)
{
	gchar *name, *uri;
	gint i;
	
	i = 2;
	name = g_strdup (_("New Folder"));
	uri = g_strdup_printf ("%s%c%s", list->path_string, G_DIR_SEPARATOR, name);
	
	while (g_list_find_custom (list->files, uri, (GCompareFunc) file_entry_cd_vs_uri_cmp))
	{
		g_free (name);
		g_free (uri);
		
		name = g_strdup_printf (_("New Folder %d"), i);
		uri = g_strdup_printf ("%s%c%s", list->path_string, G_DIR_SEPARATOR, name);
		
		i++;
	}
	
	return name;
}
	
/* Creates a new folder in the CDFileList and sets its name to be editable */

gboolean
cdfilelist_create_folder (CDFileList *list, GError **error)
{
	GtkTreeViewColumn *col;
	GtkTreePath *path;
	GtkTreeIter iter;
	gint col_index;
	gboolean retval;
	FileEntry *fe;
	FileDisplay *fd;
	GdkPixbuf *icon;
	GtkIconInfo *icon_info;
	const gchar *icon_path;
	
	retval = FALSE;
	
	/* Create FileDisplay with name and icon */
	fd = file_display_new ();
	fd->name = next_available_new_folder (list);
	icon_info = gtk_icon_theme_lookup_icon (list->icon_theme, 
			"gnome-fs-directory", ICON_SIZE, 0);
	icon_path = gtk_icon_info_get_filename (icon_info);
	icon = gdk_pixbuf_new_from_file (icon_path, NULL);
	fd->icon = gdk_pixbuf_scale_simple (icon, ICON_SIZE, ICON_SIZE, GDK_INTERP_BILINEAR);
	g_object_unref (G_OBJECT (icon));
	
	/* Add the folder to the list of FileEntries */
	fe = g_new0 (FileEntry, 1);
	fe->hd_uri = g_strdup (FOLDER_KEYWORD);
	fe->cd_uri = g_strdup_printf ("%s%c%s", list->path_string, G_DIR_SEPARATOR, fd->name);
	fe->position = 0;
	list->files = g_list_prepend (list->files, fe);
	
	/* Add the FileDisplay to the CDFileList and set it editable */
	gtk_list_store_append (list->store, &iter);
	gtk_list_store_set (list->store, &iter,
					NAME_COLUMN, fd,
					DIR_COLUMN, TRUE,
					EDITABLE_COLUMN, TRUE,
					-1);
	path = gtk_tree_model_get_path (GTK_TREE_MODEL (list->store), &iter);
	col_index = get_column_index (list->type);
	col = gtk_tree_view_get_column (GTK_TREE_VIEW (list), col_index);
	gtk_tree_view_set_cursor (GTK_TREE_VIEW (list), path, col, TRUE);
	retval = TRUE;
	
	if (retval)
		cdfilelist_set_modified (list, TRUE);
	
	gtk_tree_path_free (path);

	return retval;
}

/* Turn on drag-and-drop support for the CDFileList.  'callback' will be called
 * with the data from drag events. */

void
cdfilelist_set_dnd_add_func (CDFileList *list, DragRecvCallback callback, gpointer data)
{
	g_return_if_fail (IS_CDFILELIST (list));
	
	g_signal_connect (G_OBJECT (list), "drag_data_received", 
			G_CALLBACK ((GFunc) callback), data);
	
	gtk_drag_dest_set (GTK_WIDGET (list), GTK_DEST_DEFAULT_ALL, target_table, 
			G_N_ELEMENTS (target_table), GDK_ACTION_COPY);
	
	return;
}

static void
set_filename (CDFileList *list, const gchar *filename)
{
	gint equal;
	
	g_return_if_fail (IS_CDFILELIST (list));
	
	if (list->filename)
		equal = !strcmp (list->filename, filename);
	else
		equal = FALSE;
	
	if (!equal)
		cdfilelist_set_property (list, CDFILELIST_FILE, filename);
	
	return;
}

/* Save the contents of list->files to 'file' in XML */

gboolean
cdfilelist_save_to_file (CDFileList *list, const gchar *file, GError **error)
{
	GList *current;
	FileEntry *entry;
	gboolean retval;
	gchar *string, *err_str;
	const gchar *prop;
	xmlDocPtr doc;
	xmlNodePtr node, child;
	xmlAttrPtr attribute;
	
	g_return_val_if_fail (IS_CDFILELIST (list), FALSE);
	
	retval = FALSE;
	
	set_filename (list, file);
	
	doc = xmlNewDoc ("1.0");
	doc->children = xmlNewDocNode (doc, NULL, "disc", NULL);
	
	node = xmlDocGetRootElement (doc);
	
	/* Save the disc type */
	string = g_strdup_printf ("%d", list->type);
	attribute = xmlNewProp (node, "type", string);
	g_free (string);
	
	/* Save the disc properties */
	prop = cdfilelist_get_property (list, CDFILELIST_TITLE);
	if (prop)
		xmlNewChild (node, NULL, "title", prop);
	prop = cdfilelist_get_property (list, CDFILELIST_PUB);
	if (prop)
		xmlNewChild (node, NULL, "publisher", prop);
	prop = cdfilelist_get_property (list, CDFILELIST_CR);
	if (prop)
		xmlNewChild (node, NULL, "copyright", prop);
	
	/* Save the file list */
	for (current = list->files; current; current = current->next)
	{
		entry = current->data;
		
		child = xmlNewChild (node, NULL, "file_entry", NULL);
		attribute = xmlNewProp (child, "hd_uri", entry->hd_uri);
		attribute = xmlNewProp (child, "cd_uri", entry->cd_uri);
		string = g_strdup_printf ("%d", entry->position);
		attribute = xmlNewProp (child, "position", string);
		g_free (string);
	}
	
	if (xmlSaveFormatFile (file, doc, 1) < 0)
	{
		err_str = g_strdup_printf (_("Could not save file '%s'."), file);
		build_error (error, err_str);
		g_free (err_str);
	}
	
	xmlFreeDoc (doc);
	
	cdfilelist_set_modified (list, FALSE);
	
	return retval;
}

/* Load the contents of list->files from 'file' in XML */

gboolean
cdfilelist_load_from_file (CDFileList *list, const gchar *file, GError **error)
{
	gboolean retval;
	xmlDocPtr doc;
	xmlNodePtr node, child;
	gchar *string, *err_str;
	FileEntry *fe;
	
	g_return_val_if_fail (IS_CDFILELIST (list), FALSE);
	
	retval = FALSE;
	err_str = NULL;
	
	if (!(doc = xmlParseFile (file)))
	{
		err_str = g_strdup_printf (_("Could not open '%s'.  The file does not "
				"appear to be a saved disc."), file);
	}
	else if (!(node = xmlDocGetRootElement (doc)))
	{
		err_str = g_strdup_printf (_("Could not open '%s'.  The file does not "
				"appear to be a saved disc."), file);
	}
	else
	{
		child = node->xmlChildrenNode;
		while (child)
		{
			if (!strcmp (child->name, "file_entry"))
			{
				fe = g_new0 (FileEntry, 1);
				fe->hd_uri = xmlGetProp (child, "hd_uri");
				fe->cd_uri = xmlGetProp (child, "cd_uri");
				string = xmlGetProp (child, "position");
				fe->position = g_strtod (string, NULL);
				g_free (string);
				
				/* FIXME: Implement some strict error checking before adding
				 * FileEntries to list->files.
				 */
				if (fe->hd_uri && fe->cd_uri)
					list->files = g_list_prepend (list->files, fe);
				else
					file_entry_free (fe);
			}
			else if (!strcmp (child->name, "title"))
			{
				string = xmlNodeListGetString (doc, child->xmlChildrenNode, 1);
				cdfilelist_set_property (list, CDFILELIST_TITLE, string);
				g_free (string);
			}
			else if (!strcmp (child->name, "publisher"))
			{
				string = xmlNodeListGetString (doc, child->xmlChildrenNode, 1);
				cdfilelist_set_property (list, CDFILELIST_PUB, string);
				g_free (string);
			}
			else if (!strcmp (child->name, "copyright"))
			{
				string = xmlNodeListGetString (doc, child->xmlChildrenNode, 1);
				cdfilelist_set_property (list, CDFILELIST_CR, string);
				g_free (string);
			}
			
			child = child->next;
		}
	}
	
	if (err_str)
	{
		build_error (error, err_str);
		g_free (err_str);
		
		return FALSE;
	}
	
	xmlFreeDoc (doc);
	
	set_filename (list, file);
	
	cdfilelist_set_modified (list, FALSE);
	
	refresh_view (list);
	
	return retval;
}

/* Set list of supported audio mimetypes for the CDFileList.  The memory for
 * 'types' is not copied, so do not free it.
 */

void
cdfilelist_set_audio_types (CDFileList *list, GList *types)
{
	if (list->audio_types)
	{
		g_list_foreach (list->audio_types, (GFunc) g_free, NULL);
		g_list_free (list->audio_types);
	}
	
	list->audio_types = types;
	
	return;
}

/* Comparision function used to alphabetically sort a list of CDFileListFile 
 * structures.
 */

static gint
cdfilelist_file_cmp (gconstpointer a, gconstpointer b)
{
	const CDFileListFile *fa, *fb;
	
	fa = a;
	fb = b;
	
	return (strcmp (fa->cd_uri, fb->cd_uri));
}

/* Return a list of CDFileListFile structs containing the files to be burned */

GList*
cdfilelist_get_files_to_burn (CDFileList *list)
{
	GList *files, *current;
	CDFileListFile *f;
	FileEntry *e;
	
	files = NULL;
	
	for (current = list->files; current; current = current->next)
	{
		e = current->data;
		f = g_new0 (CDFileListFile, 1);
		f->hd_uri = g_strdup (e->hd_uri);
		f->cd_uri = g_strdup (e->cd_uri);
		files = g_list_prepend (files, f);
	}
	
	files = g_list_sort (files, cdfilelist_file_cmp);
	
	return files;
}
