/*
 *  Copyright 2003-2004 Todd Kulesza <todd@dropline.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef _OPTIMYSTIC_H_
#define _OPTIMYSTIC_H_

#include "config.h"

#include "cd-filelist.h"

#include <libburn/libburn.h>
#include <gconf/gconf-client.h>
#include <gtk/gtkwidget.h>
#include <gtk/gtkactiongroup.h>

#define DISPLAY_NAME            _("CD Writer")

typedef struct _OptiData			OptiData;
typedef struct _OptiMenus			OptiMenus;
typedef struct _CDFileListControls	CDFileListControls;

enum
{
	CD_TYPE_UNKNOWN,
	CD_TYPE_AUDIO,
	CD_TYPE_DATA
};

enum BURN_TYPE 
{
	BURN_TYPE_IMAGE,
	BURN_TYPE_COPY,
	BURN_TYPE_CD,
	BURN_TYPE_SOURCE
};

struct _OptiData
{
	GtkWidget *window_main;
	GdkPixbuf *window_icon;
	GtkWidget *appbar;
	
	OptiMenus *menus;
	
	GConfClient *gconf;
	
	gint n_drives;
	gint n_cd_writable_drives;
	struct burn_drive_info *drives;
	struct burn_drive_info *cd_writable_drives;
		
	GList *supported_audio_types;
	
	/* The CDFileList and associated controls*/
	GtkWidget *fl;
	CDFileListControls *flc;
	GtkWidget *scrolled;
	GtkWidget *sidebar;
	
	/* A GAsyncQueue is used to pass messages between threads */
	GAsyncQueue *msg_queue;
};

struct _CDFileListControls
{
	GtkWidget *vbox;
	
	/* Generic */
	GtkWidget *add;
	GtkWidget *remove;
	
	/* Data */
	GtkWidget *go_up;
	
	/* Audio */
	GtkWidget *play;
	GtkWidget *move_up;
	GtkWidget *move_down;
};

struct _OptiMenus
{
	GtkActionGroup *disc_group;
	GtkActionGroup *modified_group;
	GtkActionGroup *selected_group;
	GtkActionGroup *data_group;
	GtkActionGroup *audio_selected_group;
};

/* GLOBAL VARIABLES */

extern OptiData *od;

void
opti_quit (void);

void
opti_string_error (GtkWidget *parent, const gchar *error);

void
opti_standard_error (GtkWidget *parent, GError *error);

void
opti_set_menu_sensitivity (const gint n);

#endif /* _OPTIMYSTIC_H_ */
