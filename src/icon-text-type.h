/*
 *  Copyright 2004 Todd Kulesza <todd@dropline.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef _ICON_TEXT_TYPE_H_
#define _ICON_TEXT_TYPE_H_

#include <gdk-pixbuf/gdk-pixbuf.h>

#define FILE_DISPLAY_TYPE  file_display_get_type ()

typedef struct _FileDisplay FileDisplay;

struct _FileDisplay
{
	gchar *name;
	GdkPixbuf *icon;
};

FileDisplay*
file_display_new (void);

void
file_display_free (FileDisplay *fd);

FileDisplay*
file_display_copy (FileDisplay *src);

GType
file_display_get_type (void);

#endif /* _ICON_TEXT_TYPE_H_ */
