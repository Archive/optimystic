/*
 *  Copyright 2003 Todd Kulesza <todd@dropline.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef _CD_AUDIO_H_
#define _CD_AUDIO_H_

#include "config.h"

#include <glib.h>
#include <libxml/tree.h>
#include <libxml/parser.h>

typedef struct _AudioDragData		AudioDragData;
typedef struct _AudioAddData		AudioAddData;
typedef struct _AudioFileEntry		AudioFileEntry;

struct _AudioDragData
{
	gboolean drag_in_progress;
	gboolean is_dest_set;
	gulong handler_id;
};

struct _AudioAddData
{
	gchar *title;
	gchar *uri;
	gchar *mimetype;
	
	GtkListStore *store;
};

struct _AudioFileEntry
{
	gchar *hd_path;
	gint order;
};

GtkWidget *
opti_build_audio_view (void);

void
opti_open_audio_cd (GList *track_list);

void
opti_audio_file_entry_free (gpointer data, gpointer user_data);

#endif /* _CD_AUDIO_H_ */
