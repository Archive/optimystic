/*
 *  Copyright 2003-2004 Todd Kulesza <todd@dropline.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef _CD_LIB_H_
#define _CD_LIB_H_

#include "config.h"

#include "optimystic.h"

typedef struct _AddData AddData;
	
struct _AddData
{
	GList *file_list;
	gchar *base_uri;
};

void
cdfilelist_controls_destroy (CDFileListControls *flc);

gboolean
opti_check_drive_free_space (gulong size_needed, const gchar *drive);

gchar *
opti_cd_check_cache_dir (void);

void
opti_new_disc (CDFileListType type);

void
add_files_dialog (void);

void
add_folder_dialog (void);

void
remove_files (void);

void
rename_file (void);

void
move_files (CDFileListDirection direction);

#endif /* _CD_LIB_H_ */
