/*
 *  Copyright 2003-2004 Todd Kulesza <todd@dropline.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 
#ifndef _CD_FILELIST_H_
#define _CD_FILELIST_H_

#include <libintl.h>
#include <gtk/gtktreeview.h>
#include <gtk/gtkliststore.h>
#include <gtk/gtkicontheme.h>
#include <gst/play/play.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define _(String) gettext (String)
#define N_(String) (String)

#define TYPE_CDFILELIST            (cdfilelist_get_type ())
#define CDFILELIST(obj)            (G_TYPE_CHECK_INSTANCE_CAST (obj, TYPE_CDFILELIST, CDFileList))
#define CDFILELIST_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST (klass, TYPE_CDFILELIST, CDFileListClass))
#define IS_CDFILELIST(obj)         (G_TYPE_CHECK_INSTANCE_TYPE (obj, TYPE_CDFILELIST))
#define IS_CDFILELIST_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE (klass, TYPE_CDFILELIST))
#define CDFILELIST_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS (obj, TYPE_CDFILELIST, CDFileListClass))

typedef struct _CDFileListFile  CDFileListFile;
typedef struct _CDFileList		CDFileList;
typedef struct _CDFileListClass	CDFileListClass;

typedef enum
{
	CDFILELIST_UNKNOWN,
	CDFILELIST_DATA,
	CDFILELIST_AUDIO
} CDFileListType;

typedef enum
{
	CDFILELIST_UP,
	CDFILELIST_DOWN
} CDFileListDirection;

typedef enum
{
	CDFILELIST_FILE,		/* filename */
	CDFILELIST_TITLE,		/* title */
	CDFILELIST_PUB,			/* publisher */
	CDFILELIST_CR			/* copyright */
} CDFileListProp;

typedef void (* DragRecvCallback) (GtkWidget *widget, 
                                   GdkDragContext *context,
		                           gint x, 
                                   gint y, 
                                   GtkSelectionData *data, 
                                   guint info, guint time, 
                                   gpointer user_data);

struct _CDFileListFile
{
	gchar *hd_uri;
	gchar *cd_uri;
};

struct _CDFileList
{
	GtkTreeView parent;
	
	/* The GtkListStore and associated viewing widgets */
	GtkListStore *store;
	
	CDFileListType type;
	
	/* A linked-list of files destined for a CD */
	GList *files;
	
	/* A linked-list of folder names making up the current path on the CD */
	GList *path;
	/* A character array of the current path on the CD */
	gchar *path_string;
	
	gchar *filename;
	gchar *title;
	gchar *publisher;
	gchar *copyright;
	guint size_total;
	guint size_used;
	gboolean modified; /* true if the file has been modified since the last save */
	
	/* Pointer to the FreeDesktop.org Icon Theme */
	GtkIconTheme *icon_theme;
	
	/* GstPlayer used to determine a file's length */
	GstPlay *player;
	GList *length_files;
	gchar *length_uri;
	GMutex *length_mutex;
	gulong length_id_timeout;
	gulong length_id_cb;
	
	/* List of supported audio types */
	GList *audio_types;
};

struct _CDFileListClass
{
	GtkTreeViewClass parent_class;
	
	void (* refresh)         (CDFileList *list);
	void (* path_changed)    (CDFileList *list);
	void (* files_changed)   (CDFileList *list,
	                          gboolean modified);
	void (* name_changed)    (CDFileList *list,
	                          const gchar *name);
};

GType          cdfilelist_get_type             (void) G_GNUC_CONST;
GtkWidget*     cdfilelist_new                  (CDFileListType type);
void           cdfilelist_set_modified         (CDFileList *list, 
                                                gboolean modified);
gboolean       cdfilelist_get_modified         (CDFileList *list);
CDFileListType cdfilelist_get_cd_type          (CDFileList *list);
void           cdfilelist_set_property         (CDFileList *list,
                                                CDFileListProp property,
                                                const gchar *value);
const gchar*   cdfilelist_get_property         (CDFileList *list,
                                                CDFileListProp property);
gboolean       cdfilelist_get_is_empty         (CDFileList *list);
void           cdfilelist_path_add             (CDFileList *list, 
                                                const gchar *folder);
void           cdfilelist_path_up              (CDFileList *list);
gchar**        cdfilelist_path_get_array       (CDFileList *list);
gchar*         cdfilelist_path_get_string      (CDFileList *list);
gboolean       cdfilelist_file_add             (CDFileList *list, 
                                                const gchar *uri,
                                                const gchar *uri_base,
                                                GError **error);
gboolean       cdfilelist_remove_selected      (CDFileList *list,
                                                GError **error);
gboolean       cdfilelist_rename_selected      (CDFileList *list,
                                                GError **error);
gboolean       cdfilelist_move_selected        (CDFileList *list,
                                                CDFileListDirection direction,
                                                GError **error);
gboolean       cdfilelist_create_folder        (CDFileList *list,
                                                GError **error);
void           cdfilelist_set_dnd_add_func     (CDFileList *list,
                                                DragRecvCallback callback,
                                                gpointer data);
gboolean       cdfilelist_save_to_file         (CDFileList *list,
                                                const gchar *file,
                                                GError **error);
gboolean       cdfilelist_load_from_file       (CDFileList *list,
                                                const gchar *file,
                                                GError **error);
void           cdfilelist_set_audio_types      (CDFileList *list,
                                                GList *types);
GList*         cdfilelist_get_files_to_burn    (CDFileList *list);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _CD_FILELIST_H_ */
