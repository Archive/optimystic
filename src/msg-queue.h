/*
 *  Copyright 2004 Todd Kulesza <todd@dropline.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef _MSG_QUEUE_H_
#define _MSG_QUEUE_H_

#include "config.h"

#include <glib.h>

typedef struct _MsgInfo MsgInfo;

typedef enum
{
	MSG_TYPE_SET_WIDGET,
	MSG_TYPE_ERROR,
	MSG_TYPE_PROGRESS,
	MSG_TYPE_INSERT_CD,
	MSG_TYPE_DONE
} MsgType;

struct _MsgInfo
{
	MsgType type;
	/* Pointer to the active widget.  Exactly what this is depends on the 
	 * MsgType as follows:
	 * MSG_TYPE_ERROR: The parent window for the error dialog.
	 * MSG_TYPE_PROGRESS: The window containing the progress bar.
	 * MSG_TYPE_INSERT_CD: The parent window for the Insert CD dialog.
	 * MSG_TYPE_DONE: The window to be destroyed.
	 */
	gpointer widget;
	/* Message to display to the user. */
	gchar *msg;
	/* Progress percentage completed. */
	gdouble progress;
};

GAsyncQueue*
msg_queue_setup (void);

MsgInfo*
msg_info_new (void);

void
msg_info_free (MsgInfo *info);

#endif /* _MSG_QUEUE_H_ */
