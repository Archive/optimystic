/*
 *  Copyright 2003-2004 Todd Kulesza <todd@dropline.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h"

#include "optimystic.h"
#include "dialogs.h"
#include "cd-lib.h"
#include "win-main.h"

#include <string.h>
#include <gnome.h>
#include <libgnomevfs/gnome-vfs-ops.h>

OptiData *od;

static void menu_new_cb (void);
static void menu_open_cb (void);
static void menu_save_cb (void);
static void menu_save_as_cb (void);
static void menu_prop_cb (void);
static void menu_burn_cb (void);
static void menu_close_cb (void);
static void menu_exit_cb (void);
static void menu_add_cb (void);
static void menu_add_folder_cb (void);
static void menu_create_folder_cb (void);
static void menu_remove_cb (void);
static void menu_rename_cb (void);
static void menu_up_cb (void);
static void menu_down_cb (void);
static void menu_prefs_cb (void);
static void menu_erase_cd_cb (void);
static void menu_copy_cd_cb (void);
static void menu_write_image_cb (void);
static void menu_about_cb (void);

static GtkActionEntry global_entries [] =
{
	{ "DiscMenu", NULL, N_("_Disc") },
	{ "EditMenu", NULL, N_("_Edit") },
	{ "ToolsMenu", NULL, N_("_Tools"), },
	{ "HelpMenu", NULL, N_("_Help") },
	{ "New", GTK_STOCK_NEW, NULL, "<ctl>N", N_("Create a new disc"), menu_new_cb },
	{ "Open", GTK_STOCK_OPEN, NULL, "<ctl>O", N_("Open an existing disc"), menu_open_cb },
	{ "Quit", GTK_STOCK_QUIT, NULL, "<ctl>Q", NULL, menu_exit_cb },
	{ "Preferences", GTK_STOCK_PREFERENCES, NULL, NULL, NULL, menu_prefs_cb },
	{ "EraseCD", NULL, N_("_Erase CD..."), NULL, N_("Erase all files from the rewritable CD in your optical drive"), menu_erase_cd_cb },
	{ "CopyCD", NULL, N_("_Copy CD..."), NULL, N_("Create a copy of the CD in your optical drive"), menu_copy_cd_cb },
	{ "WriteImage", NULL, N_("_Write Image to CD..."), NULL, N_("Write an ISO image file to a CD"), menu_write_image_cb },
	{ "About", NULL, N_("_About"), NULL, N_("About Optimystic") , menu_about_cb }
};

static GtkActionEntry disc_entries [] =
{
	{ "SaveAs", GTK_STOCK_SAVE_AS, NULL, "<shft><ctl>S", NULL, menu_save_as_cb },
	{ "Properties", GTK_STOCK_PROPERTIES, NULL, NULL, NULL, menu_prop_cb },
	{ "Write", "optimystic-write-cd", NULL, "<ctl>B", N_("Write contents to CD"), menu_burn_cb },
	{ "Close", GTK_STOCK_CLOSE, NULL, "<ctl>W", NULL, menu_close_cb },
	{ "AddFiles", GTK_STOCK_ADD, N_("_Add..."), NULL, NULL, menu_add_cb },
};

static GtkActionEntry modified_entries [] =
{
	{ "Save", GTK_STOCK_SAVE, NULL, "<ctl>S", N_("Save this disc"), menu_save_cb }
};

static GtkActionEntry selected_entries [] =
{
	{ "Remove", GTK_STOCK_REMOVE, NULL, "Delete", NULL, menu_remove_cb },
	{ "Rename", NULL, N_("Re_name"), "F2", NULL, menu_rename_cb }
};

static GtkActionEntry data_entries [] =
{
	{ "AddFolder", NULL, N_("Add _Folder..."), NULL, NULL, menu_add_folder_cb },
	{ "CreateFolder", NULL, N_("_Create Folder..."), NULL, NULL, menu_create_folder_cb }
};

static GtkActionEntry audio_selected_entries [] =
{
	{ "MoveUp", GTK_STOCK_GO_UP, N_("Move _up"), NULL, NULL, menu_up_cb },
	{ "MoveDown", GTK_STOCK_GO_DOWN, N_("Move _down"), NULL, NULL, menu_down_cb }
};

static const gchar *ui_description =
"<ui>"
"  <menubar name='MainMenu'>"
"    <menu action='DiscMenu'>"
"      <menuitem action='New'/>"
"      <menuitem action='Open'/>"
"      <separator name='sep1'/>"
"      <menuitem action='Save'/>"
"      <menuitem action='SaveAs'/>"
"      <separator name='sep2'/>"
"      <menuitem action='Properties'/>"
"      <separator name='sep3'/>"
"      <menuitem action='Write'/>"
"      <separator name='sep4'/>"
"      <menuitem action='Close'/>"
"      <menuitem action='Quit'/>"
"    </menu>"
"    <menu action='EditMenu'>"
"      <menuitem action='AddFiles'/>"
"      <menuitem action='AddFolder'/>"
"      <menuitem action='CreateFolder'/>"
"      <menuitem action='Remove'/>"
"      <separator name='sep1'/>"
"      <menuitem action='Rename'/>"
"      <menuitem action='MoveUp'/>"
"      <menuitem action='MoveDown'/>"
"      <separator name='sep2'/>"
"      <menuitem action='Preferences'/>"
"    </menu>"
"    <menu action='ToolsMenu'>"
"      <menuitem action='EraseCD'/>"
"      <menuitem action='CopyCD'/>"
"      <menuitem action='WriteImage'/>"
"    </menu>"
"    <menu action='HelpMenu'>"
"      <menuitem action='About'/>"
"    </menu>"
"  </menubar>"
"  <toolbar name='Toolbar'>"
"    <toolitem action='New'/>"
"    <toolitem action='Open'/>"
"    <toolitem action='Save'/>"
"    <separator name='sep1'/>"
"    <toolitem action='Write'/>"
"    <separator name='sep2'/>"
"  </toolbar>"
"</ui>";

static void
not_ready (void)
{
	GtkWidget *dialog;
	
	dialog = gtk_message_dialog_new (GTK_WINDOW (od->window_main),
			GTK_DIALOG_MODAL,
			GTK_MESSAGE_ERROR,
			GTK_BUTTONS_CLOSE,
			"Sorry, this feature is not working (yet).");
	
	gtk_dialog_run (GTK_DIALOG (dialog));
	
	gtk_widget_destroy (dialog);
	
	return;
}

/* Close the current disc, prompt to save if it's been modified */

static void
close_disc (void)
{
	if (dlg_confirm_close ())
		return;

	if (od->fl)
		gtk_widget_destroy (od->fl);
	if (od->flc)
		cdfilelist_controls_destroy (od->flc);
	if (od->sidebar)
		gtk_widget_destroy (od->sidebar);
	if (od->scrolled)
		gtk_widget_destroy (od->scrolled);
	od->fl = od->sidebar = od->scrolled = NULL;
	od->flc = NULL;
	
	opti_set_menu_sensitivity (0);
	
	return;
}

static gboolean
delete_event_cb (GtkWidget *w, GdkEvent *event, gpointer data)
{
	gboolean retval;
	
	retval = FALSE;
	
	if (dlg_confirm_close ())
		retval = TRUE;
	
	return retval;
}

static void
destroy_event_cb (GtkObject *w, gpointer data)
{
	opti_quit ();
	
	return;
}

static void
menu_new_cb (void)
{
	gchar *last_cd_type;
	CDFileListType type;
	
	last_cd_type = gconf_client_get_string (od->gconf, 
			"/apps/optimystic/last_cd_type", NULL);
	/* if last_cd_type is unknown, just make an audio cd. */
	if (!last_cd_type || !strcmp (last_cd_type, "data"))
		type = CDFILELIST_DATA;
	else
		type = CDFILELIST_AUDIO;
	
	opti_new_disc (type);
	
	return;
}

static void
menu_open_cb (void)
{
	dlg_open_disc ();
	
	return;
}

static void
menu_save_cb (void)
{
	dlg_save_disc (FALSE);
	
	return;
}

static void
menu_save_as_cb (void)
{
	dlg_save_disc (TRUE);
	
	return;
}

static void
menu_prop_cb (void)
{
	dlg_properties ();
	
	return;
}

static void
menu_burn_cb (void)
{
	dlg_burn_disc ();
	
	return;
}

static void
menu_close_cb (void)
{
	close_disc ();
	
	return;
}

static void
menu_exit_cb (void)
{
	if (dlg_confirm_close ())
		return;
	
	opti_quit ();
	
	return;
}

static void menu_add_cb (void)
{
	add_files_dialog ();
	
	return;
}

static void menu_add_folder_cb (void)
{
	add_folder_dialog ();
	
	return;
}

static void menu_create_folder_cb (void)
{
	cdfilelist_create_folder (CDFILELIST (od->fl), NULL);
	
	return;
}

static void menu_remove_cb (void)
{
	remove_files ();
	
	return;
}

static void menu_rename_cb (void)
{
	rename_file ();
	
	return;
}

static void menu_up_cb (void)
{
	move_files (CDFILELIST_UP);
	
	return;
}

static void menu_down_cb (void)
{
	move_files (CDFILELIST_DOWN);
	
	return;
}

static void
menu_prefs_cb (void)
{
	dlg_preferences ();
	
	return;
}

static void
menu_erase_cd_cb (void)
{
	dlg_blank_disc ();
	
	return;
}

static void
menu_copy_cd_cb (void)
{
	not_ready ();
	
	return;
}

static void
menu_write_image_cb (void)
{
	dlg_burn_image ();
	
	return;
}

static void
menu_about_cb (void)
{
	dlg_about ();
	
	return;
}

/* Helper function to mark the 'name' action as "is-important", and thus worthy
 * of text beside the toolbar item.
 */

static void
mark_action_important (const gchar *name, GtkActionGroup *group)
{
	GtkAction *action;
	GValue value = {0};
	
	action = gtk_action_group_get_action (group, name);
	if (action)
	{
		g_value_init (&value, G_TYPE_BOOLEAN);
		g_value_set_boolean (&value, TRUE);
		g_object_set_property (G_OBJECT (action), "is-important", &value);
	}
	
	return;
}

static void
setup_menus_and_toolbars (GtkWidget *window)
{
	GtkActionGroup *action_group;
	GtkUIManager *ui_manager;
	GtkAccelGroup *accel_group;
	GtkWidget *menubar, *toolbar;
	GError *error;
	
	ui_manager = gtk_ui_manager_new ();
	
	action_group = gtk_action_group_new ("GlobalActions");
	gtk_action_group_set_translation_domain (action_group, GETTEXT_PACKAGE);
	gtk_action_group_add_actions (action_group, global_entries, 
			G_N_ELEMENTS (global_entries), window);
	mark_action_important ("Open", action_group);
	gtk_ui_manager_insert_action_group (ui_manager, action_group, 0);
	
	action_group = gtk_action_group_new ("DiscActions");
	gtk_action_group_set_translation_domain (action_group, GETTEXT_PACKAGE);
	gtk_action_group_add_actions (action_group, disc_entries, 
			G_N_ELEMENTS (disc_entries), window);
	gtk_ui_manager_insert_action_group (ui_manager, action_group, 0);
	mark_action_important ("Write", action_group);
	od->menus->disc_group = action_group;
	
	action_group = gtk_action_group_new ("ModifiedActions");
	gtk_action_group_set_translation_domain (action_group, GETTEXT_PACKAGE);
	gtk_action_group_add_actions (action_group, modified_entries, 
			G_N_ELEMENTS (modified_entries), window);
	gtk_ui_manager_insert_action_group (ui_manager, action_group, 0);
	mark_action_important ("Save", action_group);
	od->menus->modified_group = action_group;
	
	action_group = gtk_action_group_new ("SelectedActions");
	gtk_action_group_set_translation_domain (action_group, GETTEXT_PACKAGE);
	gtk_action_group_add_actions (action_group, selected_entries, 
			G_N_ELEMENTS (selected_entries), window);
	gtk_ui_manager_insert_action_group (ui_manager, action_group, 0);
	od->menus->selected_group = action_group;
	
	action_group = gtk_action_group_new ("DataActions");
	gtk_action_group_set_translation_domain (action_group, GETTEXT_PACKAGE);
	gtk_action_group_add_actions (action_group, data_entries, 
			G_N_ELEMENTS (data_entries), window);
	gtk_ui_manager_insert_action_group (ui_manager, action_group, 0);
	od->menus->data_group = action_group;
	
	action_group = gtk_action_group_new ("AudioSelectedActions");
	gtk_action_group_set_translation_domain (action_group, GETTEXT_PACKAGE);
	gtk_action_group_add_actions (action_group, audio_selected_entries, 
			G_N_ELEMENTS (audio_selected_entries), window);
	gtk_ui_manager_insert_action_group (ui_manager, action_group, 0);
	od->menus->audio_selected_group = action_group;
	
	accel_group = gtk_ui_manager_get_accel_group (ui_manager);
	gtk_window_add_accel_group (GTK_WINDOW (window), accel_group);
	
	error = NULL;
	if (!gtk_ui_manager_add_ui_from_string (ui_manager, ui_description, -1, &error))
	{
		g_message ("building menus failed: %s", error->message);
		g_error_free (error);
	}
	else
	{
		menubar = gtk_ui_manager_get_widget (ui_manager, "/MainMenu");
		gnome_app_set_menus (GNOME_APP (window), GTK_MENU_BAR (menubar));
		
		toolbar = gtk_ui_manager_get_widget (ui_manager, "/Toolbar");
		gnome_app_set_toolbar (GNOME_APP (window), GTK_TOOLBAR (toolbar));
		
		gtk_widget_show_all (menubar);
		gtk_widget_show_all (toolbar);
	}
	
	return;
}

void
opti_build_window_main (void)
{
	GList *icon_list;
	GtkWidget *app;
	GtkWidget *appbar;
	
	icon_list = NULL;
	icon_list = g_list_append (icon_list, od->window_icon);
	gtk_window_set_default_icon_list (icon_list);
	g_list_free (icon_list);
	
	app = gnome_app_new (PACKAGE, DISPLAY_NAME);
	od->window_main = app;
	gtk_widget_set_size_request (app, 600, 450);
	
	appbar = gtk_statusbar_new ();
	od->appbar = appbar;
	gnome_app_set_statusbar (GNOME_APP (app), appbar);

	setup_menus_and_toolbars (app);

	g_signal_connect (G_OBJECT (app), "delete_event",
			G_CALLBACK (delete_event_cb), NULL);
	g_signal_connect (G_OBJECT (app), "destroy",
			G_CALLBACK (destroy_event_cb), NULL);
	
	opti_set_menu_sensitivity (0);
	
	gtk_widget_show_all (appbar);
	gtk_widget_show (app);
	
	return;
}
