/*
 *  Copyright 2003-2004 Todd Kulesza <todd@dropline.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h"

#include "optimystic.h"
#include "msg-queue.h"
#include "subsystem.h"
#include "win-main.h"

#include <libburn/libburn.h>
#include <gconf/gconf-client.h>
#include <gtk/gtk.h>
#include <libgnomeui/gnome-client.h>
#include <libgnomeui/gnome-ui-init.h>
#include <gst/gst.h>
#include <string.h>

OptiData *od;

void
opti_quit (void)
{	
	drives_free (od->drives);
	
	g_object_unref (G_OBJECT (od->window_icon)); 
	g_object_unref (od->gconf);
	
	gtk_main_quit ();
	
	return;
}

void
opti_string_error (GtkWidget *parent, const gchar *error)
{
	GtkWidget *dialog;
	
	if (!error)
		return;
	
	dialog = gtk_message_dialog_new (GTK_WINDOW (parent), GTK_DIALOG_MODAL,
			GTK_MESSAGE_ERROR, GTK_BUTTONS_OK, error);
	
	gtk_dialog_run (GTK_DIALOG (dialog));
	
	gtk_widget_destroy (dialog);
	
	return;
}

void
opti_standard_error (GtkWidget *parent, GError *error)
{
	if (!error)
		return;
	
	opti_string_error (parent, error->message);
	
	g_clear_error (&error);
	
	return;
}

void
opti_set_menu_sensitivity (const gint selected)
{
	gboolean open, modified;
	CDFileListType type;
	
	if (od->fl)
	{
		open = TRUE;
		modified = cdfilelist_get_modified (CDFILELIST (od->fl));
		type = cdfilelist_get_cd_type(CDFILELIST (od->fl));
	}
	else
	{
		open = modified = FALSE;
		type = CDFILELIST_UNKNOWN;
	}
	
	/* FIXME: Check if the disc is empty; if it is, disable "save", "save as",
	 * and "burn"
	 */
	
	gtk_action_group_set_sensitive (od->menus->disc_group, open);
	gtk_action_group_set_sensitive (od->menus->modified_group, open && modified);
	gtk_action_group_set_sensitive (od->menus->selected_group, open && selected);
	gtk_action_group_set_sensitive (od->menus->data_group, open && (type == CDFILELIST_DATA));
	gtk_action_group_set_sensitive (od->menus->audio_selected_group, open && selected && (type == CDFILELIST_AUDIO));
	
	return;
}

static void
die_cb (GnomeClient *client, gpointer data)
{
	burn_finish ();
	
	g_object_unref (od->gconf);
	
	gtk_main_quit ();
	
	return;
}

/* Check for plugins to decode various audio formats */

GList *
detect_supported_audio_types (void)
{
	GstElement *plugin;
	gchar *mimetype;
	GList *types;
	
	types = NULL;
	
	/* FIXME: different versions of gnome-vfs seem to use different strings
	 * for the same mime type.  Try to support them all.
	 */
	
	/* mp3 */
	plugin = gst_element_factory_make ("mad", "plugin");
	if (GST_IS_ELEMENT (plugin))
	{
		mimetype = g_strdup ("audio/x-mp3");
		types = g_list_prepend (types, mimetype);
		mimetype = g_strdup ("audio/mpeg");
		types = g_list_prepend (types, mimetype);
		gst_object_unref (GST_OBJECT (plugin));
	}
	
	/* ogg vorbis */
	plugin = gst_element_factory_make ("vorbisfile", "plugin");
	if (GST_IS_ELEMENT (plugin))
	{
		mimetype = g_strdup ("application/x-ogg");
		types = g_list_prepend (types, mimetype);
		mimetype = g_strdup ("application/ogg");
		types = g_list_prepend (types, mimetype);
		gst_object_unref (GST_OBJECT (plugin));
	}
	
	/* wav */
	plugin = gst_element_factory_make ("wavparse", "plugin");
	if (GST_IS_ELEMENT (plugin))
	{
		mimetype = g_strdup ("audio/x-wav");
		types = g_list_prepend (types, mimetype);
		gst_object_unref (GST_OBJECT (plugin));
	}
	
	/* flac */
	plugin = gst_element_factory_make ("flacdec", "plugin");
	if (GST_IS_ELEMENT (plugin))
	{
		mimetype = g_strdup ("application/x-flac");
		types = g_list_prepend (types, mimetype);
		gst_object_unref (GST_OBJECT (plugin));
	}
	
	return types;
}

static void
register_stock_icons (void)
{
	GdkPixbuf *pixbuf;
	GtkIconFactory *factory;
	GtkIconSet *icon_set;
	
	static GtkStockItem items[] =
	{
		{ "optimystic-write-cd", N_("_Write to CD..."), 0, 0, NULL }
	};
	
	gtk_stock_add (items, G_N_ELEMENTS (items));
	
	/* Add our custom icon factory to the list of defaults */
	factory = gtk_icon_factory_new ();
	gtk_icon_factory_add_default (factory);
	
	/* Register icon to accompany stock item */
	pixbuf = gdk_pixbuf_new_from_file (
			PIXMAPDIR G_DIR_SEPARATOR_S "optimystic" G_DIR_SEPARATOR_S "write-cd.png", NULL);
	icon_set = gtk_icon_set_new_from_pixbuf (pixbuf);
	gtk_icon_factory_add (factory, "optimystic-write-cd", icon_set);
	gtk_icon_set_unref (icon_set);
	g_object_unref (pixbuf);
	
	/* Drop our reference to the factory, GTK will hold a reference. */
	g_object_unref (factory);
	
	return;
}

/* fill 'od->cd_writable_drives' with the recordable optical drives found on the
   system. */

static gint
cd_writable_drives_detect (void)
{
	gint count, i, j;
	
	count = 0;
	
	for (i = 0; i < od->n_drives; i++)
	{
		if (od->drives[i].write_cdr || od->drives[i].write_cdrw)
			count++;
	}
	
	if (count)
	{
		od->cd_writable_drives = g_new0 (struct burn_drive_info, count);
		for (i = j = 0; i < od->n_drives; i++)
		{
			if (od->drives[i].write_cdr || od->drives[i].write_cdrw)
			{
				memcpy (&od->cd_writable_drives[j], &od->drives[i], sizeof (struct burn_drive_info));
				j++;
			}
		}
	}
	
	return count;
}

gint
main (gint argc, gchar **argv)
{
	GnomeProgram *program;
	GnomeClient *client;
	gint result;
	
	bindtextdomain (GETTEXT_PACKAGE, GNOMELOCALEDIR);
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);
	
	program = gnome_program_init (PACKAGE, VERSION, LIBGNOMEUI_MODULE,
			argc, argv, NULL);
	
	gdk_threads_init();
	
	gst_init (&argc, &argv);
	
	client = gnome_master_client ();
	g_signal_connect (G_OBJECT (client), "die",
			G_CALLBACK (die_cb), NULL);
	
	od = g_new0 (OptiData, 1);
	od->menus = g_new0 (OptiMenus, 1);
	od->window_main = NULL;
	od->window_icon = gdk_pixbuf_new_from_file (
			PIXMAPDIR G_DIR_SEPARATOR_S "optimystic.png", NULL);
	od->drives = NULL;
	od->cd_writable_drives = NULL;
	od->n_drives = 0;
	od->n_cd_writable_drives = 0;
	od->supported_audio_types = detect_supported_audio_types ();
	od->fl = NULL;
	od->msg_queue = msg_queue_setup ();
	
	od->gconf = gconf_client_get_default ();
	
	burn_set_verbosity (12);
	
	register_stock_icons ();
	
	/* These are not final release strings, no need to translate yet */
	result = drives_detect (&od->drives, &od->n_drives);
	if (!result)
		g_error ("Failed to initialize libburn.");
	else if (result == -1)
		g_warning ("No optical drives were detected.");
	od->n_cd_writable_drives = cd_writable_drives_detect ();
	if (!od->n_cd_writable_drives)
		g_warning ("No recordable optical drives were detected.");
	
	opti_build_window_main ();
	
	gdk_threads_enter ();
	gtk_main ();
	gdk_threads_leave ();
	
	return 0;
}
