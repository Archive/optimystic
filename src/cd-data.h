/*
 *  Copyright 2003 Todd Kulesza <todd@dropline.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef _CD_DATA_H_
#define _CD_DATA_H_

#include "config.h"

#include <glib.h>
#include <libxml/tree.h>
#include <libxml/parser.h>

typedef struct _DataFileEntry	DataFileEntry;
typedef struct _DataDirList		DataDirList;
typedef struct _DataAddData		DataAddData;
typedef struct _DataImageData	DataImageData;
	
struct _DataFileEntry
{
	gchar *hd_path;
	gchar *cd_path;
	guint size;
};

struct _DataDirList
{
	GList *files;
	gchar *base_path;
};

struct _DataAddData
{
	GList *files;
	GtkListStore *store;
};

GtkWidget *
opti_build_data_view (void);

void
opti_open_data_cd (GList *file_list);

void
opti_data_file_entry_free (gpointer data, gpointer user_data);

#endif /* _CD_DATA_H_ */
