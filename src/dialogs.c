/*
 *  Copyright 2003-2004 Todd Kulesza <todd@dropline.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h"

#include "optimystic.h"
#include "cd-lib.h"
#include "msg-queue.h"
#include "subsystem.h"
#include "dialogs.h"

#include <string.h>
#include <libgnomeui/gnome-about.h>
#include <libgnomevfs/gnome-vfs-ops.h>

typedef struct _BurnWidgets BurnWidgets;

struct _BurnWidgets
{
	GtkWidget *speed;
	GtkWidget *test;
};

OptiData *od;

/* Helper functions */

/* Build a bold GtkLabel* */

static GtkWidget *
build_header (const gchar *text)
{
	GtkWidget *label;
	gchar *markup;
	
	markup = g_strdup_printf ("<span weight=\"bold\">%s</span>", text);
	label = gtk_label_new (NULL);
	gtk_label_set_markup_with_mnemonic (GTK_LABEL (label), markup);
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	g_free (markup);
	
	return label;
}

/* Build a left-aligned GtkLabel* */

static GtkWidget *
build_label (const gchar *text, GtkSizeGroup *size_group)
{
	GtkWidget *label;
	
	label = gtk_label_new_with_mnemonic (text);
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	if (size_group)
		gtk_size_group_add_widget (size_group, label);
	
	return label;
}

/* Return a GtkVBox* with a 4-space indentation, contained in 'box' */

static GtkWidget *
build_indent_box (GtkWidget *box)
{
	GtkWidget *vbox, *hbox;
	GtkWidget *label;
	
	hbox = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (box), hbox, FALSE, FALSE, 0);

	label = gtk_label_new ("    ");
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	
	vbox = gtk_vbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (hbox), vbox, TRUE, TRUE, 0);
	
	return vbox;
}

static GtkWidget *
build_indent_box_with_header (const gchar *header_text, GtkWidget *box)
{
	GtkWidget *vbox, *lower_box;
	GtkWidget *header;
	
	vbox = gtk_vbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (box), vbox, FALSE, FALSE, 0);
	
	header = build_header (header_text);
	gtk_box_pack_start (GTK_BOX (vbox), header, FALSE, FALSE, 0);
	
	lower_box = build_indent_box (vbox);
	
	return lower_box;
}

/* Return a GtkEntry* which activates the default response and is linked to
 * 'label'.
 */

static GtkWidget *
build_entry (GtkWidget *label, gint length)
{
	GtkWidget *entry;
	
	entry = gtk_entry_new ();
	if (label)
		gtk_label_set_mnemonic_widget (GTK_LABEL (label), entry);
	gtk_entry_set_activates_default (GTK_ENTRY (entry), TRUE);
	if (length)
		gtk_entry_set_max_length (GTK_ENTRY (entry), length);
	
	return entry;
}

/* Build a progress dialog that can be used for the various burning/image-writing
 * methods.
 */

static GtkWidget*
build_progress_dialog (GtkWidget *parent, const gchar *msg, const gchar *title)
{
	GtkWidget *dialog;
	GtkWidget *box, *label, *progress;

	dialog = gtk_dialog_new ();
	gtk_dialog_set_has_separator (GTK_DIALOG (dialog), FALSE);
	gtk_window_set_transient_for (GTK_WINDOW (dialog), GTK_WINDOW (parent));
	gtk_window_set_modal (GTK_WINDOW (dialog), TRUE);
	gtk_window_set_resizable (GTK_WINDOW (dialog), FALSE);
	if (title)
		gtk_window_set_title (GTK_WINDOW (dialog), title);
	else
		gtk_window_set_title (GTK_WINDOW (dialog), "");
	
	box = gtk_vbox_new (FALSE, 12);
	gtk_container_set_border_width (GTK_CONTAINER (box), 12);
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox), box, TRUE, TRUE, 0);
	
	label = gtk_label_new (msg);
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_box_pack_start (GTK_BOX (box), label, FALSE, FALSE, 0);
	
	progress = gtk_progress_bar_new ();
	gtk_box_pack_start (GTK_BOX (box), progress, FALSE, FALSE, 0);
	
	/* add 'label' and 'progress' data types to 'dialog' for use by msg-queue.c */
	g_object_set_data (G_OBJECT (dialog), "label", label);
	g_object_set_data (G_OBJECT (dialog), "progress", progress);
	
	return dialog;
}

/* Setup a progress dialog and set it as the active msg-queue widget. */
static void
setup_progress_dialog (GtkWidget *parent, const gchar *msg, const gchar *title)
{
	GtkWidget *dialog;
	MsgInfo *info;
	
	dialog = build_progress_dialog (parent, msg, title);
	
	gtk_widget_show_all (dialog);
	
	info = msg_info_new ();
	info->type = MSG_TYPE_SET_WIDGET;
	info->widget = dialog;
	g_async_queue_push (od->msg_queue, info);
	
	return;
}

/* Warn the user a file already exists and will be overwritten if they continue
 * the current operation.
 */

static gint
confirm_overwrite (GtkWidget *parent, const gchar *filename)
{
	GtkWidget *dialog;
	gint response;
	
	dialog = gtk_message_dialog_new (GTK_WINDOW (parent),
			GTK_DIALOG_DESTROY_WITH_PARENT,
			GTK_MESSAGE_QUESTION,
			GTK_BUTTONS_NONE,
			_("A file named \"%s\" already exists.  Do you want to replace "
			"it with the one you are saving?"), filename);
	gtk_dialog_add_buttons (GTK_DIALOG (dialog),
			GTK_STOCK_CANCEL, GTK_RESPONSE_REJECT,
			_("_Replace"), GTK_RESPONSE_ACCEPT,
			NULL);
	
	response = gtk_dialog_run (GTK_DIALOG (dialog));
	
	gtk_widget_destroy (dialog);
	
	return response;
}

/* This one is *totally* important! */

void
dlg_about (void)
{
	static GtkWidget *about = NULL;
	const gchar *authors [] =
	{
		"Todd Kulesza <todd@dropline.net>",
		NULL
	};
	const gchar *documenters [] =
	{
		NULL
	};
	const gchar *translator_credits = _("translator_credits");

	if (about)
	{
		gtk_window_present (GTK_WINDOW (about));
		return;
	}
	
	about = gnome_about_new ("Optimystic", VERSION, 
			"Copyright \xc2\xa9 2003-2004 Todd Kulesza",
			DISPLAY_NAME,
			authors,
			documenters,
			strcmp (translator_credits, "translator_credits") 
			!= 0 ? translator_credits : NULL,
			od->window_icon);
	
	gtk_window_set_transient_for (GTK_WINDOW (about), 
			GTK_WINDOW (od->window_main));
	
	g_object_add_weak_pointer (G_OBJECT (about), (void **)&about);
	
	gtk_widget_show_all (about);
		
	return;
}

static void
set_title_cb (GtkWidget *title, gpointer data)
{
	gchar *value;
	
	value = gtk_editable_get_chars (GTK_EDITABLE (title), 0, -1);
	cdfilelist_set_property (CDFILELIST (od->fl), CDFILELIST_TITLE, value);
	g_free (value);
	
	return;
}

static void
set_publisher_cb (GtkWidget *publisher, gpointer data)
{
	gchar *value;
	
	value = gtk_editable_get_chars (GTK_EDITABLE (publisher), 0, -1);
	cdfilelist_set_property (CDFILELIST (od->fl), CDFILELIST_PUB, value);
	g_free (value);
	
	return;
}

static void
set_copyright_cb (GtkWidget *copyright, gpointer data)
{
	gchar *value;
	
	value = gtk_editable_get_chars (GTK_EDITABLE (copyright), 0, -1);
	cdfilelist_set_property (CDFILELIST (od->fl), CDFILELIST_CR, value);
	g_free (value);
	
	return;
}

/* Allow user to enter disc properties such as 'Title' and 'Publisher' */

void
dlg_properties (void)
{
	GtkWidget *dialog;
	GtkWidget *vbox, *hbox;
	GtkWidget *label;
	GtkWidget *title, *publisher, *copyright;
	GtkSizeGroup *size_group;
	const gchar *value;
	
	dialog = gtk_dialog_new_with_buttons (_("Disc Properties"),
			GTK_WINDOW (od->window_main),
			GTK_DIALOG_MODAL,
			GTK_STOCK_CLOSE, GTK_RESPONSE_CLOSE, 
			NULL);
	gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_CLOSE);
	gtk_dialog_set_has_separator (GTK_DIALOG (dialog), FALSE);
	
	vbox = gtk_vbox_new (FALSE, 6);
	gtk_container_set_border_width (GTK_CONTAINER (vbox), 12);
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox), vbox, TRUE, TRUE, 0);
	
	size_group = gtk_size_group_new (GTK_SIZE_GROUP_BOTH);
	
	/* Title */
	hbox = gtk_hbox_new (FALSE, 12);
	gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);
	
	label = build_label (_("Disc _title:"), size_group);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	
	title = build_entry (label, 32);
	value = cdfilelist_get_property (CDFILELIST (od->fl), CDFILELIST_TITLE);
	if (value)
		gtk_entry_set_text (GTK_ENTRY (title), value);
	gtk_box_pack_start (GTK_BOX (hbox), title, TRUE, TRUE, 0);
	
	g_signal_connect (G_OBJECT (title), "focus_out_event",
			G_CALLBACK (set_title_cb), NULL);
	
	/* Publisher */
	hbox = gtk_hbox_new (FALSE, 12);
	gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);
	
	label = build_label (_("_Publisher:"), size_group);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	
	publisher = build_entry (label, 128);
	value = cdfilelist_get_property (CDFILELIST (od->fl), CDFILELIST_PUB);
	if (value)
		gtk_entry_set_text (GTK_ENTRY (publisher), value);
	gtk_box_pack_start (GTK_BOX (hbox), publisher, TRUE, TRUE, 0);
	
	g_signal_connect (G_OBJECT (publisher), "focus_out_event",
			G_CALLBACK (set_publisher_cb), NULL);
	
	/* Copyright */
	hbox = gtk_hbox_new (FALSE, 12);
	gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);
	
	label = build_label (_("Copy_right:"), size_group);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	
	copyright = build_entry (label, 128);
	value = cdfilelist_get_property (CDFILELIST (od->fl), CDFILELIST_CR);
	if (value)
		gtk_entry_set_text (GTK_ENTRY (copyright), value);
	gtk_box_pack_start (GTK_BOX (hbox), copyright, TRUE, TRUE, 0);
	
	g_signal_connect (G_OBJECT (copyright), "focus_out_event",
			G_CALLBACK (set_copyright_cb), NULL);
	
	gtk_widget_show_all (dialog);
	
	gtk_dialog_run (GTK_DIALOG (dialog));
	
	gtk_widget_destroy (dialog);
	
	return;
}

/* The ubiquitous preferences dialog */

void
dlg_preferences (void)
{
	GtkWidget *dialog;
	GtkWidget *vbox;
	GtkWidget *label;
	gchar *title;
	
	title = g_strdup_printf (_("%s Preferences"), DISPLAY_NAME);
	
	dialog = gtk_dialog_new_with_buttons (title,
			GTK_WINDOW (od->window_main),
			GTK_DIALOG_MODAL,
			GTK_STOCK_CLOSE, GTK_RESPONSE_CLOSE, 
			NULL);
	gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_CLOSE);
	
	vbox = gtk_vbox_new (FALSE, 12);
	gtk_container_set_border_width (GTK_CONTAINER (vbox), 12);
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox), vbox, TRUE, TRUE, 0);
	
	label = gtk_label_new ("No preferences yet.");
	gtk_box_pack_start (GTK_BOX (vbox), label, TRUE, TRUE, 0);
	
	gtk_widget_show_all (dialog);
	
	gtk_dialog_run (GTK_DIALOG (dialog));
	
	gtk_widget_destroy (dialog);
	
	g_free (title);
	
	return;
}

/* Get the filename to save the disc as and ensure a file doesn't already
 * exist with the same name.  Return FALSE if the user canceled the save.
 */

static gboolean
save_disc (GtkWidget *dialog)
{
	const gchar *filename;
	gchar *filename_utf8, *last_dir;
	GnomeVFSFileInfo info;
	GnomeVFSResult result;
	gint response;
	gboolean retval;
	
	filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));
	filename_utf8 = g_filename_to_utf8 (filename, -1, NULL, NULL, NULL);
	
	result = gnome_vfs_get_file_info (filename_utf8, &info, GNOME_VFS_FILE_INFO_DEFAULT);
	
	if (result != GNOME_VFS_ERROR_NOT_FOUND)
		response = confirm_overwrite (dialog, filename_utf8);
	else
		response = GTK_RESPONSE_ACCEPT;
	
	if (response == GTK_RESPONSE_ACCEPT)
	{
		cdfilelist_save_to_file (CDFILELIST (od->fl), filename_utf8, NULL);
		
		last_dir = g_path_get_dirname (filename_utf8);
		if (last_dir)
		{
			gconf_client_set_string (od->gconf, "/apps/optimystic/last_dir", 
					last_dir, NULL);
		}
		g_free (last_dir);
		
		retval = TRUE;
	}
	else
		retval = FALSE;
	
	g_free (filename_utf8);
	
	return retval;
}

/* Prompt the user for a filename to save the disc as if no filename has
 * been selected yet.  Return FALSE if the user cancels the save action.
 */

gboolean
dlg_save_disc (gboolean save_as)
{
	GtkWidget *dialog;
	const gchar *filename;
	gchar *last_dir_utf8, *last_dir_local;
	gint result;
	gboolean retval, loop;
	
	filename = cdfilelist_get_property (CDFILELIST (od->fl), CDFILELIST_FILE);
	retval = TRUE;
	loop = TRUE;
	
	if (save_as || !filename)
	{
		/* Set the path to the last folder a file was opened from */
		last_dir_utf8 = gconf_client_get_string (od->gconf, 
				"/apps/optimystic/last_dir", NULL);
		if (last_dir_utf8)
			last_dir_local = g_filename_from_utf8 (last_dir_utf8, -1, NULL, NULL, NULL);
		else
			last_dir_local = NULL;
	
		dialog = gtk_file_chooser_dialog_new (_("Save Disc"), 
				GTK_WINDOW (od->window_main),
				GTK_FILE_CHOOSER_ACTION_SAVE,
				GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
				GTK_STOCK_SAVE, GTK_RESPONSE_OK,
				NULL);
		gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_OK);
		
		if (last_dir_local)
		{
			gtk_file_chooser_set_current_folder (GTK_FILE_CHOOSER (dialog), 
					last_dir_local);
		}

		result = gtk_dialog_run (GTK_DIALOG (dialog));
		
		while ((result == GTK_RESPONSE_OK) && loop)
		{
			retval = save_disc (dialog);
			
			if (!retval)
				result = gtk_dialog_run (GTK_DIALOG (dialog));
			else
				loop = FALSE;
		}
		
		if (result != GTK_RESPONSE_OK)
			retval = FALSE;
		
		gtk_widget_destroy (dialog);
		
		g_free (last_dir_utf8);
		g_free (last_dir_local);
	}
	else
		cdfilelist_save_to_file (CDFILELIST (od->fl), filename, NULL);
	
	return retval;
}

/* Parse 'filename' to determine its CDFileListType */

static CDFileListType
get_type_from_file (const gchar *filename)
{
	xmlDocPtr doc;
	xmlNodePtr node;
	CDFileListType type;
	gchar *string;
	
	type = CDFILELIST_UNKNOWN;
	
	if (!(doc = xmlParseFile (filename)))
		return type;
	
	if ((node = xmlDocGetRootElement (doc)))
	{
		string = xmlGetProp (node, "type");
		if (string)
			type = g_strtod (string, NULL);
		else
			type = CDFILELIST_UNKNOWN;
		g_free (string);
	}
	
	xmlFreeDoc (doc);
	
	return type;
}

/* Attempt to open the filename selected in 'dialog', return FALSE on failure. */

static gboolean
open_disc (GtkWidget *dialog)
{
	const gchar *filename;
	gchar *filename_utf8, *last_dir, *err_str;
	GnomeVFSFileInfo info;
	GnomeVFSResult result;
	gboolean retval;
	CDFileListType type;
	
	retval = TRUE;
	
	filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));
	filename_utf8 = g_filename_to_utf8 (filename, -1, NULL, NULL, NULL);
	
	/* Check for some obvious file problems */
	result = gnome_vfs_get_file_info (filename_utf8, &info, GNOME_VFS_FILE_INFO_DEFAULT);
	if (result != GNOME_VFS_OK)
	{
		switch (result)
		{
			case GNOME_VFS_ERROR_NOT_FOUND:
			{
				err_str = g_strdup_printf (_("The file '%s' could not be "
						"found."), filename_utf8);
				break;
			}
			case GNOME_VFS_ERROR_ACCESS_DENIED:
			{
				err_str = g_strdup_printf (_("You do not have permission to "
						"access '%s'."), filename_utf8);
				break;
			}
			default:
			{
				err_str = g_strdup_printf (_("Unable to open the file '%s'."),
						filename_utf8);
				break;
			}
		}
		
		opti_string_error (dialog, err_str);
		
		g_free (err_str);
		
		retval = FALSE;
	}
	else
	{
		type = get_type_from_file (filename_utf8);
		
		/* Warn the user 'filename_utf8' could not be parsed. */
		if (type == CDFILELIST_UNKNOWN)
		{
			err_str = g_strdup_printf (_("Could not open '%s'.  The file does not "
				"appear to be a saved disc."), filename_utf8);
			opti_string_error (dialog, err_str);
			g_free (err_str);
			
			retval = FALSE;
		}
		else
		{
			opti_new_disc (type);
		
			cdfilelist_load_from_file (CDFILELIST (od->fl), filename_utf8, NULL);
			
			last_dir = g_path_get_dirname (filename_utf8);
			if (last_dir)
			{
				gconf_client_set_string (od->gconf, "/apps/optimystic/last_dir", 
						last_dir, NULL);
			}
			g_free (last_dir);
		}
	}
	
	g_free (filename_utf8);
	
	return retval;
}

/* Prompt the user for a filename to open. */

void
dlg_open_disc (void)
{
	GtkWidget *dialog;
	gint result;
	gboolean retval, loop;
	gchar *last_dir_utf8, *last_dir_local;
	
	loop = TRUE;
	
	/* Set the path to the last folder a file was opened from */
	last_dir_utf8 = gconf_client_get_string (od->gconf, 
			"/apps/optimystic/last_dir", NULL);
	if (last_dir_utf8)
		last_dir_local = g_filename_from_utf8 (last_dir_utf8, -1, NULL, NULL, NULL);
	else
		last_dir_local = NULL;
	
	dialog = gtk_file_chooser_dialog_new (_("Open Disc"),
			GTK_WINDOW (od->window_main),
			GTK_FILE_CHOOSER_ACTION_OPEN,
			GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
			GTK_STOCK_OPEN, GTK_RESPONSE_OK,
			NULL);
	gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_OK);
	
	if (last_dir_local)
	{
		gtk_file_chooser_set_current_folder (GTK_FILE_CHOOSER (dialog), 
				last_dir_local);
	}
	
	result = gtk_dialog_run (GTK_DIALOG (dialog));
	
	while ((result == GTK_RESPONSE_OK) && loop)
	{
		retval = open_disc (dialog);
		
		if (!retval)
			result = gtk_dialog_run (GTK_DIALOG (dialog));
		else
			loop = FALSE;
	}
	
	gtk_widget_destroy (dialog);
	
	g_free (last_dir_utf8);
	g_free (last_dir_local);
	
	return;
}

/* Prompt the user to save the disc or not, and allow her to cancel whatever
 * operation threw this dialog.
 */

gboolean
dlg_confirm_close (void)
{
	gboolean cancel;
	gint result;
	GtkWidget *dialog;
	
	if (!od->fl || (!cdfilelist_get_modified (CDFILELIST (od->fl))))
		return FALSE;
	
	dialog = gtk_message_dialog_new (GTK_WINDOW (od->window_main), 
			GTK_DIALOG_MODAL, 
			GTK_MESSAGE_QUESTION, 
			GTK_BUTTONS_NONE, 
			_("Do you want to save the changes you made to this disc?"));
	gtk_dialog_add_buttons (GTK_DIALOG (dialog),
			_("_Don't save"), GTK_RESPONSE_NO,
			GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
			GTK_STOCK_SAVE, GTK_RESPONSE_YES,
			NULL);
	gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_CANCEL);
	
	result = gtk_dialog_run (GTK_DIALOG (dialog));
	
	gtk_widget_destroy (dialog);
	
	cancel = TRUE;
	
	if (result == GTK_RESPONSE_YES)
	{
		/* If the user canceled the save, act as if she canceled the
		 * Confirm Close dialog.
		 */
		if (dlg_save_disc (FALSE))
			cancel = FALSE;
	}
	else if (result == GTK_RESPONSE_NO)
		cancel = FALSE;
	
	return cancel;
}

/* Prompt the user for the filename to save the ISO as. */

static gchar *
choose_image_file (GtkWidget *parent)
{
	GtkWidget *dialog;
	GnomeVFSFileInfo info;
	GnomeVFSResult result;
	const gchar *image;
	gchar *image_utf8, *last_dir_utf8, *last_dir_local;
	gint response;
	
	image_utf8 = NULL;
	
	/* Set the path to the last folder a file was opened from */
	last_dir_utf8 = gconf_client_get_string (od->gconf, 
			"/apps/optimystic/last_dir", NULL);
	if (last_dir_utf8)
		last_dir_local = g_filename_from_utf8 (last_dir_utf8, -1, NULL, NULL, NULL);
	else
		last_dir_local = NULL;
		
	dialog = gtk_file_chooser_dialog_new (_("Save Image File as"),
			GTK_WINDOW (parent),
			GTK_FILE_CHOOSER_ACTION_SAVE,
			GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
			GTK_STOCK_SAVE, GTK_RESPONSE_OK,
			NULL);
	gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_OK);
	
	if (last_dir_local)
	{
		gtk_file_chooser_set_current_folder (GTK_FILE_CHOOSER (dialog), 
				last_dir_local);
	}
	
	response = gtk_dialog_run (GTK_DIALOG (dialog));
	
	while (response == GTK_RESPONSE_OK)
	{
		image = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));
		image_utf8 = g_filename_to_utf8 (image, -1, NULL, NULL, NULL);
		
		result = gnome_vfs_get_file_info (image_utf8, &info, GNOME_VFS_FILE_INFO_DEFAULT);
		if (result != GNOME_VFS_ERROR_NOT_FOUND)
			response = confirm_overwrite (dialog, image_utf8);
		else
			response = GTK_RESPONSE_ACCEPT;
		
		if (response != GTK_RESPONSE_ACCEPT)
		{
			g_free (image_utf8);
			image_utf8 = NULL;
			response = gtk_dialog_run (GTK_DIALOG (dialog));
		}
	}
	
	gtk_widget_destroy (dialog);
	
	return image_utf8;
}

/* Return a GtkMenu* with the available CD burners */

static GtkWidget *
build_device_menu (gboolean include_image)
{
	GtkWidget *menu, *menuitem;
	gint i;
	
	menu = gtk_menu_new ();
	
	for (i = 0; i < od->n_cd_writable_drives; i++)
	{
		menuitem = gtk_menu_item_new_with_label (od->cd_writable_drives[i].product);
		gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
	}
	
	if (include_image)
	{
		menuitem = gtk_menu_item_new_with_label (_("Image file"));
		gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
	}
	
	return menu;
}

/* Return a GtkMenu* with available speeds for burning */

static GtkWidget *
build_speed_menu (void)
{
	GtkWidget *menu, *menuitem;
	
	menu = gtk_menu_new ();
	
	menuitem = gtk_menu_item_new_with_label (_("Fastest possible"));
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
	
	menuitem = gtk_menu_item_new_with_label ("1X");
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
	
	return menu;
}

/* Set the sensitivity of members of 'bw' based on the selected state of 
 * 'write_to'.
 */

static void
write_to_changed (GtkOptionMenu *write_to, BurnWidgets *bw)
{
	gint index;
	
	/* FIXME: also update the speed menu if a new burner is selected */
	
	index = gtk_option_menu_get_history (write_to);

	/* If the index is <= to the number of drives, it's the virtual drive */	
	gtk_widget_set_sensitive (bw->speed, (index < od->n_cd_writable_drives));
	gtk_widget_set_sensitive (bw->test, (index < od->n_cd_writable_drives));
	
	return;
}

/* Display a dialog with relevant properties for burning a CD */

void
dlg_burn_disc (void)
{
	static GtkWidget *dialog = NULL;
	GtkWidget *vbox, *vbox2, *hbox;
	GtkWidget *label, *menu;
	GtkWidget *test, *write_to, *speed, *title;
	GtkSizeGroup *size_group;
	BurnWidgets *bw;
	gint result, index;
	gchar *image, *msg;
	const gchar *value;
	
	if (dialog)
	{
		gtk_window_present (GTK_WINDOW (dialog));
		return;
	}
	
	dialog = gtk_dialog_new_with_buttons (_("Write to CD"), 
			GTK_WINDOW (od->window_main),
			GTK_DIALOG_DESTROY_WITH_PARENT,
			GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
			_("_Write"), GTK_RESPONSE_ACCEPT,
			NULL);
	gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_ACCEPT);
	gtk_dialog_set_has_separator (GTK_DIALOG (dialog), FALSE);
	
	vbox = gtk_vbox_new (FALSE, 18);
	gtk_container_set_border_width (GTK_CONTAINER (vbox), 12);
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox), vbox, TRUE, TRUE, 0);
	
	bw = g_new0 (BurnWidgets, 1);
	
	size_group = gtk_size_group_new (GTK_SIZE_GROUP_BOTH);
	
	/* Disc Properties */
	vbox2 = build_indent_box_with_header (_("Disc Properties"), vbox);
		
	/* Title */
	hbox = gtk_hbox_new (FALSE, 12);
	gtk_box_pack_start (GTK_BOX (vbox2), hbox, FALSE, FALSE, 0);
	
	label = build_label (_("Disc _title:"), size_group);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	
	title = build_entry (label, 32);
	value = cdfilelist_get_property (CDFILELIST (od->fl), CDFILELIST_TITLE);
	if (value)
		gtk_entry_set_text (GTK_ENTRY (title), value);
	gtk_box_pack_start (GTK_BOX (hbox), title, TRUE, TRUE, 0);
	
	g_signal_connect (G_OBJECT (title), "focus_out_event",
			G_CALLBACK (set_title_cb), NULL);
	
	/* Writing Options */
	vbox2 = build_indent_box_with_header (_("Writing Options"), vbox);
	
	/* Device */
	hbox = gtk_hbox_new (FALSE, 12);
	gtk_box_pack_start (GTK_BOX (vbox2), hbox, FALSE, FALSE, 0);
	
	label = build_label (_("W_rite to:"), size_group);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	
	menu = build_device_menu (TRUE);
	
	write_to = gtk_option_menu_new ();
	gtk_label_set_mnemonic_widget (GTK_LABEL (label), write_to);
	gtk_option_menu_set_menu (GTK_OPTION_MENU (write_to), menu);
	gtk_box_pack_start (GTK_BOX (hbox), write_to, TRUE, TRUE, 0);
	
	g_signal_connect (G_OBJECT (write_to), "changed",
			G_CALLBACK (write_to_changed), bw);
	
	/* Speed */
	hbox = gtk_hbox_new (FALSE, 12);
	gtk_box_pack_start (GTK_BOX (vbox2), hbox, FALSE, FALSE, 0);
	
	label = build_label (_("_Speed:"), size_group);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	
	menu = build_speed_menu ();
	
	speed = gtk_option_menu_new ();
	gtk_label_set_mnemonic_widget (GTK_LABEL (label), speed);
	gtk_option_menu_set_menu (GTK_OPTION_MENU (speed), menu);
	gtk_box_pack_start (GTK_BOX (hbox), speed, TRUE, TRUE, 0);
	bw->speed = speed;
	
	/* Simulate */
	test = gtk_check_button_new_with_mnemonic (_("Test _before writing"));
	gtk_box_pack_start (GTK_BOX (vbox2), test, FALSE, FALSE, 0);
	bw->test = test;
	
	write_to_changed (GTK_OPTION_MENU (write_to), bw);
	
	/* run the dialog */
	gtk_widget_show_all (dialog);
	
	result = gtk_dialog_run (GTK_DIALOG (dialog));
	if (result == GTK_RESPONSE_ACCEPT)
	{
		index = gtk_option_menu_get_history (GTK_OPTION_MENU (write_to));
		/* a physical burner was selected */
		if (index < od->n_cd_writable_drives)
		{
			g_warning ("not implemented yet, sorry!\n");
			gtk_widget_destroy (dialog);
			dialog = NULL;
		}
		else
		{
			image = choose_image_file (dialog);
			gtk_widget_destroy (dialog);
			dialog = NULL;
			if (image)
			{
				msg = g_strdup_printf (_("Writing image to %s..."), image);
				setup_progress_dialog (od->window_main, msg, NULL);
				write_image_to_file (CDFILELIST (od->fl), image, od->msg_queue);
				g_free (image);
			}
		}
	}
	else
	{
		gtk_widget_destroy (dialog);
		dialog = NULL;
	}
	
	return;
}

static void
open_image_file (GtkWidget *button, GtkWidget *entry)
{
	GtkWidget *dialog, *parent;
	GnomeVFSFileInfo info;
	GnomeVFSResult result;
	GtkFileFilter *filter;
	const gchar *image;
	gchar *image_utf8, *last_dir_utf8, *last_dir_local;
	gint response;
	
	image_utf8 = NULL;
	parent = g_object_get_data (G_OBJECT (entry), "parent");
	
	/* filter out non-ISO files */
	filter = gtk_file_filter_new ();
	gtk_file_filter_add_mime_type (filter, "application/x-iso-image");
	gtk_file_filter_add_mime_type (filter, "application/x-cd-image");
	
	/* Set the path to the last folder a file was opened from */
	last_dir_utf8 = gconf_client_get_string (od->gconf, 
			"/apps/optimystic/last_dir", NULL);
	if (last_dir_utf8)
		last_dir_local = g_filename_from_utf8 (last_dir_utf8, -1, NULL, NULL, NULL);
	else
		last_dir_local = NULL;
	
	dialog = gtk_file_chooser_dialog_new (_("Select Image File"),
			GTK_WINDOW (parent),
			GTK_FILE_CHOOSER_ACTION_OPEN,
			GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
			GTK_STOCK_OPEN, GTK_RESPONSE_OK,
			NULL);
	gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_OK);
	gtk_file_chooser_set_filter (GTK_FILE_CHOOSER (dialog), filter);
	
	if (last_dir_local)
	{
		gtk_file_chooser_set_current_folder (GTK_FILE_CHOOSER (dialog), 
				last_dir_local);
	}
	
	response = gtk_dialog_run (GTK_DIALOG (dialog));
	
	while (response == GTK_RESPONSE_OK)
	{
		image = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));
		image_utf8 = g_filename_to_utf8 (image, -1, NULL, NULL, NULL);
		
		result = gnome_vfs_get_file_info (image_utf8, &info, GNOME_VFS_FILE_INFO_DEFAULT);
		if (result != GNOME_VFS_ERROR_NOT_FOUND)
			response = GTK_RESPONSE_ACCEPT;
		
		if (response != GTK_RESPONSE_ACCEPT)
		{
			g_free (image_utf8);
			image_utf8 = NULL;
			response = gtk_dialog_run (GTK_DIALOG (dialog));
		}
		else
		{
			gtk_entry_set_text (GTK_ENTRY (entry), image_utf8);
			g_free (image_utf8);
		}
	}
	
	gtk_widget_destroy (dialog);
	
	return;
}

static void
image_file_changed (GtkWidget *editable, GtkWidget *dialog)
{
	gchar *text;
	gint len;
	
	text = gtk_editable_get_chars (GTK_EDITABLE (editable), 0, -1);
	len = strlen (text);
	gtk_dialog_set_response_sensitive (GTK_DIALOG (dialog), 
			GTK_RESPONSE_ACCEPT, len);
	g_free (text);
	
	return;
}

/* Display a dialog with properties relevant to burning an ISO image */

void
dlg_burn_image (void)
{
	static GtkWidget *dialog = NULL;
	GtkWidget *vbox, *vbox2, *hbox, *hbox2;
	GtkWidget *label, *spacer, *menu;
	GtkWidget *image_file, *test, *write_to, *speed, *image_file_selector;
	GtkSizeGroup *size_group;
	BurnWidgets *bw;
	gint result;
	
	if (dialog)
	{
		gtk_window_present (GTK_WINDOW (dialog));
		return;
	}
	
	dialog = gtk_dialog_new_with_buttons (_("Write Image to CD"), 
			GTK_WINDOW (od->window_main),
			GTK_DIALOG_DESTROY_WITH_PARENT,
			GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
			_("_Write"), GTK_RESPONSE_ACCEPT,
			NULL);
	gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_ACCEPT);
	gtk_dialog_set_has_separator (GTK_DIALOG (dialog), FALSE);
	gtk_dialog_set_response_sensitive (GTK_DIALOG (dialog), 
			GTK_RESPONSE_ACCEPT, FALSE);
	
	vbox = gtk_vbox_new (FALSE, 18);
	gtk_container_set_border_width (GTK_CONTAINER (vbox), 12);
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox), vbox, TRUE, TRUE, 0);
	
	bw = g_new0 (BurnWidgets, 1);
	
	size_group = gtk_size_group_new (GTK_SIZE_GROUP_BOTH);
	
	/* Image File */
	vbox2 = gtk_vbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (vbox), vbox2, FALSE, FALSE, 0);
	
	label = build_header (_("_Image File"));
	gtk_box_pack_start (GTK_BOX (vbox2), label, FALSE, FALSE, 0);
	
	hbox = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (vbox2), hbox, FALSE, FALSE, 0);
	
	spacer = gtk_label_new ("    ");
	gtk_box_pack_start (GTK_BOX (hbox), spacer, FALSE, FALSE, 0);
	
	hbox2 = gtk_hbox_new (FALSE, 12);
	gtk_box_pack_start (GTK_BOX (hbox), hbox2, TRUE, TRUE, 0);
	
	image_file = gtk_entry_new ();
	gtk_label_set_mnemonic_widget (GTK_LABEL (label), image_file);
	/* set 'parent' to the parent dialog so that the open_image_file() method
	   can be a transient of this dialog */
	g_object_set_data (G_OBJECT (image_file), "parent", dialog);
	gtk_box_pack_start (GTK_BOX (hbox2), image_file, TRUE, TRUE, 0);
	
	g_signal_connect (G_OBJECT (image_file), "changed",
			G_CALLBACK (image_file_changed), dialog);
			
	image_file_selector = gtk_button_new_with_mnemonic (_("_Browse..."));
	gtk_box_pack_start (GTK_BOX (hbox2), image_file_selector, FALSE, FALSE, 0);
	
	g_signal_connect (G_OBJECT (image_file_selector), "clicked",
			G_CALLBACK (open_image_file), image_file);
	
	/* Writing Options */
	vbox2 = build_indent_box_with_header (_("Writing Options"), vbox);
	
	/* Device */
	hbox = gtk_hbox_new (FALSE, 12);
	gtk_box_pack_start (GTK_BOX (vbox2), hbox, FALSE, FALSE, 0);
	
	label = build_label (_("W_rite to:"), size_group);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	
	menu = build_device_menu (FALSE);
	
	write_to = gtk_option_menu_new ();
	gtk_label_set_mnemonic_widget (GTK_LABEL (label), write_to);
	gtk_option_menu_set_menu (GTK_OPTION_MENU (write_to), menu);
	gtk_box_pack_start (GTK_BOX (hbox), write_to, TRUE, TRUE, 0);
	
	g_signal_connect (G_OBJECT (write_to), "changed",
			G_CALLBACK (write_to_changed), bw);
	
	/* Speed */
	hbox = gtk_hbox_new (FALSE, 12);
	gtk_box_pack_start (GTK_BOX (vbox2), hbox, FALSE, FALSE, 0);
	
	label = build_label (_("_Speed:"), size_group);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	
	menu = build_speed_menu ();
	
	speed = gtk_option_menu_new ();
	gtk_label_set_mnemonic_widget (GTK_LABEL (label), speed);
	gtk_option_menu_set_menu (GTK_OPTION_MENU (speed), menu);
	gtk_box_pack_start (GTK_BOX (hbox), speed, TRUE, TRUE, 0);
	bw->speed = speed;
	
	/* Simulate */
	test = gtk_check_button_new_with_mnemonic (_("Test _before writing"));
	gtk_box_pack_start (GTK_BOX (vbox2), test, FALSE, FALSE, 0);
	bw->test = test;
	
	write_to_changed (GTK_OPTION_MENU (write_to), bw);
	
	/* run the dialog */
	gtk_widget_show_all (dialog);
	
	result = gtk_dialog_run (GTK_DIALOG (dialog));
	if (result == GTK_RESPONSE_ACCEPT)
	{
		gint index;
		gchar *image;
		
		index = gtk_option_menu_get_history (GTK_OPTION_MENU (write_to));
		image = gtk_editable_get_chars (GTK_EDITABLE (image_file), 0, -1);
		
		/* a physical burner was selected */
		if (index < od->n_cd_writable_drives)
		{
			gboolean simulate = gtk_toggle_button_get_active (
					GTK_TOGGLE_BUTTON (test));
			
			gtk_widget_destroy (dialog);
			dialog = NULL;
			
			setup_progress_dialog (od->window_main, _("Writing image to CD..."), 
					NULL);
			write_image_to_cd (&od->cd_writable_drives[index], image, simulate, 
					od->msg_queue);
		}
		else
		{
			g_warning ("This file is already an ISO--what are you doing?");
			gtk_widget_destroy (dialog);
			dialog = NULL;
		}
		
		g_free (image);
	}
	else
	{
		gtk_widget_destroy (dialog);
		dialog = NULL;
	}
	
	return;
}

/* confirm that the user really wants to erase this disc */

void
dlg_blank_disc (void)
{
	static GtkWidget *dialog = NULL;
	GtkWidget *vbox, *hbox, *label, *menu, *drive;
	gchar *msg;
	gint result;
	
	if (dialog)
	{
		gtk_window_present (GTK_WINDOW (dialog));
		return;
	}
	
	dialog = gtk_dialog_new_with_buttons (_("Erase CD"),
			GTK_WINDOW (od->window_main),
			GTK_DIALOG_DESTROY_WITH_PARENT,
			GTK_STOCK_CANCEL, GTK_RESPONSE_REJECT,
			_("_Erase"), GTK_RESPONSE_ACCEPT,
			NULL);
	gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_REJECT);
	gtk_dialog_set_has_separator (GTK_DIALOG (dialog), FALSE);
	
	vbox = gtk_vbox_new (FALSE, 18);
	gtk_container_set_border_width (GTK_CONTAINER (vbox), 12);
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox), vbox, TRUE, TRUE, 0);
	
	msg = g_strdup_printf ("<span weight=\"bold\" size=\"larger\">%s</span>\n\n"
			"%s\n%s", "You are about to permanently erase a CD-RW.",
			"The operation you are about to perform can not be undone.",
			"All files on the disc will be erased.");
	label = gtk_label_new (NULL);
	gtk_label_set_markup (GTK_LABEL (label), msg);
	gtk_box_pack_start (GTK_BOX (vbox), label, FALSE, FALSE, 0);
	
	/* allow the user to choose which drive to erase from */
	hbox = gtk_hbox_new (FALSE, 12);
	gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);
	
	label = gtk_label_new_with_mnemonic (_("Erase the _disc in:"));
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	
	menu = build_device_menu (FALSE);
	
	drive = gtk_option_menu_new ();
	gtk_label_set_mnemonic_widget (GTK_LABEL (label), drive);
	gtk_option_menu_set_menu (GTK_OPTION_MENU (drive), menu);
	gtk_box_pack_start (GTK_BOX (hbox), drive, TRUE, TRUE, 0);
	
	/* run the dialog */
	gtk_widget_show_all (dialog);
	result = gtk_dialog_run (GTK_DIALOG (dialog));
	if (result == GTK_RESPONSE_ACCEPT)
	{
		gint index;
		
		index = gtk_option_menu_get_history (GTK_OPTION_MENU (drive));
		gtk_widget_destroy (dialog);
		dialog = NULL;
			
		setup_progress_dialog (od->window_main, _("Erasing files from CD..."), 
				NULL);
		erase_cd (&od->cd_writable_drives[index], od->msg_queue);
	}
	
	gtk_widget_destroy (dialog);
	dialog = NULL;
	
	g_free (msg);
	
	return;
}
