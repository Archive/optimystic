/*
 *  Copyright 2003-2004 Todd Kulesza <todd@dropline.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h"

#include "msg-queue.h"
#include "subsystem.h"

#include <string.h>
#include <libgnomevfs/gnome-vfs-ops.h>
#include <libburn/libisofs.h>
#include <libburn/libburn.h>

#define SECSIZE 2048

typedef struct _WriteImageData WriteImageData;

struct _WriteImageData
{
	CDFileList *list;
	struct burn_drive_info *drive;
	gboolean simulate;
	gchar *image;
	GAsyncQueue *msg_queue;
};

/* Free memory used by the WriteImageData structure */

static void
write_image_data_free (WriteImageData *data)
{
	g_free (data->image);
	g_free (data);
	
	return;
}

/* tell the GUI thread that the write operation has completed */

static void
end_of_thread (WriteImageData *data)
{
	MsgInfo *info;
	
	info = msg_info_new ();
	info->type = MSG_TYPE_DONE;
	g_async_queue_push (data->msg_queue, info);
	
	return;
}

/* Detect available CD writers on the system */

gint
drives_detect (struct burn_drive_info **drives, gint *n_drives)
{
	gint retval;
	
	retval = 1;

	if (burn_initialize ())
	{
		while (!burn_drive_scan (drives, n_drives));
		
		if (*n_drives)
			retval = 1;
		else
			retval = -1;
	}
	else
		retval = 0;

	return retval;
}

/* Free 'drives' structure and shutdown libburn */

void
drives_free (struct burn_drive_info *drives)
{
	if (drives)
		burn_drive_info_free (drives);
	
	burn_finish ();	
	
	return;
}

/* Return the number of G_DIR_SEPARATORs in 'string' */

static gint
count_separators (const gchar *string)
{
	gint i, len, count;
	
	len = strlen (string);
	for (i = count = 0; i < len; i++)
	{
		if (string [i] == G_DIR_SEPARATOR)
			count++;
	}
	
	return count;
}

/* Iterate through 'files', adding each to the iso_tree_dir. */

static void
add_recursive (struct iso_tree_dir **parent, GList **files, gint depth)
{
	struct iso_tree_dir **dir;
	CDFileListFile *f;
	gchar *folder;
	gint separators;
	
	while (*files)
	{
		f = (*files)->data;
		separators = count_separators (f->cd_uri);
		
		if (!g_ascii_strcasecmp (f->hd_uri, "folder"))
		{	
			/* If this file does not belong to this folder, return */
			if (separators < depth)
			{
				*files = g_list_previous (*files);
				return;
			}
			
			folder = g_path_get_basename (f->cd_uri);
			dir = iso_tree_add_dir (parent, folder, 0, NULL);
			g_free (folder);
			
			/* Descend into 'dir' and add the files/folders from 'files' with
			 * 'dir' as their parent, then return.
			 */
			*files = g_list_next (*files);
			add_recursive (dir, files, depth + 1);
		}
		else
		{
			/* If this file does not belong to this folder, return */
			if ((depth > 1) && (separators < depth))
			{
				*files = g_list_previous (*files);
				return;
			}

			iso_tree_add_file (parent, f->hd_uri, 0, NULL);
		}

		*files = g_list_next (*files);
	}

	return;
}

static void
add_files_to_image (struct iso_volumeset *volume, GList *files)
{
	GList *current;
	struct iso_tree_dir **parent;
	
	parent = iso_volumeset_get_root (volume);
	
	current = files;
	add_recursive (parent, &current, 1);
	
	parent = iso_volumeset_get_root (volume);
	
	g_warning ("Not finished yet, but here is the ISO file system:\n");
	iso_tree_print (parent);
	
	return;
}

/* Write an ISO of 'list' to a file named 'image' */

static gpointer
write_image_to_file_threaded (WriteImageData *data)
{
	struct iso_volumeset *volset;
	struct burn_source *src;
	enum burn_source_status st;
	unsigned char buf [SECSIZE];
	GnomeVFSResult result;
	GnomeVFSHandle *handle;
	gchar *volume_names [] = { "CDROM" };
	const gchar *publisher, *title, *copyright;
	GList *files;
	gint size, size_written;
	gdouble last_percent, percent;
	MsgInfo *info;
	
	result = gnome_vfs_open (&handle, data->image, GNOME_VFS_OPEN_WRITE);
	if (result != GNOME_VFS_OK)
	{
		result = gnome_vfs_create (&handle, data->image, GNOME_VFS_OPEN_WRITE, FALSE,
				GNOME_VFS_PERM_USER_READ |
				GNOME_VFS_PERM_USER_WRITE |
				GNOME_VFS_PERM_GROUP_READ |
				GNOME_VFS_PERM_OTHER_READ);
		
		if (result != GNOME_VFS_OK)
		{
			g_warning ("Failed to open '%s'\n", data->image);
			write_image_data_free (data);
			end_of_thread (data);
			return NULL;
		}
	}
	
	title = cdfilelist_get_property (data->list, CDFILELIST_TITLE);
	publisher = cdfilelist_get_property (data->list, CDFILELIST_PUB);
	/* FIXME: libburn doesn't support the copyright tag, yet */
	copyright = cdfilelist_get_property (data->list, CDFILELIST_CR);
	
	volset = iso_volumeset_new (1,
			title,
			volume_names,
			publisher,
			NULL,
			NULL,
			NULL);
	iso_volumeset_set_iso_level (volset, 1);
	iso_volumeset_set_rr (volset, 1);
	iso_volumeset_set_joliet (volset, 0);
	
	files = cdfilelist_get_files_to_burn (data->list);
	add_files_to_image (volset, files);
	
	src = iso_source_new (volset, 0);
	size = src->get_size(src);
	
	last_percent = 0.0;
	size_written = 0;
	
	/* write the ISO */
	st = iso_source_generate (src, buf, SECSIZE);
	while ((st == BURN_SOURCE_OK) && (size_written <= size))
	{
		gint w;
		
		for (w = 0; w < SECSIZE; )
		{
			GnomeVFSFileSize written;
			
			result = gnome_vfs_write (handle, buf + w, SECSIZE - w, &written);
			if (result != GNOME_VFS_OK)
			{
				g_warning ("error writing file\n");
				break;
			}
			
			w += written;
			size_written += written;
			
			/* update the progress bar, but only do it in 0.01-percent increments, so
			 * as to not spike the CPU usage just to have an up-to-the-millisecond
			 * progress indicator. */
			percent = (gdouble) size_written / (gdouble) size;
			if ((percent - last_percent) > 0.01)
			{
				info = msg_info_new ();
				info->type = MSG_TYPE_PROGRESS;
				info->progress = percent;
				g_async_queue_push (data->msg_queue, info);
				last_percent = percent;
			}
		}
		
		st = iso_source_generate (src, buf, SECSIZE);
	}
	
	if (st != BURN_SOURCE_EOF)
		g_warning ("Error in ISO generation\n");
	
	burn_source_free (src);
	iso_volumeset_free (volset);
	
	gnome_vfs_close (handle);
	
	end_of_thread (data);
	
	write_image_data_free (data);
	
	return NULL;
}

/* Write an ISO to a CD-R */

static gpointer
write_image_to_cd_threaded (WriteImageData *data)
{
	struct burn_source *src;
	struct burn_disc *disc;
	struct burn_session *session;
	struct burn_write_opts *o;
	enum burn_disc_status s;
	enum burn_drive_status sd;
	struct burn_track *tr;
	struct burn_progress p;
	MsgInfo *info;
	
	disc = burn_disc_create ();
	session = burn_session_create ();
	burn_disc_add_session (disc, session, BURN_POS_END);
	tr = burn_track_create ();
	burn_track_define_data (tr, 0, 0, 0, BURN_MODE1);
	src = burn_file_source_new (data->image, NULL);
	if (!src)
	{
		g_warning ("Could not create burn source.");
		end_of_thread (data);
		return NULL;
	}
	
	if (burn_track_set_source (tr, src) != BURN_SOURCE_OK)
	{
		g_warning ("Problem with the source.");
		end_of_thread (data);
		return NULL;
    }
	burn_session_add_track (session, tr, BURN_POS_END);
	burn_source_free (src);
	
	if (!burn_drive_grab (data->drive->drive, 1))
	{
		g_warning ("Unable to open the drive!");
		end_of_thread (data);
		return NULL;
	}
	
	while (burn_drive_get_status (data->drive->drive, NULL))
		g_usleep (1000);
	
	while ((s = burn_disc_get_status (data->drive->drive)) == BURN_DISC_UNREADY)
		g_usleep (1000);
	
	if (s != BURN_DISC_BLANK)
	{
		burn_drive_release (data->drive->drive, 0);
		g_print ("You need to put a blank in the drive.");
		end_of_thread (data);
		return NULL;
    }
	o = burn_write_opts_new (data->drive->drive);
	burn_write_opts_set_perform_opc (o, 0);
	burn_write_opts_set_write_type(o, BURN_WRITE_RAW, BURN_BLOCK_RAW96R);
	burn_write_opts_set_simulate (o, data->simulate);	
	burn_drive_set_speed (data->drive->drive, 0, 0);
	burn_disc_write (o, disc);
	burn_write_opts_free (o);
	
	while (burn_drive_get_status (data->drive->drive, NULL) 
			== BURN_DRIVE_SPAWNING) ;
	
	while ((sd = burn_drive_get_status (data->drive->drive, &p)))
	{
		gdouble percent;
		
		if (sd == BURN_DRIVE_WRITING_LEADIN)
			g_print ("writing leadin...\n");
		else if (sd == BURN_DRIVE_WRITING_LEADOUT)
			g_print ("writing leadout...\n");
		else if (sd == BURN_DRIVE_WRITING)
			g_print ("writing...\n");
		else
			g_print ("something else: %d\n", sd);
		
		g_print ("S: %d/%d ", p.session, p.sessions);
		g_print ("T: %d/%d ", p.track, p.tracks);
		g_print ("L: %d: %d/%d\n", p.start_sector, p.sector, p.sectors);
		
		percent = (gdouble) p.sector / (gdouble) p.sectors;
		info = msg_info_new ();
		info->type = MSG_TYPE_PROGRESS;
		info->progress = percent;
		g_async_queue_push (data->msg_queue, info);
		
		g_usleep (1000);
	}
	g_print ("\n");
	
	burn_drive_release (data->drive->drive, 1);
	burn_track_free (tr);
	burn_session_free (session);
	burn_disc_free (disc);
	
	end_of_thread (data);
	
	write_image_data_free (data);
	
	return NULL;
}

static gpointer
erase_cd_threaded (WriteImageData *data)
{
	enum burn_disc_status s;
	enum burn_drive_status sd;
	struct burn_progress p;
	MsgInfo *info;
	
	if (!burn_drive_grab (data->drive->drive, 1))
	{
		g_warning ("Unable to open the drive!");
		end_of_thread (data);
		return NULL;
	}
	
	while (burn_drive_get_status (data->drive->drive, NULL))
		g_usleep (1000);
	
	while ((s = burn_disc_get_status (data->drive->drive)) == BURN_DISC_UNREADY)
		g_usleep (1000);
	
	if (s != BURN_DISC_FULL)
	{
		burn_drive_release (data->drive->drive, 0);
		g_warning ("There are no files on this disc.");
		end_of_thread (data);
		return NULL;
    }
	
	if (!burn_disc_erasable (data->drive->drive))
	{
		burn_drive_release (data->drive->drive, 0);
		g_warning ("This disc is not erasable.");
		end_of_thread (data);
		return NULL;
	}
	
	burn_disc_erase (data->drive->drive, 1);
	
	while (burn_drive_get_status (data->drive->drive, NULL) 
			== BURN_DRIVE_SPAWNING) ;
	
	while ((sd = burn_drive_get_status (data->drive->drive, &p)))
	{
		gdouble percent;
		
		g_print ("S: %d/%d ", p.session, p.sessions);
		g_print ("T: %d/%d ", p.track, p.tracks);
		g_print ("L: %d: %d/%d\n", p.start_sector, p.sector, p.sectors);
		
		percent = (gdouble) p.sector / (gdouble) p.sectors;
		info = msg_info_new ();
		info->type = MSG_TYPE_PROGRESS;
		info->progress = percent;
		g_async_queue_push (data->msg_queue, info);
		
		g_usleep (1000);
	}
	g_print ("\n");
	
	burn_drive_release (data->drive->drive, 1);
	
	end_of_thread (data);
	
	write_image_data_free (data);
	
	return NULL;
}

/* Create a new thread to write an ISO image */

void
write_image_to_file (CDFileList *list, const gchar *image, GAsyncQueue *queue)
{
	WriteImageData *data;
	
	data = g_new0 (WriteImageData, 1);
	data->list = list;
	data->image = g_strdup (image);
	data->msg_queue = queue;
	
	g_thread_create ((GThreadFunc) write_image_to_file_threaded, data, FALSE, NULL);
	
	return;
}

/* Create a new thread to write an ISO image to CD */

void
write_image_to_cd (struct burn_drive_info *drive, const gchar *image,
		gboolean simulate, GAsyncQueue *queue)
{
	WriteImageData *data;
	
	data = g_new0 (WriteImageData, 1);
	data->image = g_strdup (image);
	data->drive = drive;
	data->simulate = simulate;
	data->msg_queue = queue;
	
	g_thread_create ((GThreadFunc) write_image_to_cd_threaded, data, FALSE, NULL);
	
	return;
}

/* Create a new thread to blank a rewritable disc */

void
erase_cd (struct burn_drive_info *drive, GAsyncQueue *queue)
{
	WriteImageData *data;
	
	data = g_new0 (WriteImageData, 1);
	data->image = NULL;
	data->drive = drive;
	data->msg_queue = queue;
	
	g_thread_create ((GThreadFunc) erase_cd_threaded, data, FALSE, NULL);
	
	return;
}
