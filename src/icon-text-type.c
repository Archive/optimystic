/*
 *  Copyright 2004 Todd Kulesza <todd@dropline.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "icon-text-type.h"

FileDisplay*
file_display_new (void)
{
	FileDisplay *fd;
	
	fd = g_new0 (FileDisplay, 1);
	fd->name = NULL;
	fd->icon = NULL;
	
	return fd;
}

void
file_display_free (FileDisplay *fd)
{
	if (!fd)
		return;
	
	g_free (fd->name);
	if (fd->icon)
	g_object_unref (fd->icon);

	g_free (fd);

	return;
}

FileDisplay*
file_display_copy (FileDisplay *src)
{
	FileDisplay *fd;

	fd = g_new0 (FileDisplay, 1);
	fd->name = g_strdup (src->name);
	fd->icon = src->icon;
	if (fd->icon)
		g_object_ref (fd->icon);

	return fd;
}

GType
file_display_get_type (void)
{
	static GType our_type = 0;

	if (our_type == 0)
		our_type = g_boxed_type_register_static ("FileDisplay",
				(GBoxedCopyFunc) file_display_copy,
				(GBoxedFreeFunc) file_display_free);
	
	return our_type;
}
