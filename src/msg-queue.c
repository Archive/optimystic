/*
 *  Copyright 2004 Todd Kulesza <todd@dropline.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h"

#include "optimystic.h"
#include "msg-queue.h"

#include <gtk/gtk.h>

OptiData *od;

static void
update_progress_bar (GtkWidget *dialog, gdouble percent)
{
	GtkWidget *progress;
	
	progress = g_object_get_data (G_OBJECT (dialog), "progress");
	
	gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (progress), percent);
	
	return;
}

/* This function is called via a timeout loop and processes any events waiting
 * in 'queue'.
 */

static gboolean
queue_loop (GAsyncQueue *queue)
{
	MsgInfo *info;
	static gpointer active_widget = NULL;
	
	do
	{
		info = g_async_queue_try_pop (queue);
		if (info)
		{
			switch (info->type)
			{
				case MSG_TYPE_SET_WIDGET:
				{
					g_assert (info->widget);
					g_assert (GTK_IS_WIDGET (info->widget));
					active_widget = info->widget;
					break;
				}
				case MSG_TYPE_ERROR:
				{
					g_print ("error: %s\n", info->msg);
					break;
				}
				case MSG_TYPE_PROGRESS:
				{
					g_assert (active_widget);
					g_assert (GTK_IS_WIDGET (active_widget));
					update_progress_bar (GTK_WIDGET (active_widget), info->progress);
					break;
				}
				case MSG_TYPE_INSERT_CD:
				{
					g_assert (active_widget);
					g_assert (GTK_IS_WIDGET (active_widget));
					break;
				}
				case MSG_TYPE_DONE:
				{
					g_assert (active_widget);
					g_assert (GTK_IS_WIDGET (active_widget));
					gtk_widget_destroy (GTK_WIDGET (active_widget));
					active_widget = NULL;
					break;
				}
				default: break;
			}
			msg_info_free (info);
		}
	} while (info);
	
	return TRUE;
}

/* Create a new GAsyncQueue and setup a timeout function to check the
 * queue for new events.
 */

GAsyncQueue*
msg_queue_setup (void)
{
	GAsyncQueue *queue;
	
	queue = g_async_queue_new ();
	
	g_timeout_add (333, (GSourceFunc) queue_loop, queue);
	
	return queue;
}

/* Allocate memory for a new MsgInfo structure. */

MsgInfo*
msg_info_new (void)
{
	MsgInfo *info;
	
	info = g_new0 (MsgInfo, 1);
	info->widget = NULL;
	info->msg = NULL;
	info->progress = 0.0;
	
	return info;
}

/* Free the memory used by a MsgInfo structure. */

void
msg_info_free (MsgInfo *info)
{
	g_free (info->msg);
	g_free (info);

	return;
}
