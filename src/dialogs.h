/*
 *  Copyright 2003 Todd Kulesza <todd@dropline.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef _DIALOGS_H_
#define _DIALOGS_H_

#include "config.h"

#include <gtk/gtk.h>

void
dlg_about(void);

void
dlg_properties (void);

void
dlg_preferences (void);

gboolean
dlg_save_disc (gboolean save_as);

void
dlg_open_disc (void);

gboolean
dlg_confirm_close (void);

void
dlg_burn_disc (void);

void
dlg_burn_image (void);

void
dlg_blank_disc (void);

#endif /* _DIALOGS_H_ */
