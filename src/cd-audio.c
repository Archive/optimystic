/*
 *  Copyright 2003-2004 Todd Kulesza <todd@dropline.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h"

#include "optimystic.h"
#include "cd-lib.h"
#include "cd-audio.h"

#include <string.h>
#include <gnome.h>
#include <libgnomevfs/gnome-vfs-utils.h>
#include <libgnomevfs/gnome-vfs-mime-handlers.h>
#include <libgnomevfs/gnome-vfs-directory.h>
#include <libgnomevfs/gnome-vfs-ops.h>
#include <libgnomevfs/gnome-vfs-async-ops.h>
#include <libxml/tree.h>
#include <libxml/parser.h>
#include <gst/gst.h>

enum 
{
	AUDIO_TRACK_COLUMN,
	AUDIO_TITLE_COLUMN,
	AUDIO_LENGTH_COLUMN,
	AUDIO_FILENAME_COLUMN,
	AUDIO_CACHE_COLUMN,
	AUDIO_CACHED_COLUMN,
	AUDIO_MIMETYPE_COLUMN,
	AUDIO_LENGTH_SECONDS_COLUMN,
	N_AUDIO_COLUMNS
};

enum
{
	TARGET_STRING,
	TARGET_URL
};

static const GtkTargetEntry target_table [] =
{
	{ "STRING",        0, TARGET_STRING },
	{ "text/plain",    0, TARGET_STRING },
	{ "text/uri-list", 0, TARGET_URL }
};

OptiData *od;
GMutex *files_to_add_mutex;
GMutex *stream_length_mutex;

// free the AudioAddData struct
void
audio_add_data_free (AudioAddData *data)
{
	g_free (data->title);
	g_free (data->uri);
	g_free (data->mimetype);
	
	g_free (data);
	
	return;
}

// free the AudioFileEntry struct
void
opti_audio_file_entry_free (gpointer data, gpointer user_data)
{
	AudioFileEntry *entry = data;
	
	g_free (entry->hd_path);
	g_free (entry);
	
	return;
}

// return a GstElement that can convert 'mimetype' to audio/raw
GstElement *
get_conv_from_mimetype (const gchar *mimetype)
{
	GstElement *conv;
	
	// FIXME: the autoplugger doesn't work reliably yet, so we do this by hand

	if (!strcmp (mimetype, "audio/mpeg"))
		conv = gst_element_factory_make ("mad", "read_mp3");
	else if (!strcmp (mimetype, "application/ogg"))
		conv = gst_element_factory_make ("vorbisfile", "read_ogg");
	else if (!strcmp (mimetype, "audio/x-wav"))
		conv = gst_element_factory_make ("wavparse", "read_wav");
	else if (!strcmp (mimetype, "application/x-flac"))
		conv = gst_element_factory_make ("flacdec", "read_flac");
	else
		conv = NULL;
	
	return conv;
}

// run over the audio list and update the progress meter to the full percentage
void
audio_update_progress_meter (GtkTreeModel *model)
{
	gint seconds, seconds_total;
	gdouble percent;
	gchar *text;
	gboolean valid;
	GtkTreeIter iter;
	
	seconds_total = 0;
	
	valid = gtk_tree_model_get_iter_first (model, &iter);
	while (valid)
	{
		gtk_tree_model_get (model, &iter,
				AUDIO_LENGTH_SECONDS_COLUMN, &seconds, -1);
		seconds_total += seconds;
		
		valid = gtk_tree_model_iter_next (model, &iter);
	}
	
	percent = (gdouble) seconds_total / (gdouble) od->oc->length_seconds_total;
	text = g_strdup_printf ("%.0f%% full", percent * 100.0);
	if (percent > 1.0)
		percent = 1.0;
	gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (od->oc->meter), percent);
	gtk_progress_bar_set_text (GTK_PROGRESS_BAR (od->oc->meter), text);
	g_free (text);
	
	return;
}

// begin playing the selected row, hide the play button, show the pause button
void
audio_play (GtkWidget *view)
{
	GtkTreeModel *model;
	GtkTreeSelection *selection;
	GtkTreeIter iter;
	GtkTreePath *path;
	GList *rows;
	gchar *uri;
	
	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (view));
	rows = gtk_tree_selection_get_selected_rows (selection, &model);
	if (rows && (g_list_length (rows) == 1))
	{
		path = (GtkTreePath *)rows->data;
		gtk_tree_model_get_iter (model, &iter, rows->data);
		gtk_tree_model_get (model, &iter, AUDIO_FILENAME_COLUMN, &uri, -1);
		gtk_tree_path_free (path);
		g_list_free (rows);
		
		gst_play_set_state (od->player_preview, GST_STATE_NULL);
		gst_play_set_location (od->player_preview, uri);
		gst_play_set_state (od->player_preview, GST_STATE_PLAYING);
		g_free (uri);
	}
	
	gtk_widget_show_all (od->oc->comm_pause);
	gtk_widget_hide (od->oc->comm_play);
	
	return;
}

// pause gstreamer, show the play button, hide the pause button
void
audio_pause (void)
{
	gst_play_set_state (od->player_preview, GST_STATE_NULL);
	
	gtk_widget_hide (od->oc->comm_pause);
	gtk_widget_show_all (od->oc->comm_play);
	
	return;
}

// alert us that a drag is in progress
void
audio_drag_begin_cb (GtkWidget *view, GdkDragContext *context, gpointer data)
{
	AudioDragData *drag_data = data;
	
	drag_data->drag_in_progress = TRUE;

	return;
}

// alert us that a drag has ended.  if the treeview is reorderable, make it
// non-reorderable and turn on the ability to drag files to the audio track
// list
void
audio_drag_end_cb (GtkWidget *view, GdkDragContext *context, gpointer data)
{
	AudioDragData *drag_data = data;
	
	drag_data->drag_in_progress = FALSE;
	
	if (!drag_data->is_dest_set)
	{
		gtk_tree_view_set_reorderable (GTK_TREE_VIEW (view), FALSE);
		
		gtk_drag_dest_set (view, GTK_DEST_DEFAULT_ALL, target_table, 
				G_N_ELEMENTS (target_table), GDK_ACTION_COPY);
		g_signal_handler_unblock (view, drag_data->handler_id);
		drag_data->is_dest_set = TRUE;
		
//		opti_set_menu_sensitivity ();
	}
	
	return;
}

// if files can be dragged to the audio track list, block the signals that
// allow it to work and make the track list reorderable
gboolean
audio_button_press_cb (GtkWidget *view, GdkEventButton *event, gpointer data)
{
	AudioDragData *drag_data = data;

	if (drag_data->is_dest_set)
	{
		gtk_drag_dest_unset (view);
		g_signal_handler_block (view, drag_data->handler_id);
		drag_data->is_dest_set = FALSE;
	}	
	
	gtk_tree_view_set_reorderable (GTK_TREE_VIEW (view), TRUE);
	
	return FALSE;
}

// if no drag is in progress, allow files to be dragged onto the audio track
// list
gboolean
audio_button_release_cb (GtkWidget *view, GdkEventButton *event, gpointer data)
{
	AudioDragData *drag_data = data;
	
	if (!drag_data->is_dest_set && !drag_data->drag_in_progress)
	{
		gtk_tree_view_set_reorderable (GTK_TREE_VIEW (view), FALSE);
	
		gtk_drag_dest_set (view, GTK_DEST_DEFAULT_ALL, target_table, 
				G_N_ELEMENTS (target_table), GDK_ACTION_COPY);
		g_signal_handler_unblock (view, drag_data->handler_id);
		drag_data->is_dest_set = TRUE;
	}
	
	return FALSE;
}

// change the length of the cd (used to calculate the % full)
void
change_cd_length_74_cb (GtkWidget *menuitem, gpointer data)
{
	GtkListStore *store = data;
	
	od->oc->length_seconds_total = (74 * 60);
	
	audio_update_progress_meter (GTK_TREE_MODEL (store));
	
	return;
}

// change the length of the cd (used to calculate the % full)
void
change_cd_length_80_cb (GtkWidget *menuitem, gpointer data)
{
	GtkListStore *store = data;
	
	od->oc->length_seconds_total = (80 * 60);
	
	audio_update_progress_meter (GTK_TREE_MODEL (store));
	
	return;
}

// begin playing the selected row, hide the play button, show the pause button
void
audio_play_cb (GtkWidget *button, GtkWidget *view)
{
	audio_play (view);
	
	return;
}

// pause gstreamer, show the play button, hide the pause button
void
audio_pause_cb (GtkWidget *button, gpointer data)
{
	audio_pause ();
	
	return;
}

// refresh the interface with the new track_list
void
opti_open_audio_cd (GList *track_list)
{
	GList *current, *to_add;
	AudioFileEntry *entry;
	
	to_add = NULL;
	
//	track_list = g_list_sort (track_list, (GCompareFunc) sort_track_list);
	
	for (current = track_list; current; current = current->next)
	{
		entry = current->data;
		to_add = g_list_append (to_add, entry->hd_path);
	}
	
//	opti_set_menu_sensitivity ();
	
	return;
}

// build the audio track list and command buttons
GtkWidget *
opti_build_audio_view (void)
{
	GtkWidget *box;
	GtkWidget *hbox1, *hbox2;
	GtkWidget *scrolled;
	GtkWidget *view;
	GtkWidget *commands;
	GtkWidget *button;
	GtkWidget *spacer;
	GtkWidget *label;
	GtkWidget *meter, *meter_menu, *menu, *menuitem_74, *menuitem_80;
	GtkListStore *store;
	GtkCellRenderer *renderer;
	GtkTreeViewColumn *column;
	GtkSizeGroup *size1, *size2;
	AudioDragData *drag_data;
	gint length;
	
	box = gtk_vbox_new (FALSE, 12);
	
	scrolled = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled),
			GTK_POLICY_AUTOMATIC, GTK_POLICY_ALWAYS);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scrolled),
			GTK_SHADOW_IN);
	gtk_box_pack_start (GTK_BOX (box), scrolled, TRUE, TRUE, 0);
	
	store = gtk_list_store_new (N_AUDIO_COLUMNS,
			G_TYPE_STRING,
			G_TYPE_STRING,
			G_TYPE_STRING,
			G_TYPE_STRING,
			G_TYPE_STRING,
			G_TYPE_BOOLEAN,
			G_TYPE_STRING,
			G_TYPE_INT);
	
	view = gtk_tree_view_new_with_model (GTK_TREE_MODEL (store));
	
	drag_data = g_new0 (AudioDragData, 1);
	drag_data->drag_in_progress = FALSE;
	drag_data->is_dest_set = TRUE;
	drag_data->handler_id = 0;
			
	gtk_drag_dest_set (view, GTK_DEST_DEFAULT_ALL, target_table, 
			G_N_ELEMENTS (target_table), GDK_ACTION_COPY);

	// handle drag and drop for reordering and adding files
	g_signal_connect (G_OBJECT (view), "button_press_event",
			G_CALLBACK (audio_button_press_cb), drag_data);
	g_signal_connect (G_OBJECT (view), "button_release_event",
			G_CALLBACK (audio_button_release_cb), drag_data);
	g_signal_connect (G_OBJECT (view), "drag_begin",
			G_CALLBACK (audio_drag_begin_cb), drag_data);
	g_signal_connect (G_OBJECT (view), "drag_end",
			G_CALLBACK (audio_drag_end_cb), drag_data);

	renderer = gtk_cell_renderer_text_new ();
	column = gtk_tree_view_column_new_with_attributes (_("Track"), renderer,
			"text", AUDIO_TRACK_COLUMN, NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (view), column);
	
	renderer = gtk_cell_renderer_text_new ();
	column = gtk_tree_view_column_new_with_attributes (_("Name"), renderer,
			"text", AUDIO_TITLE_COLUMN, NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (view), column);
	
	renderer = gtk_cell_renderer_text_new ();
	column = gtk_tree_view_column_new_with_attributes (_("Length"), renderer,
			"text", AUDIO_LENGTH_COLUMN, NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (view), column);
	
	size1 = gtk_size_group_new (GTK_SIZE_GROUP_BOTH);
	size2 = gtk_size_group_new (GTK_SIZE_GROUP_BOTH);
	
	hbox1 = gtk_hbox_new (FALSE, 12);
	gtk_box_pack_start (GTK_BOX (box), hbox1, FALSE, FALSE, 0);
	
	// take up 6 pixels of space to make the padding look nice
	spacer = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_end (GTK_BOX (hbox1), spacer, FALSE, FALSE, 0);
	
	hbox2 = gtk_hbox_new (FALSE, 12);
	gtk_box_pack_start (GTK_BOX (hbox1), hbox2, TRUE, TRUE, 0);
	
	meter = gtk_progress_bar_new ();
	gtk_progress_bar_set_text (GTK_PROGRESS_BAR (meter), "0% full");
	od->oc->meter = meter;
	gtk_box_pack_start (GTK_BOX (hbox2), meter, TRUE, TRUE, 0);
	
	hbox2 = gtk_hbox_new (FALSE, 12);
	gtk_size_group_add_widget (size1, hbox2);
	gtk_box_pack_start (GTK_BOX (hbox1), hbox2, FALSE, FALSE, 0);
	
	label = gtk_label_new_with_mnemonic (_("CD _Length:"));
	gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);
	gtk_box_pack_start (GTK_BOX (hbox2), label, FALSE, FALSE, 0);
	
	menu = gtk_menu_new ();
	
	menuitem_74 = gtk_menu_item_new_with_label (_("74 minutes"));
	g_signal_connect (G_OBJECT (menuitem_74), "activate", 
			G_CALLBACK (change_cd_length_74_cb), store);
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem_74);
	
	menuitem_80 = gtk_menu_item_new_with_label (_("80 minutes"));
	g_signal_connect (G_OBJECT (menuitem_80), "activate", 
			G_CALLBACK (change_cd_length_80_cb), store);
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem_80);
	
	length = gconf_client_get_int (od->gconf, 
			"/apps/optimystic/audio_last_length", NULL);
	switch (length)
	{
		case 80: gtk_menu_item_activate (GTK_MENU_ITEM (menuitem_80)); break;
		default: gtk_menu_item_activate (GTK_MENU_ITEM (menuitem_74)); break;
	}
	
	meter_menu = gtk_option_menu_new ();
	gtk_label_set_mnemonic_widget (GTK_LABEL (label), meter_menu);
	gtk_option_menu_set_menu (GTK_OPTION_MENU (meter_menu), menu);
	gtk_box_pack_start (GTK_BOX (hbox2), meter_menu, TRUE, TRUE, 0);
	
	hbox1 = gtk_hbox_new (FALSE, 12);
	gtk_box_pack_start (GTK_BOX (box), hbox1, FALSE, FALSE, 0);
	
	// take up 6 pixels of space to make the padding look nice
	spacer = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_end (GTK_BOX (hbox1), spacer, FALSE, FALSE, 0);
	
	commands = gtk_hbutton_box_new ();
	gtk_button_box_set_layout (GTK_BUTTON_BOX (commands), GTK_BUTTONBOX_START);
	gtk_box_set_spacing (GTK_BOX (commands), 12);
	gtk_box_pack_start (GTK_BOX (hbox1), commands, TRUE, TRUE, 0);
	
	button = gtk_button_new_with_mnemonic (_("_Play"));
	gtk_size_group_add_widget (size2, button);
	gtk_box_pack_start (GTK_BOX (commands), button, FALSE, FALSE, 0);
	g_signal_connect (G_OBJECT (button), "clicked",
			G_CALLBACK (audio_play_cb), view);
	od->oc->comm_play = button;
	
	button = gtk_button_new_with_mnemonic (_("Sto_p"));
	gtk_size_group_add_widget (size2, button);
	gtk_box_pack_start (GTK_BOX (commands), button, FALSE, FALSE, 0);
	g_signal_connect (G_OBJECT (button), "clicked",
			G_CALLBACK (audio_pause_cb), NULL);
	od->oc->comm_pause = button;
	
	commands = gtk_hbutton_box_new ();
	gtk_button_box_set_layout (GTK_BUTTON_BOX (commands), GTK_BUTTONBOX_END);
	gtk_box_set_spacing (GTK_BOX (commands), 12);
	gtk_size_group_add_widget (size1, commands);
	gtk_box_pack_start (GTK_BOX (hbox1), commands, TRUE, TRUE, 0);
	
	button = gtk_button_new_from_stock (GTK_STOCK_REMOVE);
	gtk_box_pack_start (GTK_BOX (commands), button, FALSE, FALSE, 0);
	od->oc->comm_remove = button;
	
	button = gtk_button_new_from_stock (GTK_STOCK_ADD);
	gtk_size_group_add_widget (size2, button);
	gtk_box_pack_start (GTK_BOX (commands), button, FALSE, FALSE, 0);
	od->oc->comm_add = button;
	
	// take up 6 pixels of space to make the padding look nice
	spacer = gtk_vbox_new (FALSE, 0);
	gtk_box_pack_end (GTK_BOX (box), spacer, FALSE, FALSE, 0);
	
	gtk_container_add (GTK_CONTAINER (scrolled), view);
			
	gtk_widget_show_all (box);
	gtk_widget_hide_all (od->oc->comm_pause);
	
	od->oc->store = store;
	od->oc->type = CD_TYPE_AUDIO;
	
	return box;
}
