/*
 *  Copyright 2003-2004 Todd Kulesza <todd@dropline.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h"

#include "optimystic.h"
#include "cd-lib.h"
#include "cd-data.h"

#include <string.h>
#include <gnome.h>
#include <libgnomevfs/gnome-vfs-utils.h>
#include <libgnomevfs/gnome-vfs-mime-handlers.h>
#include <libgnomevfs/gnome-vfs-directory.h>
#include <libgnomevfs/gnome-vfs-ops.h>
#include <libgnomevfs/gnome-vfs-async-ops.h>
#include <libxml/tree.h>
#include <libxml/parser.h>

#define ICON_SIZE	24.0
#define VIRTUAL_FOLDER_KEYWORD	"folder"

enum
{
	DATA_FILE_COLUMN,
	DATA_SIZE_COLUMN,
	DATA_DIR_COLUMN,
	DATA_TYPE_COLUMN,
	DATA_ICON_COLUMN,
	N_DATA_COLUMNS
};

enum
{
	SORT_FILENAME,
};

OptiData *od;
GMutex *generic_mutex;

gint
data_file_entry_cmp (gconstpointer a, gconstpointer b)
{
	gint retval;
	const DataFileEntry *x, *y;
	
	x = a;
	y = b;
	
	retval = strcmp (x->cd_path, y->cd_path);
	
	return retval;
}

void
opti_data_file_entry_free (gpointer data, gpointer user_data)
{
	DataFileEntry *entry = data;
	
	g_free (entry->hd_path);
	g_free (entry->cd_path);
	g_free (entry);
	
	return;
}

// sum up the size of the new files added to the cd
void
sum_file_sizes (gpointer data, gint *sum)
{
	GnomeVFSFileInfo info;
	DataFileEntry *entry = data;
	
	if ((gnome_vfs_get_file_info (entry->hd_path, &info, 
			(GNOME_VFS_FILE_INFO_DEFAULT | GNOME_VFS_FILE_INFO_FOLLOW_LINKS)))
			== GNOME_VFS_OK)
	{
		entry->size = (guint) info.size;
		*sum += entry->size;
	}
	
	return;
}

// update the cd size meter
void
update_size_meter (guint size, guint size_total)
{
	gdouble percent;
	gchar *text;
	
	percent = (gdouble) size / (gdouble) size_total;
	text = g_strdup_printf ("%.0f%% full", percent * 100.0);
	if (percent > 1.0)
		percent = 1.0;
	gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (od->oc->meter), percent);
	gtk_progress_bar_set_text (GTK_PROGRESS_BAR (od->oc->meter), text);
	g_free (text);

	return;
}

// change the size of the cd in bytes (used to calculate the % full)
void
change_cd_length_650_cb (GtkWidget *menuitem, gpointer data)
{
	od->oc->size_total = (650 * 1024 * 1024);
	
	update_size_meter (od->oc->size, od->oc->size_total);
	
	return;
}

// change the size of the cd in bytes (used to calculate the % full)
void
change_cd_length_700_cb (GtkWidget *menuitem, gpointer data)
{
	od->oc->size_total = (700 * 1024 * 1024);
	
	update_size_meter (od->oc->size, od->oc->size_total);
	
	return;
}

// refresh the interface with the new file_list
void
opti_open_data_cd (GList *file_list)
{
	GnomeVFSFileInfo info;
	GnomeVFSResult result;
	GList *current, *to_remove;
	DataFileEntry *entry;
	
	to_remove = NULL;
	
	for (current = file_list; current; current = current->next)
	{
		entry = current->data;
		
		result = gnome_vfs_get_file_info (entry->hd_path, &info, 
				GNOME_VFS_FILE_INFO_DEFAULT | GNOME_VFS_FILE_INFO_FOLLOW_LINKS);
		// skip over hierarchy folders in the cd
		if (strcmp (entry->hd_path, VIRTUAL_FOLDER_KEYWORD) && (result != GNOME_VFS_OK))
			to_remove = g_list_prepend (to_remove, entry);
		else
		{
			entry->size = (guint) info.size;
			od->oc->size += entry->size;
		}
	}
	
	for (current = to_remove; current; current = current->next)
	{
		entry = current->data;
		file_list = g_list_remove (file_list, entry);
		g_free (entry->hd_path);
		g_free (entry->cd_path);
		g_free (entry);
	}
	g_list_free (to_remove);
	
	od->oc->file_list = file_list;
	
//	data_update_store (od->oc->file_list, od->oc->store, "/");
	update_size_meter (od->oc->size, od->oc->size_total);
	
	return;
}

// build the data file list and command buttons
GtkWidget *
opti_build_data_view (void)
{
	GtkWidget *box;
	GtkWidget *hbox1, *hbox2;
	GtkWidget *scrolled;
	GtkWidget *view;
	GtkWidget *commands;
	GtkWidget *button;
	GtkWidget *spacer;
	GtkWidget *label;
	GtkWidget *meter, *meter_menu, *menu, *menuitem_650, *menuitem_700;
	gint length;
	GtkListStore *store;
	GtkCellRenderer *renderer;
	GtkTreeViewColumn *column;
	GtkSizeGroup *size1, *size2;
	
	box = gtk_vbox_new (FALSE, 12);
	
	scrolled = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled),
			GTK_POLICY_AUTOMATIC, GTK_POLICY_ALWAYS);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scrolled),
			GTK_SHADOW_IN);
	gtk_box_pack_start (GTK_BOX (box), scrolled, TRUE, TRUE, 0);
	
	store = gtk_list_store_new (N_DATA_COLUMNS,
			G_TYPE_STRING,
			G_TYPE_STRING,
			G_TYPE_BOOLEAN,
			G_TYPE_STRING,
			GDK_TYPE_PIXBUF);
	
	view = gtk_tree_view_new_with_model (GTK_TREE_MODEL (store));
	gtk_tree_view_set_headers_clickable (GTK_TREE_VIEW (view), FALSE);
	
	renderer = gtk_cell_renderer_pixbuf_new ();
	column = gtk_tree_view_column_new_with_attributes ("", renderer,
			"pixbuf", DATA_ICON_COLUMN, NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (view), column);
	
	renderer = gtk_cell_renderer_text_new ();
	column = gtk_tree_view_column_new_with_attributes (_("Name"), renderer,
			"text", DATA_FILE_COLUMN, NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (view), column);
	gtk_tree_view_column_set_sort_column_id (GTK_TREE_VIEW_COLUMN (column),
			SORT_FILENAME);

	renderer = gtk_cell_renderer_text_new ();
	column = gtk_tree_view_column_new_with_attributes (_("Size"), renderer,
			"text", DATA_SIZE_COLUMN, NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (view), column);
	
	size1 = gtk_size_group_new (GTK_SIZE_GROUP_BOTH);
	size2 = gtk_size_group_new (GTK_SIZE_GROUP_BOTH);

	hbox1 = gtk_hbox_new (FALSE, 12);
	gtk_box_pack_start (GTK_BOX (box), hbox1, FALSE, FALSE, 0);
	
	// take up 6 pixels of space to make the padding look nice
	spacer = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_end (GTK_BOX (hbox1), spacer, FALSE, FALSE, 0);
	
	hbox2 = gtk_hbox_new (FALSE, 12);
	gtk_box_pack_start (GTK_BOX (hbox1), hbox2, TRUE, TRUE, 0);
	
	meter = gtk_progress_bar_new ();
	gtk_progress_bar_set_text (GTK_PROGRESS_BAR (meter), "0% full");
	od->oc->meter = meter;
	gtk_box_pack_start (GTK_BOX (hbox2), meter, TRUE, TRUE, 0);
	
	hbox2 = gtk_hbox_new (FALSE, 12);
	gtk_size_group_add_widget (size1, hbox2);
	gtk_box_pack_start (GTK_BOX (hbox1), hbox2, FALSE, FALSE, 0);
	
	label = gtk_label_new_with_mnemonic (_("CD _Size:"));
	gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);
	gtk_box_pack_start (GTK_BOX (hbox2), label, FALSE, FALSE, 0);
	
	menu = gtk_menu_new ();
	
	menuitem_650 = gtk_menu_item_new_with_label (_("650 megabytes"));
	g_signal_connect (G_OBJECT (menuitem_650), "activate", 
			G_CALLBACK (change_cd_length_650_cb), store);
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem_650);
	
	menuitem_700 = gtk_menu_item_new_with_label (_("700 megabytes"));
	g_signal_connect (G_OBJECT (menuitem_700), "activate", 
			G_CALLBACK (change_cd_length_700_cb), store);
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem_700);
	
	length = gconf_client_get_int (od->gconf, 
			"/apps/optimystic/data_last_length", NULL);
	switch (length)
	{
		case 700: gtk_menu_item_activate (GTK_MENU_ITEM (menuitem_700)); break;
		default: gtk_menu_item_activate (GTK_MENU_ITEM (menuitem_650)); break;
	}
	
	meter_menu = gtk_option_menu_new ();
	gtk_label_set_mnemonic_widget (GTK_LABEL (label), meter_menu);
	gtk_option_menu_set_menu (GTK_OPTION_MENU (meter_menu), menu);
	gtk_box_pack_start (GTK_BOX (hbox2), meter_menu, TRUE, TRUE, 0);

	hbox1 = gtk_hbox_new (FALSE, 12);
	gtk_box_pack_start (GTK_BOX (box), hbox1, FALSE, FALSE, 0);
	
	// take up 6 pixels of space to make the padding look nice
	spacer = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_end (GTK_BOX (hbox1), spacer, FALSE, FALSE, 0);
	
	commands = gtk_hbutton_box_new ();
	gtk_button_box_set_layout (GTK_BUTTON_BOX (commands), GTK_BUTTONBOX_START);
	gtk_box_set_spacing (GTK_BOX (commands), 12);
	gtk_box_pack_start (GTK_BOX (hbox1), commands, TRUE, TRUE, 0);
	
	button = gtk_button_new_from_stock (GTK_STOCK_GO_UP);
	gtk_size_group_add_widget (size2, button);
	gtk_box_pack_start (GTK_BOX (commands), button, FALSE, FALSE, 0);
	od->oc->comm_up = button;
	
	commands = gtk_hbutton_box_new ();
	gtk_button_box_set_layout (GTK_BUTTON_BOX (commands), GTK_BUTTONBOX_START);
	gtk_box_set_spacing (GTK_BOX (commands), 12);
	gtk_box_pack_start (GTK_BOX (hbox1), commands, FALSE, TRUE, 0);
	
	button = gtk_button_new_with_mnemonic (_("_Create Folder"));
	gtk_size_group_add_widget (size2, button);
	gtk_box_pack_start (GTK_BOX (commands), button, FALSE, FALSE, 0);
	gtk_button_box_set_child_secondary (GTK_BUTTON_BOX (commands), button, TRUE);
	od->oc->comm_folder = button;
	
	commands = gtk_hbutton_box_new ();
	gtk_button_box_set_layout (GTK_BUTTON_BOX (commands), GTK_BUTTONBOX_END);
	gtk_box_set_spacing (GTK_BOX (commands), 12);
	gtk_size_group_add_widget (size1, commands);
	gtk_box_pack_start (GTK_BOX (hbox1), commands, FALSE, TRUE, 0);
		
	button = gtk_button_new_from_stock (GTK_STOCK_REMOVE);
	gtk_box_pack_start (GTK_BOX (commands), button, FALSE, FALSE, 0);
	od->oc->comm_remove = button;
	
	button = gtk_button_new_from_stock (GTK_STOCK_ADD);
	gtk_size_group_add_widget (size2, button);
	gtk_box_pack_start (GTK_BOX (commands), button, FALSE, FALSE, 0);
	od->oc->comm_add = button;
	
	// take up 6 pixels of space to make the padding look nice
	spacer = gtk_vbox_new (FALSE, 0);
	gtk_box_pack_end (GTK_BOX (box), spacer, FALSE, FALSE, 0);
	
	gtk_container_add (GTK_CONTAINER (scrolled), view);
	
	gtk_widget_show_all (box);
	
	od->oc->type = CD_TYPE_DATA;
	od->oc->store = store;
	
	return box;
}
