%define name       @PACKAGE@
%define version    @VERSION@
%define release    1
%define prefix     /usr
%define sysconfdir /etc
%define gettext_package @PACKAGE@

Summary:    A CD creation application.
Name:       %{name}
Version:    %{version}
Release:    %{release}
Epoch:      1
License:    GPL
Group:      Applications
Vendor:     Dropline Systems
Url:        http://www.dropline.net/optimystic
Source:     %{name}-%{version}.tar.bz2
Packager:   Todd Kulesza <todd@dropline.net>
BuildRoot:  /var/tmp/%{name}-%{version}-root
Requires:   glib2 >= @GLIB_REQUIRED@
Requires:   gtk2 >= @GTK_REQUIRED@
Requires:   libgnome >= @LIBGNOME_REQUIRED@
Requires:   libgnomeui >= @LIBGNOMEUI_REQUIRED@
Requires:   gnome-vfs2 >= @GNOME_VFS_REQUIRED@
Requires:   gconf2 >= @GCONF_REQUIRED@
Requires:   gstreamer >= @GSTREAMER_REQUIRED@
Requires:   libburn >= @LIBBURN_REQUIRED@
BuildRequires:   glib2-devel >= @GLIB_REQUIRED@
BuildRequires:   gtk2-devel >= @GTK_REQUIRED@
BuildRequires:   libgnome-devel >= @LIBGNOME_REQUIRED@
BuildRequires:   libgnomeui-devel >= @LIBGNOMEUI_REQUIRED@
BuildRequires:   gnome-vfs2-devel >= @GNOME_VFS_REQUIRED@
BuildRequires:   gconf2-devel >= @GCONF_REQUIRED@
BuildRequires:   gstreamer-devel >= @GSTREAMER_REQUIRED@
BuildRequires:   libburn-devel >= @LIBBURN_REQUIRED@
Prefix:     %{prefix}

%description
Optimystic is a CD creation application designed with usability as the
primary concern.  It makes creating CDs simple and straight-forward.

%prep
%setup

%build
%configure
make

%install
rm -rf $RPM_BUILD_ROOT

export GCONF_DISABLE_MAKEFILE_SCHEMA_INSTALL=1
%makeinstall
unset GCONF_DISABLE_MAKEFILE_SCHEMA_INSTALL

# Clean out files that should not be part of the rpm.
# This is the recommended way of dealing with it for RH8
rm -rf $RPM_BUILD_ROOT/var/scrollkeeper/*

%find_lang %{gettext_package}

%post
export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
SCHEMAS="optimystic.schemas"
for S in $SCHEMAS; do
  gconftool-2 --makefile-install-rule %{_sysconfdir}/gconf/schemas/$S > /dev/null
done
/sbin/ldconfig

%postun
/sbin/ldconfig
/bin/true ## for rpmlint, -p requires absolute path and is just dumb

%files -f %{gettext_package}.lang
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_datadir}/applications/optimystic.desktop
%{_datadir}/pixmaps/optimystic.png
%{_datadir}/pixmaps/optimystic/cd.png
%{_datadir}/pixmaps/optimystic/play.png
%{_datadir}/pixmaps/optimystic/stop.png
%{_bindir}/optimystic

%clean
rm -rf $RPM_BUILD_ROOT

%changelog
* Tue Jan 06 2004 Todd Kulesza <todd@dropline.net>
- Removed dependancy on Scrollkeeper
* Sat Jan 03 2004 Todd Kulesza <todd@dropline.net>
- Wrote basic .spec.in file
